import React from 'react';
import './Index.css'
import { Alert, AlertContainer } from "react-bs-notifier";

class Alertas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true,
        }
        this.handleClose = this.handleClose.bind(this);

    }
    handleClose(e) {
        this.props.onShow(!this.props.value);
    }

    render() {

        if (this.props.value) {
            /**
             * Colocamos el setTimeout, para que cierre el mensaje despues de cierto tiempo
             */
            this.timeout = setTimeout(this.handleClose, 3000);
            return (
                <AlertContainer>
                    <Alert type={this.props.onType}>{this.props.onText} <i className="fa fa-times cerrarMensaje" aria-hidden="true" onClick={this.handleClose} ></i></Alert>
                </AlertContainer>
            )
        }
        return null;
    }
}

export default Alertas;