import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import ExportarInfo from '../../components/botonexportar/Index';

class ModalGeneral extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: this.props.onShow
        }
    }
    handleClose = () => {this.setState({ show: false });this.props.onChange()}
    handleAcept = () => {this.setState({ show: false });this.props.onChange("Si")}
    handleNotAcept = () => {this.setState({ show: false });this.props.onChange("No")}

    render() {
        return (
            <Modal show={this.state.show} onHide={this.handleClose}>
                <Modal.Header closeButton>
                     <Modal.Title>{this.props.onTitle}</Modal.Title> 
                </Modal.Header>
                <Modal.Body>{this.props.onBody}</Modal.Body>
                <Modal.Footer>
                    {/* <Button onClick={this.handleClose}>Cerrar</Button> */}
                    {this.props.onIf?<Button onClick={this.handleAcept}>Si</Button>:null}
                    {this.props.onClose?<Button onClick={this.handleNotAcept}>No</Button>:<Button onClick={this.handleClose}>Cerrar</Button>}
                    {this.props.onExport?<ExportarInfo onData={this.props.insumoasociado} onName={this.props.onExportName} onColumn={this.props.onExportColumn} onInfo={this.props.onExportInfo}></ExportarInfo>:null}
                </Modal.Footer>
            </Modal>
        )
    }
}


export default ModalGeneral;