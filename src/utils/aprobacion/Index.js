import React from 'react';
import { Modal, Button } from 'react-bootstrap';
//import Moment from 'moment';
const Aprovaciones = React.forwardRef((props, ref) => {
    return <Aprovacion {...props} innerRef={ref}></Aprovacion>
});
class Aprovacion extends React.Component {
    constructor(prop) {
        super(prop);
        this.state = {
            open: true,
            must: true,
            msg: "",
        };
        //this.updateData = this.updateData.bind(this);

    }
    updateData = (dt) => {

        let dtEst = this.props.onDataEst;
        let dtSelec = dtEst.filter(e => e.value === this.props.row.EstadoSolicitud).map(e => e.id + 1);
        let sendEst = { "valueEs": dtSelec[0], "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
        if (!dt) {
            switch (this.props.row.EstadoSolicitud) {
                case "Pendiente por Aprobar Entrega":
                    sendEst = { "valueEs": 10, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                case "Pendiente por Validar":
                    sendEst = { "valueEs": 5, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                case "Pendiente por Aprobar Validación":
                    sendEst = { "valueEs": 10, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                case "Pendiente por Enviar al Inventario":
                    sendEst = { "valueEs": 6, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                case "Error en Integración":
                    sendEst = { "valueEs": 3, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                default:
                    break;
            }
        } else {
            switch (this.props.row.EstadoSolicitud) {
                case "Pendiente por Validar":
                    sendEst = { "valueEs": 4, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                case "Pendiente por Aprobar Validación":
                    sendEst = { "valueEs": 5, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                case "Pendiente por Enviar al Inventario":
                    sendEst = { "valueEs": 8, "idSoli": this.props.row.id, "dtObra": this.props.row.NombreObra };
                    break;
                default:
                    break;
            }
        }
        this.props.onChangeEst(sendEst);
        setTimeout(this.close(), 100);
    }
    close = () => {
        this.setState({ open: false });
        this.props.onUpdate(this.props.defaultValue);
    }
    render() {
        return (
            <Modal show={this.state.open} ref={this.props.innerRef} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Aprobación</Modal.Title>
                </Modal.Header>
                <Modal.Body>¿Desea aprobar el estado {this.props.row.dtestado}?</Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.updateData(true)}>Si</Button>
                    <Button onClick={() => this.updateData(false)}>No</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}


export default Aprovaciones;