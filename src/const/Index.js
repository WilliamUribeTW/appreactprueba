// const RutaApi = 'http://localhost:53993/api/';
// const RutaApi = 'http://localhost:8034/api/';
const RutaApi = 'http://192.168.0.218:8034/api/';


/**
 * Maestros 
 */
// const DataClasSoli = [
//   { value: 1, label: "URBANISMO" },
//   { value: 2, label: "ESTRUCTURA" },
//   { value: 3, label: "ACABADOS" }
// ]

// const DataCapitulo = [
//   { value: 1, label: "URBANISMO", idClasiSoli: 1 },
//   { value: 2, label: "GASTOS GENERALES", idClasiSoli: 1 },

//   { value: 3, label: "PRELIMINARES", idClasiSoli: 2 },
//   { value: 4, label: "CIMENTACIONES", idClasiSoli: 2 },
//   { value: 5, label: "ESTRUCTURA", idClasiSoli: 2 },

//   { value: 6, label: "CARPINTERIA METALICA", idClasiSoli: 3 },
//   { value: 7, label: "VENTANERIA", idClasiSoli: 3 },
//   { value: 8, label: "CARPINTERIA EN MADERA", idClasiSoli: 3 },

// ]

// const DataSubCapitulo = [
//   { value: 1, label: "EXCAVACIONES LLENOS Y RETIROS", idCapitulo: 1 },
//   { value: 2, label: "CAÑUELAS Y CAJAS", idCapitulo: 1 },
//   { value: 3, label: "MUROS DE CONTENCION", idCapitulo: 1 },

//   { value: 4, label: "GASTOS MENSUALES DE OBRA", idCapitulo: 2 },
//   { value: 5, label: "GASTOS NOMINA Y ADMON OBRA", idCapitulo: 2 },

//   { value: 6, label: "DEMOLICIONES Y RETIROS", idCapitulo: 3 },
//   { value: 7, label: "INSTALACIONES PROVISIONALES", idCapitulo: 3 },
//   { value: 8, label: "ADECUACIONES", idCapitulo: 3 },

//   { value: 9, label: "EXCAVACIONES Y LLENOS", idCapitulo: 4 },
//   { value: 10, label: "ACERO CIMENTACIONES", idCapitulo: 4 },
//   { value: 11, label: "VOLADURA", idCapitulo: 4 },

//   { value: 12, label: "ACEROS ESTRUCTURA", idCapitulo: 5 },
//   { value: 13, label: "LOSAS DE CONTRAPISO", idCapitulo: 5 },
//   { value: 14, label: "COLUMNAS EN CONCRETO", idCapitulo: 5 },

//   { value: 15, label: "PUERTAS Y REJAS METALICAS", idCapitulo: 6 },
//   { value: 16, label: "PASAMANOS METALICO", idCapitulo: 6 },

//   { value: 17, label: "VENTANERIA ALUMINIO CRUDO", idCapitulo: 7 },

//   { value: 18, label: "PUERTAS Y MARCOS EN MADERA", idCapitulo: 8 },

// ]

// const DataCostos = [
//   { value: 1, label: 'Comision topografia via', idsubcapitulo: 1 },
//   { value: 2, label: 'Roceria de lote', idsubcapitulo: 1 },
//   { value: 3, label: 'Corte cargue acarreo externo via', idsubcapitulo: 1 },

//   { value: 4, label: 'Cañuela de 0.60', idsubcapitulo: 2 },
//   { value: 5, label: 'Cajas cañuela', idsubcapitulo: 2 },
//   { value: 6, label: 'Cuneta en v de 0.40', idsubcapitulo: 2 },

//   { value: 7, label: 'Muro Contenc.Conc.0.20 21MPa', idsubcapitulo: 3 },
//   { value: 8, label: 'Impermeab muro contencion', idsubcapitulo: 3 },
//   { value: 9, label: 'Filtros por m2 muro contencion', idsubcapitulo: 3 },

//   { value: 10, label: 'Papeleria y fotocopias', idsubcapitulo: 4 },
//   { value: 11, label: 'Caja menor', idsubcapitulo: 4 },
//   { value: 12, label: 'Servicios provisionales epm', idsubcapitulo: 4 },

//   { value: 13, label: 'Ayudante de obra', idsubcapitulo: 5 },
//   { value: 14, label: 'Nomina personal operativo obra', idsubcapitulo: 5 },
//   { value: 15, label: 'Nomina personal administrativo obra', idsubcapitulo: 5 },

//   { value: 16, label: 'Demolicion vivienda', idsubcapitulo: 6 },
//   { value: 17, label: 'Desmonte provisionales existentes', idsubcapitulo: 6 },
//   { value: 18, label: 'Desmonte instalaciones electricas provisionales', idsubcapitulo: 6 },

//   { value: 19, label: 'Provisionales contratistas', idsubcapitulo: 7 },
//   { value: 20, label: 'Dotacion oficinas', idsubcapitulo: 7 },
//   { value: 21, label: 'Planta  mezclas dosificadora', idsubcapitulo: 7 },

//   { value: 22, label: 'Cajas desarenadoras provision', idsubcapitulo: 8 },
//   { value: 23, label: 'Cañuelas manejo aguas lluvias', idsubcapitulo: 8 },
//   { value: 24, label: 'Guaje o cárcamo lavada carros', idsubcapitulo: 8 },

//   { value: 25, label: 'Nivelacion manual lotes E <= 15 cm', idsubcapitulo: 9 },
//   { value: 26, label: 'Corte cargue y bot exc banco', idsubcapitulo: 9 },
//   { value: 27, label: 'Excavacion Mecanica vigas fundac', idsubcapitulo: 9 },

//   { value: 28, label: 'Alambre quemado C18 cimentacion', idsubcapitulo: 10 },
//   { value: 29, label: 'Acero Cimentaciones Tenor Torre', idsubcapitulo: 10 },

//   { value: 30, label: 'Voladura de roca', idsubcapitulo: 11 },

//   { value: 31, label: 'Acero estructura Torre', idsubcapitulo: 12 },
//   { value: 32, label: 'Alambre quemado C18 estructura', idsubcapitulo: 12 },

//   { value: 33, label: 'Losa contrapiso interior(210)e=10cm', idsubcapitulo: 13 },

//   { value: 34, label: 'Columnas concreto 21 MPA (Machones)', idsubcapitulo: 14 },

//   { value: 35, label: 'Puerta metalica acceso  1.0x2.3 m', idsubcapitulo: 15 },
//   { value: 36, label: 'Puerta metalica cto basuras', idsubcapitulo: 15 },
//   { value: 37, label: 'Puerta metalica cto maquinas', idsubcapitulo: 15 },

//   { value: 38, label: 'Pasamanos Metalico Escaleras torre', idsubcapitulo: 16 },
//   { value: 39, label: 'Pasamanos metalico tipo 2 (pared)', idsubcapitulo: 16 },
//   { value: 40, label: 'Pasamanos metalico balcon', idsubcapitulo: 16 },

//   { value: 41, label: 'Ventana alum 1 cuerpo fijo+1corred 0.80x1.20', idsubcapitulo: 17 },
//   { value: 42, label: 'Vent.corrediza 1cuerpo fijo + 1 corred 1.80x0.80', idsubcapitulo: 17 },
//   { value: 43, label: 'Vent.corrediza 3 cuerpos 1.50x1.10', idsubcapitulo: 17 },

//   { value: 44, label: 'Marco+ala madecor 0.7x2.3 Baños aptos 54 m2', idsubcapitulo: 18 },
//   { value: 45, label: 'Marco+ala madecor 0.65x2.3 Baños aptos 48 m2', idsubcapitulo: 18 },
// ];


// const DataInsumo = [

//   { value: 1, label: 'Comision topografia via', und: 'dia', idCosto: 1 },

//   { value: 2, label: 'Roceria de lote', und: 'm2', idCosto: 2 },

//   { value: 3, label: 'Corte, cargue y botada tierra', und: 'm3', idCosto: 3 },

//   { value: 4, label: 'Formaleta de madera', und: 'm2', idCosto: 4 },
//   { value: 5, label: 'Triturado  3/4"', und: 'm3', idCosto: 4 },
//   { value: 6, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 4 },

//   { value: 7, label: 'Acero 3/8 chipa 60.000 psi', und: 'kg', idCosto: 5 },
//   { value: 8, label: 'Triturado  3/4"', und: 'm3', idCosto: 5 },
//   { value: 9, label: 'Cemento gris (50Kg)(Cemex)', und: 'm3', idCosto: 5 },

//   { value: 10, label: 'Formaleta de madera', und: 'm2', idCosto: 6 },
//   { value: 11, label: 'Triturado  3/4"', und: 'm3', idCosto: 6 },
//   { value: 12, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 6 },

//   { value: 13, label: 'Cemento gris granel Cemex', und: 'kg', idCosto: 7 },
//   { value: 14, label: 'Triturado 3/8"', und: 'm3', idCosto: 7 },
//   { value: 15, label: 'Eucom MR 5000', und: 'kg', idCosto: 7 },

//   { value: 16, label: 'Sum/instal impermeab manto polieste', und: 'm2', idCosto: 8 },
//   { value: 17, label: 'Lamina icopor 1mx1mx1cm', und: 'und', idCosto: 8 },
//   { value: 18, label: 'Sum/instal impermeab manto polieste', und: 'm2', idCosto: 8 },

//   { value: 19, label: 'Triturado 1 1/2"', und: 'm3', idCosto: 9 },
//   { value: 20, label: 'Arena gruesa concreto', und: 'm3', idCosto: 9 },
//   { value: 21, label: 'Triturado  3/4"', und: 'm3', idCosto: 9 },

//   { value: 22, label: 'Papeleria y fotocopias', und: 'und', idCosto: 10 },

//   { value: 23, label: 'Consumos por caja menor', und: 'und', idCosto: 11 },

//   { value: 24, label: 'Servicios publicos de obra', und: 'mes', idCosto: 12 },

//   { value: 25, label: 'Recargo hora extra ayudante', und: 'hr', idCosto: 13 },
//   { value: 26, label: 'Recargo hora extra llavero', und: 'hr', idCosto: 13 },
//   { value: 27, label: 'Recargo hora extra ascensorista', und: 'hr', idCosto: 13 },

//   { value: 28, label: 'Nomina personal operativo obra', und: 'mes', idCosto: 14 },

//   { value: 29, label: 'Nomina personal administrativo obra', und: 'mes', idCosto: 15 },

//   { value: 30, label: 'Subc demolicion viviendas', und: 'und', idCosto: 16 },

//   { value: 31, label: 'Desmonte provisionales existentes', und: 'und', idCosto: 17 },

//   { value: 32, label: 'Desmonte instalaciones electricas provisionales', und: 'und', idCosto: 18 },

//   { value: 33, label: 'Acero grafil 4 mm  x 6mt', und: 'und', idCosto: 19 },
//   { value: 34, label: 'Triturado  3/4"', und: 'm3', idCosto: 19 },
//   { value: 35, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 19 },

//   { value: 36, label: 'Impresora HP 2460', und: 'und', idCosto: 20 },
//   { value: 37, label: 'Cable UTP computador', und: 'ml', idCosto: 20 },
//   { value: 38, label: 'Licencia reloj biometrico obra', und: 'mes', idCosto: 20 },

//   { value: 39, label: 'Triturado  3/4"', und: 'm3', idCosto: 21 },
//   { value: 40, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 21 },
//   { value: 41, label: 'Eucom MR 5000', und: 'kg', idCosto: 21 },

//   { value: 42, label: 'Transp. cemento 50Kg (Cemex)', und: 'sac', idCosto: 22 },
//   { value: 43, label: 'Arena gruesa concreto', und: 'm3', idCosto: 22 },
//   { value: 44, label: 'Mallas electrosoldadas', und: 'kg', idCosto: 22 },

//   { value: 45, label: 'Triturado  3/4"', und: 'm3', idCosto: 23 },
//   { value: 46, label: 'Transp. cemento 50Kg (Cemex)', und: 'sac', idCosto: 23 },
//   { value: 47, label: 'Transporte  arena a obra', und: 'm3', idCosto: 23 },

//   { value: 48, label: 'Rejilla metalica carcamo', und: 'm', idCosto: 24 },
//   { value: 49, label: 'Formaleta de madera', und: 'm2', idCosto: 24 },
//   { value: 50, label: 'Triturado  3/4"', und: 'm3', idCosto: 24 },

//   { value: 51, label: 'Nivelacion manual lotes E <= 15 cm', und: 'und', idCosto: 25 },

//   { value: 52, label: 'Corte, cargue y botada tierra', und: 'm3', idCosto: 26 },

//   { value: 53, label: 'Corte, cargue y botada tierra', und: 'm3', idCosto: 27 },

//   { value: 54, label: 'Alambre quemado C18', und: 'kg', idCosto: 28 },

//   { value: 55, label: 'Acero figurado 60.000 PSI', und: 'kg', idCosto: 29 },

//   { value: 56, label: 'Subcontrato voladura roca din', und: 'pul', idCosto: 30 },

//   { value: 57, label: 'Mallas electrosoldadas', und: 'kg', idCosto: 31 },
//   { value: 58, label: 'Acero figurado 60.000 PSI', und: 'kg', idCosto: 31 },

//   { value: 59, label: 'Alambre quemado C18', und: 'kg', idCosto: 32 },

//   { value: 60, label: 'Arena gruesa concreto', und: 'm3', idCosto: 33 },
//   { value: 61, label: 'Triturado  3/4"', und: 'm3', idCosto: 33 },
//   { value: 62, label: 'Cemento gris granel Cemex', und: 'kg', idCosto: 33 },

//   { value: 63, label: 'Cemento gris granel Cemex', und: 'kg', idCosto: 34 },
//   { value: 64, label: 'Triturado 3/8"', und: 'm3', idCosto: 34 },
//   { value: 65, label: 'Eucom MR 5000', und: 'kg', idCosto: 34 },

//   { value: 66, label: 'Porton metalico acceso', und: 'und', idCosto: 35 },
//   { value: 67, label: 'Tope puerta', und: 'und', idCosto: 35 },
//   { value: 68, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 35 },

//   { value: 69, label: 'Puerta metalica cuarto basuras', und: 'und', idCosto: 36 },
//   { value: 70, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 36 },
//   { value: 71, label: 'Arena  pega', und: 'm3', idCosto: 36 },

//   { value: 72, label: 'Tope puerta', und: 'und', idCosto: 37 },
//   { value: 73, label: 'Cemento gris (50Kg)(Cemex)', und: 'sac', idCosto: 37 },
//   { value: 74, label: 'Arena  pega', und: 'm3', idCosto: 37 },

//   { value: 75, label: 'Pasamanos metalico x mt', und: 'm', idCosto: 38 },
//   { value: 76, label: 'Subc. pintura pasamanos completo', und: 'ml', idCosto: 38 },

//   { value: 77, label: 'Subc pintura pasam tubo lineal', und: 'ml', idCosto: 39 },
//   { value: 78, label: 'Sum.pasamanos sobr muro anticorrosi', und: 'ml', idCosto: 39 },

//   { value: 79, label: 'Pasamanos metalico x mt', und: 'm', idCosto: 40 },
//   { value: 80, label: 'Subc. pintura pasamanos completo', und: 'ml', idCosto: 40 },

//   { value: 81, label: 'Ventana Alum crudo 0.80x1.20', und: 'und', idCosto: 41 },
//   { value: 82, label: 'Sub.contrato sellado ventanería', und: 'ml', idCosto: 41 },

//   { value: 83, label: 'Ventana aluminio 0.80x1.80', und: 'und', idCosto: 42 },
//   { value: 84, label: 'Sub.contrato sellado ventanería', und: 'ml', idCosto: 42 },

//   { value: 85, label: 'Ventana Alum Anodizado 1.10x1.50', und: 'und', idCosto: 43 },
//   { value: 86, label: 'Sub.contrato sellado ventanería', und: 'ml', idCosto: 43 },

//   { value: 87, label: 'Bisagra hierro', und: 'und', idCosto: 44 },
//   { value: 88, label: 'Silicona transparente DAP', und: 'und', idCosto: 44 },
//   { value: 89, label: 'Tope puerta', und: 'und', idCosto: 44 },

//   { value: 90, label: 'Bisagra hierro', und: 'und', idCosto: 45 },
//   { value: 91, label: 'Silicona transparente DAP', und: 'und', idCosto: 45 },
//   { value: 92, label: 'Tope puerta', und: 'und', idCosto: 45 },

// ];

// const DataContratista = [
//   { value: 1, label: "Contratista 1" },
//   { value: 2, label: "Contratista 2" },
//   { value: 3, label: "Contratista 3" },
// ]

// const DataEncargado = [
//   { value: 1, label: 'Juan Castillo', fkcontratista: 1 },
//   { value: 2, label: 'Marcela Rojas', fkcontratista: 1 },
//   { value: 3, label: 'Cristina Mendez', fkcontratista: 1 },
//   { value: 4, label: 'Lucas Martez', fkcontratista: 2 },
//   { value: 5, label: 'Andrea Salazar', fkcontratista: 2 },
//   { value: 6, label: 'Tatiana Castro', fkcontratista: 2 },
//   { value: 7, label: 'Andres Carvajal', fkcontratista: 3 },
//   { value: 8, label: 'Catalina Maestre', fkcontratista: 3 },
//   { value: 9, label: 'Carlos Martinez', fkcontratista: 3 },
// ];

// const DataObra = [
//   { value: 1, label: "MIRASOL ETAPA 1(T4)205 APTOS+URB", dtestppae: "Si", dtestppav: "No", },
//   { value: 2, label: "VERDEVIVO CEIBA T2 128 AP+PQ-BASE", dtestppae: "No", dtestppav: "Si", },
//   { value: 3, label: "ALTOSVERDES ETAPA 1(T4)205 APTOS+URB", dtestppae: "Si", dtestppav: "No", },
// ]


// const DataEstadoUsuario = [
//   { value: "Activo", label: "Activo", icon: 'check-square-o', style: 'color:#00922A' },
//   { value: "Inactivo", label: "Inactivo", icon: 'ban', style: 'color:#FF0000' }
// ]

// const DataEstadosSolicitud = [
//   { value: 1, label: 'Pendiente por aprobar entrega' },
//   { value: 2, label: 'Pendiente por Entregar' },
//   { value: 3, label: 'Pendiente por Validar' },
//   { value: 4, label: 'Pendiente por aprobar validación' },
//   { value: 5, label: 'Pendiente por Enviar al Inventario' },
//   { value: 6, label: 'Error en Integración' },
//   { value: 7, label: 'Finalizada Sin Entrega' },
//   { value: 8, label: 'Finalizada' },
//   { value: 9, label: 'Anulada' },
//   { value: 10, label: 'Cancelada' },
// ];
// /**
//  * Datos de estado solicitud
//  */
// const DataEstado = [
//   { id: 1, value: "Pendiente por Aprobar Entrega", icon: 'flag', style: 'color:#FD0000; cursor:pointer', url: "/formaprobarentrega/" },
//   { id: 2, value: "Pendiente por Entregar", icon: 'exclamation', style: 'color:#e0dd00; cursor:pointer' },
//   { id: 3, value: "Pendiente por Validar", icon: 'check', style: 'color:#01A927; cursor:pointer' },//exclamation-triangle/check-square-o
//   { id: 4, value: "Pendiente por Aprobar Validación", icon: 'flag', style: 'color:#01A927; cursor:pointer' },
//   { id: 5, value: "Pendiente por Enviar al Inventario", icon: 'paper-plane', style: 'color:#01A927; cursor:pointer' },
//   { id: 6, value: "Error en Integración", icon: 'times-circle', style: 'color:#FD0000; cursor:pointer' },
//   { id: 7, value: "Finalizada Sin Entrega", icon: 'check-square-o', style: 'color:#01A927; cursor:pointer' },
//   { id: 8, value: "Finalizada", icon: 'check-square-o', style: 'color:#01A927; cursor:pointer' },
//   { id: 9, value: "Anulada", icon: 'circle', style: 'color:#FD0000; cursor:pointer' },
//   { id: 10, value: "Cancelada", icon: 'ban', style: 'color:#FD0000; cursor:pointer' },
// ]

/**
 * Menu 
 */
const DataMenuExterno = [
  { value: " Usuarios", id: "ModUsuario", url: "/formusuario/", icon: "fa fa-address-book-o", sg: false, subM: undefined },
  { value: " Acceso a Obras", id: "ModAccesoObra", url: "/formacceobra/", icon: "fa fa-building-o", sg: false, subM: undefined },
  { value: " Solicitud Materiales", id: "ModMaterial", url: "/formmaterial/", icon: "fa fa-wpforms", sg: false, subM: undefined },
  { value: " Parametrización Obra", id: "ModObraEstado", url: "/formobraestado/", icon: "fa fa-wpforms", sg: false, subM: undefined },
  { value: " Entrega de Materiales", id: "ModEntregaMaterial", url: "/formentregamaterial/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Validar", id: "ModPendienteValidar", url: "/formpendientevalidar/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Aprobar Entrega", id: "ModAprobarEntrega", url: "/formaprobarentrega/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Aprobar Validación", id: "ModAprobarValidacion", url: "/formaprobarvalidacion/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Enviar al Inventario", id: "ModEnviarInventario", url: "/formenviarinventario/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Obras", id: "ModObra", url: "/formobra/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Contratistas", id: "ModContratista", url: "/formcontratista/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Clasificaciones", id: "ModClasificacion", url: "/formclasificacion/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Capitulos", id: "ModCapitulo", url: "/formcapitulo/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Subcapitulos", id: "ModSubCapitulo", url: "/formsubcapitulo/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  // { value: " Centros de Costos", id: "ModCentroCosto", url: "/formcentrocosto/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Actividades", id: "ModCentroCosto", url: "/formcentrocosto/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Insumos", id: "ModInsumo", url: "/forminsumo/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Estados Solicitud", id: "ModEstadoSolicitud", url: "/formestadosolicitud/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Encargados Contratistas", id: "ModEContratista", url: "/formecontratista/", icon: "fa fa-check-square-o", sg: false, subM: undefined },
  { value: " Insumos Asociados", id: "ModIAsociado", url: "/formiasociados/", icon: "fa fa-check-square-o", sg: false, subM: undefined },

]
/**
 * Creación de token
 */
const secret = "appConaltura";

/**
 * Roles e ingreso al aplicativo
 */
const DataUsuarioLogueo = [
  { id: 1, user: "seguridad", pass: 'seguridad', rol: 'Admon', value: 1, label: 'seguridad' },
  { id: 2, user: "profesional", pass: 'profesional', rol: 'Profesional de Control', value: 1, label: 'profesional' },
  { id: 3, user: "residente", pass: 'residente', rol: 'Residente', value: 2, label: 'residente' },
  { id: 4, user: "almacenista", pass: 'almacenista', rol: 'Almacenista', value: 4, label: 'almacenista' },
]
/**
 * c = permiso de crear
 * r = permiso de lectura 
 * u = permiso de atualización/modificación
 * d = permiso de eliminación
 */
const DataModulosUsuario = [
  {
    rol: 'Profesional de Control', c: '1', r: '1', u: '1', d: '1', mod: [
      {
        value: "Maestros", id: "ModMaestros", url: "/formusuario/", icon: "fa fa-address-book-o",
        subM: [
          // { value: " Usuarios", id: "ModUsuario", url: "/formusuario/", icon: "fa fa-address-book-o" },
          // { value: " Obras", id: "ModObra", url: "/formobra/", icon: "fa fa-address-book-o" },
          // { value: " Parametrización Obra", id: "ModObraEstado", url: "/formobraestado/", icon: "fa fa-address-book-o" },
          { value: " Usuarios", id: "ModUsuario", url: "/formusuario/", icon: "fa fa-address-book-o" },
          { value: " Obras", id: "ModObra", url: "/formobra/", icon: "fa fa-address-book-o" },
          { value: " Parametrización Obra", id: "ModObraEstado", url: "/formobraestado/", icon: "fa fa-address-book-o" },
          { value: " Contratistas", id: "ModContratista", url: "/formcontratista/", icon: "fa fa-address-book-o" },
          { value: " Clasificaciones", id: "ModClasificacion", url: "/formclasificacion/", icon: "fa fa-address-book-o" },
          { value: " Capitulos", id: "ModCapitulo", url: "/formcapitulo/", icon: "fa fa-address-book-o" },
          { value: " Subcapitulos", id: "ModSubCapitulo", url: "/formsubcapitulo/", icon: "fa fa-address-book-o" },
          { value: " Actividades", id: "ModCentroCosto", url: "/formcentrocosto/", icon: "fa fa-address-book-o" },
          { value: " Insumos", id: "ModInsumo", url: "/forminsumo/", icon: "fa fa-address-book-o" },
          { value: " Estados Solicitud", id: "ModEstadoSolicitud", url: "/formestadosolicitud/", icon: "fa fa-check-square-o" },
          { value: " Encargados Contratistas", id: "ModEContratista", url: "/formecontratista/", icon: "fa fa-check-square-o" },
          { value: " Insumos Asociados", id: "ModIAsociado", url: "/formiasociados/", icon: "fa fa-check-square-o" },
          // { value: " Acceso a Obras", id: "ModAccesoObra", url: "/formacceobra/", icon: "fa fa-building-o" },
          { value: " Acceso a Obras", id: "ModAccesoObra", url: "/formacceobra/", icon: "fa fa-building-o" },
        ]
      },
      {
        value: " Solicitudes", id: "ModSolicitud", url: "/formsolicitud/", icon: "fa fa-wpforms",
        subM: [
          { value: " Solicitud Materiales", id: "ModMaterial", url: "/formmaterial/", icon: "fa fa-wpforms" },
          { value: " Aprobar Entrega", id: "ModAprobarEntrega", url: "/formaprobarentrega/", icon: "fa fa-check-square-o" },
          { value: " Entrega de Materiales", id: "ModEntregaMaterial", url: "/formentregamaterial/", icon: "fa fa-check-square-o" },
          { value: " Validar", id: "ModPendienteValidar", url: "/formpendientevalidar/", icon: "fa fa-check-square-o" },
          { value: " Aprobar Validación", id: "ModAprobarValidacion", url: "/formaprobarvalidacion/", icon: "fa fa-check-square-o" },
          { value: " Enviar al Inventario", id: "ModEnviarInventario", url: "/formenviarinventario/", icon: "fa fa-paper-plane" },
        ]
      },
    ],
  },
  {
    rol: 'Almacenista', c: '1', r: '1', u: '1', d: '0', mod: [
      {
        value: " Solicitudes", id: "ModSolicitud", url: "/formsolicitud/", icon: "fa fa-wpforms",
        subM: [
          { value: " Solicitud Materiales", id: "ModMaterial", url: "/formmaterial/", icon: "fa fa-wpforms" },
          { value: " Entrega de Materiales", id: "ModEntregaMaterial", url: "/formentregamaterial/", icon: "fa fa-check-square-o" },
          { value: " Enviar al Inventario", id: "ModEnviarInventario", url: "/formenviarinventario/", icon: "fa fa-paper-plane" },
        ]
      },
    ]
  },
  {
    rol: 'Admon', c: '0', r: '1', u: '0', d: '0', mod: [
      {
        value: "Maestros", id: "ModMaestros", url: "/formusuario/", icon: "fa fa-address-book-o",
        subM: [
          { value: " Usuarios", id: "ModUsuario", url: "/formusuario/", icon: "fa fa-address-book-o" },
          { value: " Obras", id: "ModObra", url: "/formobra/", icon: "fa fa-address-book-o" },
          { value: " Parametrización Obra", id: "ModObraEstado", url: "/formobraestado/", icon: "fa fa-address-book-o" },
          { value: " Acceso a Obras", id: "ModAccesoObra", url: "/formacceobra/", icon: "fa fa-building-o" },
        ]
      },
      {
        value: " Solicitudes", id: "ModSolicitud", url: "/formsolicitud/", icon: "fa fa-wpforms",
        subM: [
          { value: " Solicitud Materiales", id: "ModMaterial", url: "/formmaterial/", icon: "fa fa-wpforms" },
        ]
      },
    ]
  },
  {
    rol: 'Residente', c: '0', r: '1', u: '0', d: '0', mod: [
      {
        value: "Maestros", id: "ModMaestros", url: "/formusuario/", icon: "fa fa-address-book-o",
        subM: [
          { value: " Contratistas", id: "ModContratista", url: "/formcontratista/", icon: "fa fa-address-book-o" },
          { value: " Clasificaciones", id: "ModClasificacion", url: "/formclasificacion/", icon: "fa fa-address-book-o" },
          { value: " Subcapitulos", id: "ModSubCapitulo", url: "/formsubcapitulo/", icon: "fa fa-address-book-o" },
          { value: " Actividades", id: "ModCentroCosto", url: "/formcentrocosto/", icon: "fa fa-address-book-o" },
          { value: " Insumos", id: "ModInsumo", url: "/forminsumo/", icon: "fa fa-address-book-o" },
          { value: " Estados Solicitud", id: "ModEstadoSolicitud", url: "/formestadosolicitud/", icon: "fa fa-check-square-o" },
          { value: " Encargados Contratistas", id: "ModEContratista", url: "/formecontratista/", icon: "fa fa-check-square-o" },
          { value: " Insumos Asociados", id: "ModIAsociado", url: "/formiasociados/", icon: "fa fa-check-square-o" },
        ]
      },
      {
        value: " Solicitudes", id: "ModSolicitud", url: "/formsolicitud/", icon: "fa fa-wpforms",
        subM: [
          { value: " Solicitud Materiales", id: "ModMaterial", url: "/formmaterial/", icon: "fa fa-wpforms" },
          { value: " Aprobar Entrega", id: "ModAprobarEntrega", url: "/formaprobarentrega/", icon: "fa fa-check-square-o" },
          { value: " Entrega de Materiales", id: "ModEntregaMaterial", url: "/formentregamaterial/", icon: "fa fa-check-square-o" },
          { value: " Validar", id: "ModPendienteValidar", url: "/formpendientevalidar/", icon: "fa fa-check-square-o" },
          { value: " Aprobar Validación", id: "ModAprobarValidacion", url: "/formaprobarvalidacion/", icon: "fa fa-check-square-o" },
          { value: " Enviar al Inventario", id: "ModEnviarInventario", url: "/formenviarinventario/", icon: "fa fa-paper-plane" },
        ]
      },
    ]
  },
]
/**
 * Datos de pruebas
 */
// const DataListPrueba = [
//   {
//     id: 1, dtnumerosolicitud: "123", dtfecha: "09/10/2019", dtobra: "MIRASOL ETAPA 1(T4)205 APTOS+URB", dtencargado: "Encargado uno", dtcontratista: "Contratista uno", dtsolicitadopor: "Admin",
//     dtestado: "Pendiente por Entregar", dtobservacion: "", dtidobserva: 1,
//     DataListItems: [
//       { id: 1, formclassoli: "clas soli 1", formccostos: "costo 1", forminsumo: "insumo 1", formcantidad: "3", formcantidadentregada: "", formunidad: "Kg", },
//       { id: 2, formclassoli: "clas soli 2", formccostos: "costo 2", forminsumo: "insumo 2", formcantidad: "45", formcantidadentregada: "", formunidad: "Kg", },
//       { id: 3, formclassoli: "clas soli 3", formccostos: "costo 3", forminsumo: "insumo 3", formcantidad: "23", formcantidadentregada: "", formunidad: "Kg", },
//     ]
//   },
//   { id: 2, dtnumerosolicitud: "456", dtfecha: "29/10/2019", dtobra: "prueba2", dtencargado: "prueba2", dtcontratista: "prueba2", dtsolicitadopor: "Admin", dtestado: "Cancelada", dtobservacion: "", dtidobserva: 2, DataListItems: [{ id: 1, formclassoli: "clas soli 2", formccostos: "costo 2", forminsumo: "insumo 2", formcantidad: "4", formcantidadentregada: "", formunidad: "m", }] }
// ]

const DataObservacionPrueba = [
  { id: 1, dtidsolicitud: 1, dtfecha: "2019-09-01 10:05 am", dtsolicitadopor: "admin", dtobservacion: "Cambio de estado: Solicitada", },
  { id: 2, dtidsolicitud: 1, dtfecha: "2019-09-04 03:20 pm", dtsolicitadopor: "admin", dtobservacion: "Registro de solicitud: Modificada", },
  { id: 3, dtidsolicitud: 1, dtfecha: "2019-09-12 11:55 am", dtsolicitadopor: "almacenista", dtobservacion: "Cambio de estado: Entregada", },
  { id: 4, dtidsolicitud: 1, dtfecha: "2019-09-20 08:40 am", dtsolicitadopor: "encargado", dtobservacion: "Comentario: Se reciben los implementos imcompletos, ya que no se encontraron algunos en el inventario", },

  { id: 5, dtidsolicitud: 2, dtfecha: "2019-09-03 09:03 am", dtsolicitadopor: "Admin", dtobservacion: "Cambio de estado: Solicitada", },
  { id: 6, dtidsolicitud: 2, dtfecha: "2019-09-08 02:08 pm", dtsolicitadopor: "almacenista", dtobservacion: "Comentario: se solicita la revision por parte del aprobador de la entrega de manteriales para la obra", },
  { id: 7, dtidsolicitud: 2, dtfecha: "2019-09-13 04:50 pm", dtsolicitadopor: "almacenista", dtobservacion: "Cambio de estado: Entregada", },
]

module.exports = {
  // DataClasSoli,
  // DataCostos,
  // DataInsumo,
  // DataEncargado,
  // DataObra,
  DataMenuExterno,
  secret,
  // DataEstado,
  // DataListPrueba,
  // DataEstadoUsuario,
  DataUsuarioLogueo,
  DataModulosUsuario,
  // DataContratista,
  // DataEstadosSolicitud,
  DataObservacionPrueba,
  // DataCapitulo,
  // DataSubCapitulo,
  RutaApi,
}