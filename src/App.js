import React from 'react';
//import logo from './logo.svg';
import './App.css';
import Login from './components/login/Login';
import Menu from './components/menu/Menu';
import Herramientas from './components/inside/SeleccionarHerramienta';
import { secret } from './const/Index';
import jwt from 'jsonwebtoken';
import { connect } from 'react-redux';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      validMenu: false,
      validHerr: false,
      rol: null
    };
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleSignOut = this.handleSignOut.bind(this);
    this.onSignInMenu = this.onSignInMenu.bind(this);
  }

  handleSignIn(validLogin) {
    //Crear token para el usuario
    const payload = { "user": validLogin.user, };
    const scrt = secret;
    const token = jwt.sign({
      data: payload
    }, scrt, { expiresIn: '1m' });

    localStorage.setItem('jwtToken', token);
    this.setState({ valid: validLogin, validHerr: validLogin});
  }

  handleSignOut(validLogin) {
    //Limpiar token
    localStorage.setItem('jwtToken', undefined);
    this.setState({ valid: validLogin, validMenu: validLogin });
    //Limpiar los datos de 
    this.props.dispatch({
      type: 'CLEAN_LOGIN'
    })
  }
  onSignInMenu(){
    this.setState({validMenu: true,validHerr: false,});
  }
  // onRolUser={this.state.rol}
  render() {
    return (
      <div className="">
        {this.state.valid ?
          <div>
            {this.state.validHerr && (<Herramientas onSignInMenu={this.onSignInMenu}></Herramientas>)}
            {this.state.validMenu && (<Menu onSignOut={this.handleSignOut}></Menu>)}
          </div>
          :
          <Login onSingnIn={this.handleSignIn} />
        }
      </div>
    )
  }
}

export default connect()(App);
