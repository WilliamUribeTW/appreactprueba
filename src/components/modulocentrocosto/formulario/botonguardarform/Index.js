import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import './Index.css';
import Moment from 'moment';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import readXlsFile from 'read-excel-file';
import axios from 'axios';
import { RutaApi } from '../../../../const/Index';

class BotonGuardarForm extends React.Component {
    constructor(props) {
        super(props);
        this.dtacc = this.props.centrocostos;
        this.arrnoload = [];
        this.arrexportCCosto = [];
        this.arrinfo = [];
        this.state = {
            sw: false,
            btn: false,
            brrData: false,
            btnExport: false,
            title: "",
            msj: "",
        };
        this.handleCreateData = this.handleCreateData.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    handleCreateData(e) {
        const { NombreCentroCosto, FkSubCapitulo, NombreSubCapitulo, Estado, sw, CodCentroCosto, FkEstado } = this.props.onDataState;

        if (sw === true) {
            const input = document.getElementById('dtcentrocostofile');

            if (input.files.length === 0) {
                this.setState({ sw: true, brrData: false, })
                this.titleMsn = "Advertencia";
                this.textMsn = "Se debe seleccionar el archivo.";
            } else {
                this.setState({ sw: true, btn: true });
                this.titleMsn = "Carga";
                this.textMsn = "Cargando la informacion, por favor espere mientras termina el proceso...";
                let usr = this.props.user.map(dtuser => dtuser.dtuserid), subcap = this.props.subcapitulo, result = 0;
                readXlsFile(input.files[0]).then((rows) => {
                    rows.map(fil => {
                        this.getUsers();
                        if (fil[0] !== "Código Actividad") {
                            let contCC = this.dtacc.filter(c => c.CodCentroCosto === fil[0]);
                            /**
                             * Buscamos el id del subcapitudo con el código
                             */
                            let idsbcap = subcap.filter(sb => sb.CodSubCapitulo === fil[2] && sb.FkEstado === 1).map(e => e.value);
                            idsbcap = idsbcap[0];
                            if (contCC.length === 0) {
                                /**
                                 * Crear los datos
                                 * Validamos que los campos del excel no esten null
                                 */
                                if (!isNaN(fil[0])) {
                                    /**
                                     * Validamos que la consulta a los campos de la bd no esten vacios
                                     */
                                    if (!isNaN(idsbcap) && idsbcap !== undefined) {
                                        const dataCentroCosto = {
                                            IdCentroCosto: 0,
                                            CodCentroCosto: fil[0],
                                            NombreCentroCosto: fil[1],
                                            // FkSubCapitulo: fil[2],
                                            FkSubCapitulo: idsbcap,
                                            NombreSubCapitulo: fil[3],
                                            FkEstado: fil[4],
                                            Estado: fil[5],
                                            Cargue: "Masiva",
                                            FkUsuarioCreador: usr[0],
                                            FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),
                                        }
                                        /**
                                         * Guardar la informacón en la base de datos
                                         */
                                        axios.post(RutaApi + 'CentroCosto', JSON.stringify(dataCentroCosto))
                                            .then(res => {
                                                //console.log(res.data);
                                                result = res.data;
                                            })
                                            .catch(error => {
                                                //console.log(error.response);
                                                this.titleMsn = "Error";
                                                this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                                            });
                                    } else {
                                        const dataCentroCosto = {
                                            CodCentroCosto: fil[0],
                                            NombreCentroCosto: fil[1],
                                            FkSubCapitulo: fil[2],
                                            // FkSubCapitulo: idsbcap,
                                            NombreSubCapitulo: fil[3],
                                            FkEstado: fil[4],
                                            Estado: fil[5],
                                        }
                                        this.arrnoload.push(dataCentroCosto);
                                    }
                                } else {
                                    const dataCentroCosto = {
                                        CodCentroCosto: fil[0],
                                        NombreCentroCosto: fil[1],
                                        FkSubCapitulo: fil[2],
                                        // FkSubCapitulo: idsbcap,
                                        NombreSubCapitulo: fil[3],
                                        FkEstado: fil[4],
                                        Estado: fil[5],
                                    }
                                    this.arrnoload.push(dataCentroCosto);
                                }
                            } else {
                                /**
                                 * Modificar los datos
                                 * Validamos que los campos del excel no esten null
                                 */
                                if (!isNaN(fil[0])) {
                                    /**
                                     * Validamos que la consulta a los campos de la bd no esten vacios
                                     */
                                    if (!isNaN(idsbcap) && idsbcap !== undefined) {
                                        const dataCentroCosto = {
                                            IdCentroCosto: 1,
                                            CodCentroCosto: fil[0],
                                            NombreCentroCosto: fil[1],
                                            // FkSubCapitulo: fil[2],
                                            FkSubCapitulo: idsbcap,
                                            NombreSubCapitulo: fil[3],
                                            FkEstado: fil[4],
                                            Estado: fil[5],
                                            Cargue: "Masiva",
                                            FkUsuarioModificador: usr[0],
                                            FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                                        }
                                        /**
                                        * Modificar la informacón en la base de datos
                                        */
                                        axios.post(RutaApi + 'CentroCosto', JSON.stringify(dataCentroCosto))
                                            .then(res => {
                                                result = res.data;
                                            })
                                            .catch(error => {
                                                //console.log(error.response);
                                                this.titleMsn = "Error";
                                                this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                                            });
                                    } else {
                                        const dataCentroCosto = {
                                            CodCentroCosto: fil[0],
                                            NombreCentroCosto: fil[1],
                                            FkSubCapitulo: fil[2],
                                            // FkSubCapitulo: idsbcap,
                                            NombreSubCapitulo: fil[3],
                                            FkEstado: fil[4],
                                            Estado: fil[5],
                                        }
                                        this.arrnoload.push(dataCentroCosto);
                                    }
                                }else {
                                    const dataCentroCosto = {
                                        CodCentroCosto: fil[0],
                                        NombreCentroCosto: fil[1],
                                        FkSubCapitulo: fil[2],
                                        // FkSubCapitulo: idsbcap,
                                        NombreSubCapitulo: fil[3],
                                        FkEstado: fil[4],
                                        Estado: fil[5],
                                    }
                                    this.arrnoload.push(dataCentroCosto);
                                }
                            }
                            return contCC;

                        } else {
                            return null;
                        }
                    })
                })
                setTimeout(() => { this.handleShowMsj(result); }, 700);
            }
        } else {
            if (NombreCentroCosto === "" || NombreSubCapitulo === "" || Estado === "" || CodCentroCosto === "") {
                this.setState({ sw: true, brrData: false, title: "Advertencia", msj: "Se debe diligenciar el formulario completo." })
            } else {
                //Agregar informacion a store de 
                this.setState({ sw: false, });
                let usr = this.props.user.map(dtuser => dtuser.dtuserid);
                let validccosto = this.props.centrocostos.filter(c => c.CodCentroCosto === CodCentroCosto).map(c => c.id);
                if (validccosto.length > 0) {
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado.";
                    this.setState({ sw: true, brrData: false, });
                } else {
                    const dataCentroCosto = {
                        //id: cont,
                        NombreCentroCosto: NombreCentroCosto,
                        FkSubCapitulo: FkSubCapitulo,
                        NombreSubCapitulo: NombreSubCapitulo,
                        FkEstado: FkEstado,
                        Estado: Estado,
                        CodCentroCosto: CodCentroCosto,
                        Cargue: "Manual",
                        FkUsuarioCreador: usr[0],//this.props.onDataState.formcreadopor,
                        FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),//this.props.onDataState.formfechacreacion,
                    }

                    /**
                     * Guardar la informacón en la base de datos
                     */
                    let result = 0;
                    axios.post(RutaApi + 'CentroCosto', JSON.stringify(dataCentroCosto))
                        .then(res => {
                            result = res.data;
                        })
                        .catch(error => {
                            //console.log(error.response);
                            this.titleMsn = "Error";
                            this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                        });
                    setTimeout(() => { this.handleShowMsj(result); }, 700);
                }

            }
        }
    }

    handleShowMsj = async (result) => {
        this.handleChange();
        if (this.arrnoload.length > 0) {
            /**
             * Se crea una lista, para poder agregar la información del maestro
             */
            let dt = [];
            this.arrnoload.forEach(element => {
                const inf = [element.CodCentroCosto, element.NombreCentroCosto, element.CodSubCapitulo, element.NombreSubCapitulo, element.FkEstado, element.Estado];
                dt.push(inf);
            });
            /**
             * Si la lista no tiene información, se le agrega los campos vacios, 
             * para que se visualice de manera correcta el formato
             */
            if (dt.length <= 0) {
                dt.push(["", "", "", "", ""]);
            }
            this.arrexportCCosto = [
                {
                    columns: ["Código Actividad", "Nombre Actividad", "Código SubCapitulo", "Nombre SubCapitulo", "Código Estado", "Estado"],
                    data: dt
                },
            ];

            // // const arrinfo = [
            this.arrinfo = [
                {
                    columns: ["Instrucciones para subir el archivo por Carga Masiva"],
                    data: [
                        ["- Ir a la Hoja FormatoActividades."],
                        ["- Borrar los registros que no se necesiten actualizar."],
                        ["- Diligenciar las columnas Código Actividad, Nombre Actividad, Código SubCapitulo y Código Estado."],
                        ["- La columna Nombre SubCapitulo y Estado son informativos, no debes diligenciarlos para la carga."],
                        ["- Tener en cuenta que el codigo del Estado es: 1 = Activo y 2 = Inactivo."],
                        ["- Eliminar esta hoja (Instrucciones),en el archivo de excel solo debe existir la hoja (FormatoActividades)."],
                    ]
                }
            ];
            this.setState({ btnExport: true });
        }
        // Validamos la variable para poder mostrar el mensaje
        if (parseInt(result) === parseInt(0)) {
            this.titleMsn = "Advertencia";
            this.textMsn = "Se presento un inconveniente al insertar la información.";
        } else {
            this.titleMsn = "Registro exitoso";
            this.textMsn = "Se registro la información";

            /**
             * Se limpia los datos para que no se dupliquen cada ves que se cargue
             */
            this.props.dispatch({
                type: 'CLEAN_CENTROCOSTO'
            });
            this.props.dispatch({
                type: 'CLEAN_LISTACENTROCOSTO'
            });
            /**
             * Cargamos la informacion para actualizar el redux
             */
            let res = await axios.get(RutaApi + 'CentroCosto');
            for (let index = 0; index < res.data.length; index++) {
                const element = res.data[index];
                const dataCentroCosto = {
                    id: element.IdCentroCosto,
                    CodCentroCosto: element.CodCentroCosto,
                    CodCentroCostoOld: element.CodCentroCosto,
                    NombreCentroCosto: element.NombreCentroCosto,
                    FkSubCapitulo: element.FkSubCapitulo,
                    NombreSubCapitulo: element.NombreSubCapitulo,
                    CodSubCapitulo: element.CodSubCapitulo,
                    FkEstado: element.FkEstado,
                    Estado: element.Estado,
                    Cargue: element.Cargue,
                    FkUsuarioCreador: element.FkUsuarioCreador,
                    UsuarioCreador: element.UsuarioCreador,
                    FechaCreacion: element.FechaCreacion,
                    FkUsuarioModificador: element.FkUsuarioModificador,
                    UsuarioModificador: element.UsuarioModificador,
                    FechaModificacion: element.FechaModificacion,
                }

                this.props.dispatch({
                    type: 'ADD_CENTROCOSTO',
                    dataCentroCosto
                });
                /**
                 * Cargar listado desplegable
                 */
                if (parseInt(element.FkEstado) !== parseInt(2)) {
                    const dataListaCentroCosto = {
                        value: element.IdCentroCosto,
                        label: element.NombreCentroCosto,
                        codCC: element.CodCentroCosto,
                        // idsubcapitulo: element.FkContratista,
                    }
                    this.props.dispatch({
                        type: 'ADD_LISTACENTROCOSTO',
                        dataListaCentroCosto
                    });
                }
            }
        }
        this.setState({ sw: true, show: true, brrData: true, }, function () {
            this.props.onCreate(this.state.brrData);
        });
    }
    getUsers = async () => {
        let res = await axios.get(RutaApi + 'CentroCosto');
        let { data } = res.data;
        this.dtacc = data;
    };

    render() {
        return (
            <div>
                <br></br>
                <div className="divBoton">
                    <Button variant="primary" type="button" onClick={() => this.props.onCancel()} disabled={this.state.btn}>Volver</Button>
                    <Button variant="primary" type="submit" onClick={this.handleCreateData} disabled={this.state.btn}>Guardar</Button>
                </div>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        centrocostos: state.CentroCostos,
        user: state.Login,
        subcapitulo: state.ListaSubcapitulos,
    }
}

export default connect(mapStateToProps)(BotonGuardarForm);