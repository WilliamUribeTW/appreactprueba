import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { DataEstadoUsuario, DataSubCapitulo } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';
import CampoInput from '../../campoinput/Index';
import CampoRadio from '../../camporadio/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      NombreCentroCosto: "",
      FkSubCapitulo: "",
      NombreSubCapitulo: "",
      CodCentroCosto: "",
      Estado: "",
      FkEstado: "",
      Cargue: "Manual",
      dtcentrocostofile: null,
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
      sw: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childestado = React.createRef();
    this.childsubcapitulo = React.createRef();
  }

  handleInput(e) {
    const { value, name, id } = e;
    if (name === "NombreSubCapitulo") { this.setState({ FkSubCapitulo: id }); }
    if (name === "Estado") { console.log(id);this.setState({ FkEstado: id });}
    if (name === "Cargue") { if (value === "Masiva") { this.setState({ sw: true }); } else { this.setState({ sw: false }); } }
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {
      
      if(this.state.sw === false){
        /**
         * Se llama la función del componente 
         */
        this.childestado.current.handleClick();
        this.childsubcapitulo.current.handleClick()
      };

      this.setState({
        NombreCentroCosto: "",
        FkSubCapitulo: "",
        NombreSubCapitulo: "",
        CodCentroCosto: "",
        Estado: "",
        FkEstado: "",
        Cargue: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: "",
        sw: false,
      });

    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoRadio onResult={this.handleInput} name="Cargue" onChecked={this.state.sw}></CampoRadio>
          {this.state.sw ?
            <CampoInput onResult={this.handleInput} type="file" placeholder="Archivo" name="dtcentrocostofile" controlId="dtcentrocostofile" id="dtcentrocostofile"></CampoInput>
            :
            <div>
              <CampoInput onResult={this.handleInput} value={this.state.NombreCentroCosto} onRequired={true} onDisabled={false} type="text" placeholder="Actividad" name="NombreCentroCosto" controlId="NombreCentroCosto"></CampoInput>
              <CampoInput onResult={this.handleInput} value={this.state.CodCentroCosto} onRequired={true} onDisabled={false} type="number" placeholder="Código Actividad" name="CodCentroCosto" controlId="CodCentroCosto"></CampoInput>
              <DataListInput dataselect={this.props.listsubcap} onSelectAuto={this.handleInput} valueText={this.state.NombreSubCapitulo} ref={this.childsubcapitulo} placeholder="Seleccione el subcapitulo" name="NombreSubCapitulo" list="NombreSubCapitulo" onRequired={true}>
              </DataListInput>
              <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.Estado} ref={this.childestado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
              </DataListInput>
            </div>
          }

          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
    return {
        centrocostos: state.CentroCostos,
        lg: state.Login,
        estado: state.Estado,
        listsubcap: state.ListaSubcapitulos,
    }
}
export default connect(mapStateToProps)(FormularioGeneral);