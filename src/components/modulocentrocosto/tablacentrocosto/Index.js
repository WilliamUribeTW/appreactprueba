import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { /*DataEstadoUsuario, DataSubCapitulo,*/ RutaApi } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import Moment from 'moment';
import ExportarInfo from '../../botonexportar/Index';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaCentroCosto extends React.Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            sw: false,
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        /**
         * Array para la exportacion del formato de excel
         */
        // const arrexportCCosto = [
        //     // { label: "Id", value: "id", id: "1" },
        //     { label: "Código Actividad", value: "CodCentroCosto", id: "1", style: "" },
        //     { label: "Nombre Actividad", value: "NombreCentroCosto", id: "2", style: "" },
        //     { label: "Código SubCapitulo", value: "CodSubCapitulo", id: "3", style: "" },
        //     { label: "Nombre SubCapitulo", value: "NombreSubCapitulo", id: "4", style: "" },
        //     { label: "Código Estado", value: "FkEstado", id: "5", style: "" },
        //     { label: "Estado", value: "Estado", id: "6", style: { font: { color: { rgb: "FD2623" } } } },
        // ];
        /**
         * Se crea una lista, para poder agregar la información del maestro
         */
        let dt = [];
        this.props.centrocostos.forEach(element => {
            const inf = [element.CodCentroCosto, element.NombreCentroCosto, element.CodSubCapitulo, element.NombreSubCapitulo, element.FkEstado, element.Estado];
            dt.push(inf);
        });
        /**
         * Si la lista no tiene información, se le agrega los campos vacios, 
         * para que se visualice de manera correcta el formato
         */
        if(dt.length <= 0){
            dt.push(["","", "", "", ""]);
        }
        const arrexportCCosto = [
            { 
                columns: ["Código Actividad","Nombre Actividad","Código SubCapitulo", "Nombre SubCapitulo", "Código Estado", "Estado"],
                data: dt
            },
        ];
        /**
         * Array para dar las instrucciones de como actualizar el excel
         */
        const arrinfo = [
            {
                columns: ["Instrucciones para subir el archivo por Carga Masiva"],
                data: [
                    ["- Ir a la Hoja FormatoActividades."],
                    ["- Borrar los registros que no se necesiten actualizar."],
                    ["- Diligenciar las columnas Código Actividad, Nombre Actividad, Código SubCapitulo y Código Estado."],
                    ["- La columna Nombre SubCapitulo y Estado son informativos, no debes diligenciarlos para la carga."],
                    ["- Tener en cuenta que el codigo del Estado es: 1 = Activo y 2 = Inactivo."],
                    ["- Eliminar esta hoja (Instrucciones),en el archivo de excel solo debe existir la hoja (FormatoActividades)."],
                ]
            }
        ]; 
        return (
            <div>
                {parseInt(crud[0].dtc) !== parseInt(0) && <div>
                    <ExportarInfo onData={this.props.centrocostos} onName="FormatoActividades" onColumn={arrexportCCosto} onInfo={arrinfo}></ExportarInfo>
                    {/* <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear Centros de Costos</button> */}
                    <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear Actividad</button>
                </div>}

            </div>
        );
    }
    /**
     * Ir a crear
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_CENTROCOSTO',
                id: items[i]
            })
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), valcap = 0, valest = 0, valcargue = "";
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'CentroCosto');
        let dt = res.data.filter(f => f.IdCentroCosto === row.id);
        dt = dt[0];

        if (cellName === "NombreSubCapitulo") { valcap = this.props.listsubcap.filter(c => c.label === cellValue).map(c => c.value); valcap = valcap[0]; } else { valcap = row.FkSubCapitulo }
        if (cellName === "Estado") { valest = this.props.estado.filter(c => c.label === cellValue).map(c => c.value); valest = valest[0] } else { valest = row.FkEstado }
        if (row.Cargue === "") { valcargue = "Manual"; } else { valcargue = row.Cargue; }

        if (valcap === row.FkSubCapitulo && valest === row.FkEstado && dt.CodCentroCosto === row.CodCentroCosto && dt.NombreCentroCosto === row.NombreCentroCosto) {
            // console.log("no actualizar");
        } else {
            if (cellName === "CodCentroCosto") {
                /**
                 * Validar que el codigo no este repetido
                 */
                let codccosto = res.data.filter(f => f.CodCentroCosto === row.CodCentroCosto);
                if (codccosto.length > 0) {
                    row.CodCentroCosto = row.CodCentroCostoOld;
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado";
                    this.setState({ sw: true, });
                } else {
                    const dataCentroCosto = {
                        IdCentroCosto: row.id,
                        CodCentroCosto: row.CodCentroCosto,
                        NombreCentroCosto: row.NombreCentroCosto,
                        FkSubCapitulo: valcap,
                        NombreSubCapitulo: row.NombreSubCapitulo,
                        FkEstado: valest,
                        Estado: row.Estado,
                        Cargue: valcargue,
                        FkUsuarioModificador: usr[0],
                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                    }
                    /**
                     * Modificar la informacón en la base de datos
                     */
                    axios.post(RutaApi + 'CentroCosto', JSON.stringify(dataCentroCosto))
                        .then(res => {
                            // console.log(res.data);
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                        });
                    this.onLoadData();
                }
            } else {
                /**
                 * Validar que el codigo no este repetido
                 */
                let codccosto = res.data.filter(f => f.CodCentroCosto === row.CodCentroCosto);
                if (codccosto.length > 0) {
                    row.CodCentroCosto = row.CodCentroCostoOld;
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado";
                    this.setState({ sw: true, });
                } else {
                    const dataCentroCosto = {
                        IdCentroCosto: row.id,
                        CodCentroCosto: row.CodCentroCosto,
                        NombreCentroCosto: row.NombreCentroCosto,
                        FkSubCapitulo: valcap,
                        NombreSubCapitulo: row.NombreSubCapitulo,
                        FkEstado: valest,
                        Estado: row.Estado,
                        Cargue: valcargue,
                        FkUsuarioModificador: usr[0],
                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                    }
                    /**
                     * Modificar la informacón en la base de datos
                     */
                    axios.post(RutaApi + 'CentroCosto', JSON.stringify(dataCentroCosto))
                        .then(res => {
                            // console.log(res.data);
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                        });
                    this.onLoadData();
                }

            }
        }
    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }
    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_CENTROCOSTO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTACENTROCOSTO'
        });

        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'CentroCosto');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataCentroCosto = {
                id: element.IdCentroCosto,
                CodCentroCosto: element.CodCentroCosto,
                CodCentroCostoOld: element.CodCentroCosto,
                NombreCentroCosto: element.NombreCentroCosto,
                FkSubCapitulo: element.FkSubCapitulo,
                NombreSubCapitulo: element.NombreSubCapitulo,
                CodSubCapitulo: element.CodSubCapitulo,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                Cargue: element.Cargue,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_CENTROCOSTO',
                dataCentroCosto
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaCentroCosto = {
                    value: element.IdCentroCosto,
                    label: element.NombreCentroCosto,
                    codCC: element.CodCentroCosto,
                    // idsubcapitulo: element.FkContratista,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACENTROCOSTO',
                    dataListaCentroCosto
                });
            }
        }

        setTimeout(() => { this.onChangeLoading(); }, 300);
    };

    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataCc = this.props.centrocostos;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataCc} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkSubCapitulo" hidden={true}># CodSubCapitulo</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkEstado" hidden={true}># CodEstado</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodCentroCostoOld" hidden={true}># CodCentroCostoOld</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodSubCapitulo" hidden={true}># CodCentroCostoOld</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreCentroCosto" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Actividades</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodCentroCosto"  dataSort={ true }editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Código Actividad</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreSubCapitulo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.listsubcap.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Subcapitulo</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }} thStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }}>Estado</TableHeaderColumn>
                        <TableHeaderColumn dataField="Cargue" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }} thStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }}>Cargue</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>

        )
    }
}
const mapStateToProps = (state) => {
    return {
        centrocostos: state.CentroCostos,
        lg: state.Login,
        estado: state.Estado,
        listsubcap: state.ListaSubcapitulos,
    }
}

export default connect(mapStateToProps)(TablaCentroCosto);