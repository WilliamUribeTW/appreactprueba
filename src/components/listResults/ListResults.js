import React from 'react';
import { Card } from 'react-bootstrap';

class ListResults extends React.Component {
    constructor(props) {
        super(props);
        //console.log(props.result);
        this.handleInter = this.handleInter.bind(this);
    }

    handleInter(e) {
        const mn = e.target.getAttribute('name');
        this.props.onGotInter(mn);
    }

    render() {
        return (
            this.props.result.map(dataResult => {
                return (
                    <Card style={{ width: '18rem', display: "inline-block", margin: "1rem" }} key={dataResult.id + 1}>
                        <Card.Body key={dataResult.id + 2}>
                            <Card.Title key={dataResult.id + 3}>{dataResult.titulo}</Card.Title>
                            <Card.Text key={dataResult.id + 4}>{dataResult.value}</Card.Text>
                            <Card.Link href="#" key={dataResult.id + 5} onClick={this.handleInter} name={dataResult.inter}>Ir</Card.Link>
                        </Card.Body>
                    </Card>
                )
            })
        )
    }
}

export default ListResults;
