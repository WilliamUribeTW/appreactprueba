import React from 'react';
import Formulario from './formulario/Index';
import TablaAccesoObra from './tablaaccesoobra/Index';

class ModAccesoObra extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      swD:true
    }
    this.handleCreateSoli = this.handleCreateSoli.bind(this);
    this.handleCancelSoli = this.handleCancelSoli.bind(this);
  }

  handleCreateSoli(){
    this.setState({swD:false});
  }
  handleCancelSoli(){
    this.setState({swD:true});
  }
  render() {
    return (
      <div className="container">
        {this.state.swD ? <TablaAccesoObra onCreate={this.handleCreateSoli}></TablaAccesoObra>:<Formulario onCancel={this.handleCancelSoli}></Formulario>}
      </div>
    )
  }
}

export default ModAccesoObra;