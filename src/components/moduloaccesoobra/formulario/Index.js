import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { /*DataEncargado, DataObra, DataUsuarioLogueo */} from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      FkObra: "",
      NombreObra: "",
      FkUsuario: "",
      Usuario: "",
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
      dtusuario: [],
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childobra = React.createRef();
    this.childusuario = React.createRef();
  }

  handleInput(e) {
    const { value, name, id } = e;
    if(name === "NombreObra"){ this.setState({FkObra: id});}
    if(name === "Usuario"){ this.setState({FkUsuario: id});}
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {
      this.setState({
        FkObra: "",
        NombreObra: "",
        FkUsuario: "",
        Usuario: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: ""
      });

      /**
       * Se llama la función del componente 
       */
      this.childobra.current.handleClick();
      this.childusuario.current.handleClick();
    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <DataListInput dataselect={this.props.listobra} onSelectAuto={this.handleInput} valueText={this.state.NombreObra} ref={this.childobra} placeholder="Seleccione la Obra" name="NombreObra" list="NombreObra" onRequired={true}>
          </DataListInput>
          <DataListInput dataselect={this.props.listusuaurio} onSelectAuto={this.handleInput} valueText={this.state.Usuario} ref={this.childusuario} placeholder="Selecione el Usuario" name="Usuario" list="Usuario" onRequired={true}>
          </DataListInput>
          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    listobra: state.ListaObra,
    listusuaurio: state.ListaUsuarios,
  }
}

export default connect(mapStateToProps)(FormularioGeneral);