import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { RutaApi } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaAccesoObra extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sw: false,
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        return (
            <div>
                {parseInt(crud[0].dtd) !== parseInt(0) && <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar item</button>}
                {parseInt(crud[0].dtc) !== parseInt(0) && <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear acceso a obra</button>}
            </div>
        );
    }
    /**
     * Ir a crear acceso a obra
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            const dataAccObra = {
                IdAccesoObra: items[i],
                eliminar: 1,
            }
            /**
             * ELiminar la informacón en la base de datos
             */
            axios.post(RutaApi + 'AccesoObra', JSON.stringify(dataAccObra))
                .then(res => {
                    // console.log(res.data);
                })
                .catch(error => {
                    console.log("Se presento un error; se detalla a continuación " + error.response);
                });
        }
        this.UNSAFE_componentWillMount();
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), idobra = 0, idusr = 0;

        if (cellName === "NombreObra") { idobra = this.props.lstobra.filter(e => e.label === cellValue).map(e => e.value); idobra = idobra[0]; } else { idobra = row.FkObra }
        if (cellName === "Usuario") { idusr = this.props.lstusuario.filter(e => e.label === cellValue).map(e => e.value); idusr = idusr[0]; } else { idusr = row.FkUsuario }

        if (idobra === row.FkObra && idusr === row.FkUsuario) {
            // console.log("no actualizar");
        } else {
            /**
             * Realizamos la consulta a la BD nuevamente para validar 
             * si uno de los campos de la tabla que se le muestra al usuario 
             * fue actualizado
             */
            let res = await axios.get(RutaApi + 'AccesoObra');
            let dt = res.data.filter(f => f.FkObra === idobra && f.FkUsuario === idusr);
            if (dt.length > 0) {
                row.Usuario = row.UsuarioOld;
                row.NombreObra = row.NombreObraOld;
                this.titleMsn = "Advertencia";
                this.textMsn = "El usuario ya tiene asociado la Obra";
                this.setState({ sw: true, });
            } else {
                const dataAccObra = {
                    IdAccesoObra: row.id,
                    FkObra: idobra,
                    FkUsuario: idusr,
                    FkUsuarioModificador: usr[0],
                    FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                }
                /**
                 * Modificar la informacón en la base de datos
                 */
                axios.post(RutaApi + 'AccesoObra', JSON.stringify(dataAccObra))
                    .then(res => {
                        // console.log(res.data);
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                    });
                // this.onLoadData();
                this.UNSAFE_componentWillMount();
            }

        }
    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }


    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_ACCEOBRA'
        });

        setTimeout(() => { this.getData(); }, 100);
    }
    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {

        let res = await axios.get(RutaApi + 'AccesoObra');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataAccObra = {
                id: element.IdAccesoObra,
                NombreObra: element.NombreObra,
                NombreObraOld: element.NombreObra,
                FkObra: element.FkObra,
                FkUsuario: element.FkUsuario,
                Usuario: element.Usuario,
                UsuarioOld: element.Usuario,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_ACCEOBRA',
                dataAccObra
            });
        }

        setTimeout(() => { this.onChangeLoading(); }, 300);
    };
    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataAO = this.props.accobra;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        const selectRow = {
            mode: 'checkbox',
            onSelect: this.onSelectDelete

        };
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataAO} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} selectRow={selectRow} deleteRow>
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkObra" hidden={true}># CodObra</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkUsuario" hidden={true}># CodUser</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObraOld" hidden={true}># CodUser</TableHeaderColumn>
                        <TableHeaderColumn dataField="UsuarioOld" hidden={true}># UsuarioOld</TableHeaderColumn>
                        <TableHeaderColumn dataField="Usuario" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.lstusuario.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Usuario</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObra" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.lstobra.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Obra</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        accobra: state.AccObra,
        lg: state.Login,
        lstusuario: state.ListaUsuarios,
        lstobra: state.ListaObra,
    }
}


export default connect(mapStateToProps)(TablaAccesoObra);