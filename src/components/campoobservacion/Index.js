import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import Moment from 'moment';
// import axios from "axios";

const Observaciones = React.forwardRef((props, ref) => {
    return <Observacion {...props} innerRef={ref}></Observacion>
});
class Observacion extends React.Component {
    constructor(prop) {
        super(prop);
        this.state = {
            name: "",
            open: true,
            msg: "",
            color: "",
        };
        this.updateData = this.updateData.bind(this);
    }
    updateData() {
        const dt = this.state.name;
        if (dt === "") {
            this.setState({ msg: "Se debe diligenciar el campo de observación", color: "red" });
            //alert("Se debe diligenciar el campo de observación");
        } else {
            let data = { "valueOb": dt, "idOb": this.props.defaultValue }
            this.props.onChangeObser(data);
            this.setState({ name: "", msg: "Se guardo la observación de manera correcta", color: "green"});
        }
    }
    close = () => {
        this.setState({ open: false });
        this.props.onUpdate(this.props.defaultValue);
    }
    render() {
        let dtObs = this.props.onObser, dtIdsol = this.props.defaultValue, swmuest = false;
        switch (this.props.row.dtestado) {
            case "Finalizada Sin Entrega":
                swmuest = true;
                break;
            case "Finalizada":
                swmuest = true;
                break;
            case "Anulada":
                swmuest = true;
                break;
            case "Cancelada":
                swmuest = true;
                break;
            default:
                break;
        }
        // Se realiza el filtro para mostrar solo el seleccionado
        let dtsw = dtObs.filter(p => p.FkSolicitudMaterial === dtIdsol).map(p => p);
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton,
            expandBy: 'column',//Esto es para saber que columnas son expandable true o false
        };
        return (
            <Modal show={this.state.open} ref={this.props.innerRef} onHide={this.close} size="xl">
                <Modal.Body>
                    <textarea
                        className={(this.props.editorClass || '') + ' form-control editor edit-text'}
                        style={{ display: 'inline', width: '100%' }}
                        value={this.state.name}
                        onChange={e => { this.setState({ name: e.currentTarget.value }); }}
                        disabled={swmuest}
                    >
                    </textarea>
                    <label style={{ display: 'block', width: '100%', color: this.state.color }}>{this.state.msg}</label>
                    <BootstrapTable data={dtsw} search={true} pagination={true} options={options}>
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FechaCreacion" editable={false} tdStyle={{ whiteSpace: "normal", width: "17%" }} thStyle={{ whiteSpace: "normal", width: "17%" }} expandable={false}>Fecha</TableHeaderColumn>
                        <TableHeaderColumn dataField="Usuario" editable={false} tdStyle={{ whiteSpace: "normal", width: "11%" }} thStyle={{ whiteSpace: "normal", width: "11%"  }} expandable={false}>Usuario</TableHeaderColumn>
                        <TableHeaderColumn dataField="Observaciones" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Descripción</TableHeaderColumn>
                    </BootstrapTable>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.close}>Cerrar</Button>
                    <Button onClick={this.updateData} disabled={swmuest}>Guardar</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default Observaciones;