import React from "react";
import ReactExport from "react-export-excel";
import './Index.css';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
// const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class ExportarInfo extends React.Component {

    render() {
        let cl = this.props.onColumn;
        return (
            <ExcelFile element={<button className="botonExportar" style={{ marginLeft: '2px' }} >Descargar formato</button>} filename={this.props.onName}>
                <ExcelSheet dataSet={this.props.onInfo} name="Instrucciones" />
                <ExcelSheet dataSet={cl} name={this.props.onName} />
                {/* <ExcelSheet data={this.props.onData} name={this.props.onName}>
                    {
                        cl.map(c => {
                            return <ExcelColumn label={c.label} value={c.value} key={c.id} style={{font: {sz: "18", bold: true}}} />;
                        })
                    }
                </ExcelSheet> */}
            </ExcelFile>
        );
    }
}

export default ExportarInfo;