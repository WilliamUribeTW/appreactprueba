import React from 'react';
import Webix from '../../utils/webix/Index';


function getUI(select){
  return {
    view:"datatable", 
    scroll:false, 
    //width:400,
    autowidth:true, 
    autoheight:true, 
    select:true, 
    columns:[
      { id:"id", header: "id", fillspace:1 },
      { id:"dtidusuario", header: "id", fillspace:1 },
      { id:"dtidobra", header: "id", fillspace:1 },
      { id:"dtcreadopor", header: "id", fillspace:1 },
      { id:"dtfechacreacion", header: "id", fillspace:1 },
      { id:"dtmodificadopor", header: "id", fillspace:1 },
      { id:"dtfechamodificacion", header: "id", fillspace:1 },
    ],
    on:{
      onAfterSelect:function(id){
        select(id);
      }
    }
  };
}

const TableView = ({ data, select }) => (
  <Webix ui={getUI(select)} data={data} />
)

export default TableView;