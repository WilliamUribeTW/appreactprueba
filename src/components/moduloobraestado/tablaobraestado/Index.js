import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { /*DataObra,*/ RutaApi } from '../../../const/Index';
//import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaObraEstado extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        // let usr = this.props.lg.map(dtuser => dtuser.dtuserid);
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'ParametrizacionObra');
        let dt = res.data.filter(f => f.IdObra === row.id);
        dt = dt[0];

        if(dt.PendienteAprobarEntrega === row.PendienteAprobarEntrega && dt.PendienteAprobarValidacion === row.PendienteAprobarValidacion){
            // console.log("no actualizar");
        }else{
            const dataAccObra = {
                IdObra: row.id,
                PendienteAprobarEntrega: row.PendienteAprobarEntrega,
                PendienteAprobarValidacion: row.PendienteAprobarValidacion,
            }
    
            /**
             * Modificar la informacón en la base de datos
             */
            axios.post(RutaApi + 'ParametrizacionObra', JSON.stringify(dataAccObra))
                .then(res => {
                    // console.log(res.data);
                })
                .catch(error => {
                    console.log("Se presento un error; se detalla a continuación " + error.response);
                });
            // this.onLoadData();
            this.UNSAFE_componentWillMount();

        }
    }
    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        // const handler = e => this.setState({ matches: e.matches });
        // window.matchMedia("(max-width: 1300px)").addListener(handler);
        // if (window.innerWidth < 1280) {
        //     this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        // } else {
        //     this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        // }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_ACCEOBRA'
        });
        
        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'ParametrizacionObra');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataAccObra = {
                id: element.IdObra,
                // CodObra: element.CodObra,
                NombreObra: element.NombreObra,
                // FkEstado: element.FkEstado,
                // Estado: element.Estado,
                // FkUsuarioCreador: element.FkUsuarioCreador,
                // UsuarioCreador: element.UsuarioCreador,
                // FechaCreacion: element.FechaCreacion,
                // FkUsuarioModificador: element.FkUsuarioModificador,
                // UsuarioModificador: element.UsuarioModificador,
                // FechaModificacion: element.FechaModificacion,
                PendienteAprobarEntrega: element.PendienteAprobarEntrega,
                PendienteAprobarValidacion: element.PendienteAprobarValidacion,
            }

            this.props.dispatch({
                type: 'ADD_ACCEOBRA',
                dataAccObra
            });
        }
        setTimeout(() => { this.onChangeLoading(); }, 300);
    };

    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataAO = this.props.accobra;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            //btnGroup: this.createCustomDeleteButton
        };
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataAO} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true}>
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObra" dataSort={ true } editable={false}>Obra</TableHeaderColumn>
                        <TableHeaderColumn dataField="PendienteAprobarEntrega" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ?{ type: 'checkbox', defaultValue: 'No', options: { values: 'Si:No' } }:false}>Pendiente por Aprobar Entrega</TableHeaderColumn>
                        <TableHeaderColumn dataField="PendienteAprobarValidacion" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ?{ type: 'checkbox', defaultValue: 'No', options: { values: 'Si:No' } }:false}>Pendiente por Aprobar Validación</TableHeaderColumn>
                    </BootstrapTable>
                }
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        estobra: state.EstadoObra,
        accobra: state.AccObra,
        lg: state.Login
    }
}


export default connect(mapStateToProps)(TablaObraEstado);