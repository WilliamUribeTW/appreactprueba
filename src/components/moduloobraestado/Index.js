import React from 'react';
import TablaObraEstado from './tablaobraestado/Index';

class ModObraEstado extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      swD:true
    }
    this.handleCreateSoli = this.handleCreateSoli.bind(this);
    this.handleCancelSoli = this.handleCancelSoli.bind(this);
  }

  handleCreateSoli(){
    this.setState({swD:false});
  }
  handleCancelSoli(){
    this.setState({swD:true});
  }
  render() {
    return (
      <div className="container">
        {/* {this.state.swD ? <TablaObraEstado onCreate={this.handleCreateSoli}></TablaAccesoObra>:<Formulario onCancel={this.handleCancelSoli}></Formulario>} */}
        <TablaObraEstado></TablaObraEstado>
      </div>
    )
  }
}

export default ModObraEstado;