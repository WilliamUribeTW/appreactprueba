import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import CampoInput from '../campoinput/Index';

const Firmas = React.forwardRef((props, ref) => {
    return <Firma {...props} innerRef={ref}></Firma>
});
class Firma extends React.Component {
    constructor(prop) {
        super(prop);
        this.state = {
            name: "",
            open: true,
            msg: "",
            color: "",
        };
        this.updateData = this.updateData.bind(this);
    }
    updateData() {
        const dt = this.state.name;
        if (dt === "") {
            this.setState({ msg: "Se debe diligenciar el campo de la firma", color: "red" });
        } else {
            let data = { "est": this.props.onEstadoSoli, "idSoli": this.props.onIdSoli, "firma": dt}
            this.props.onUpdateEst(data);
            this.setState({ name: "", msg: "Se guardo la firma de manera correcta", color: "green" });
        }
    }
    close = () => {
        this.setState({ open: false });
        //this.props.onUpdate(this.props.defaultValue);
        this.props.onChange();
    }
    render() {
        return (
            <Modal show={this.state.open} ref={this.props.innerRef} onHide={this.close} size="xl">
                <Modal.Header closeButton>
                    <Modal.Title>Firmar Solicitud</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Row>
                            <CampoInput value={this.props.onNroSolici} onDisabled={true} type="text" placeholder="Solicitud" name="formfirmanro" controlId="controlId"></CampoInput>
                            <CampoInput value={this.props.onEncargado} onDisabled={true} type="text" placeholder="Encargado" name="formfirmaencargado" controlId="controlId"></CampoInput>
                        </Form.Row>

                        <textarea
                            className={(this.props.editorClass || '') + ' form-control editor edit-text'}
                            style={{ display: 'inline', width: '100%' }}
                            value={this.state.name}
                            onChange={e => { this.setState({ name: e.currentTarget.value }); }}
                        >
                        </textarea>
                        <label style={{ display: 'block', width: '100%', color: this.state.color }}>{this.state.msg}</label>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.close}>Cerrar</Button>
                    <Button onClick={this.updateData}>Guardar</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
export default Firmas;