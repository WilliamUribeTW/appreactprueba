import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import './Index.css';
import Moment from 'moment';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import readXlsFile from 'read-excel-file';
import axios from 'axios';
import { RutaApi } from '../../../../const/Index';

class BotonGuardarForm extends React.Component {
    constructor(props) {
        super(props);
        this.arrnoload = [];
        this.arrexportInsumo = [];
        this.arrinfo = [];
        this.state = {
            sw: false,
            btn: false,
            brrData: false,
            title: "",
            msj: "",
        };
        this.handleCreateData = this.handleCreateData.bind(this);
    }

    handleChange = () => {
        this.setState({ sw: false });
    }
    handleCreateData(e) {
        const { NombreInsumo, UnidadMedidaInsumo, Estado, sw, CodInsumo, FkEstado } = this.props.onDataState;
        let result = 0;
        if (sw === true) {
            const input = document.getElementById('dtinsumofile');

            if (input.files.length === 0) {
                this.setState({ sw: true, brrData: false, })
                this.titleMsn = "Advertencia";
                this.textMsn = "Se debe seleccionar el archivo.";
            } else {
                this.setState({ sw: true, btn: true });
                this.titleMsn = "Carga";
                this.textMsn = "Cargando la informacion, por favor espere mientras termina el proceso...";
                let usr = this.props.user.map(dtuser => dtuser.dtuserid), cont = this.props.insumo;
                readXlsFile(input.files[0]).then((rows) => {
                    rows.map(fil => {
                        if (fil[0] !== "Código Insumo") {

                            let contIn = cont.filter(c => c.CodInsumo === fil[0]);
                            
                            if (contIn.length === 0) {
                                if (!isNaN(fil[0])) {
                                    //Crear los datos
                                    const dataInsumo = {
                                        IdInsumo: 0,
                                        CodInsumo: fil[0],
                                        NombreInsumo: fil[1],
                                        UnidadMedidaInsumo: fil[2],
                                        FkEstado: fil[3],
                                        Estado: fil[4],
                                        Cargue: "Masiva",
                                        FkUsuarioCreador: usr[0],//this.props.onDataState.formcreadopor,
                                        FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),//this.props.onDataState.formfechacreacion,
                                    }
                                    /**
                                    * Guardar la informacón en la base de datos
                                    */
                                    axios.post(RutaApi + 'Insumo', JSON.stringify(dataInsumo))
                                        .then(res => {
                                            //console.log(res.data);
                                            result = res.data;
                                        })
                                        .catch(error => {
                                            //console.log(error.response);
                                            this.titleMsn = "Error";
                                            this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                                        });
                                }
                            } else {
                                if (!isNaN(fil[0])) {
                                    //Modificar los datos
                                    const dataInsumo = {
                                        IdInsumo: 1,
                                        CodInsumo: fil[0],
                                        NombreInsumo: fil[1],
                                        UnidadMedidaInsumo: fil[2],
                                        FkEstado: fil[3],
                                        Estado: fil[4],
                                        Cargue: "Masiva",
                                        FkUsuarioModificador: usr[0],
                                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                                    }

                                    /**
                                    * Modificar la informacón en la base de datos
                                    */
                                    axios.post(RutaApi + 'Insumo', JSON.stringify(dataInsumo))
                                        .then(res => {
                                            //console.log(res.data);
                                            result = res.data;
                                        })
                                        .catch(error => {
                                            //console.log(error.response);
                                            this.titleMsn = "Error";
                                            this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                                        });
                                }
                            }
                            return contIn;
                        } else {
                            return null;
                        }
                    })
                })
                setTimeout(() => { this.handleShowMsj(result); }, 700);
            }
        } else {
            if (NombreInsumo === "" || UnidadMedidaInsumo === "" || Estado === "" || CodInsumo === "") {
                this.setState({ sw: true, brrData: false, title: "Advertencia", msj: "Se debe diligenciar el formulario completo." })
            } else {
                //Agregar informacion a store de 
                this.setState({ sw: false, });
                let usr = this.props.user.map(dtuser => dtuser.dtuserid);
                let validcodinsu = this.props.insumo.filter(c => c.CodInsumo === CodInsumo).map(c => c.id);
                if (validcodinsu.length > 0) {
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado.";
                    this.setState({ sw: true, brrData: false, });
                } else {
                    const dataInsumo = {
                        CodInsumo: CodInsumo,
                        NombreInsumo: NombreInsumo,
                        UnidadMedidaInsumo: UnidadMedidaInsumo,
                        FkEstado: FkEstado,
                        Estado: Estado,
                        Cargue: "Manual",
                        FkUsuarioCreador: usr[0],
                        FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),
                    }
                    /**
                     * Guardar la informacón en la base de datos
                     */
                    let result = 0;
                    axios.post(RutaApi + 'Insumo', JSON.stringify(dataInsumo))
                        .then(res => {
                            result = res.data;
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                            // this.titleMsn = "Error";
                            // this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                        });

                    setTimeout(() => { this.handleShowMsj(result); }, 300);

                }
            }
        }
    }
    handleShowMsj = async (result) => {
        // Validamos la variable para poder mostrar el mensaje
        if (parseInt(result) === parseInt(0)) {
            this.titleMsn = "Advertencia";
            this.textMsn = "Se presento un inconveniente al insertar la información.";
        } else {
            this.titleMsn = "Registro exitoso";
            this.textMsn = "Se registro la información";

            /**
             * Se limpia los datos para que no se dupliquen cada ves que se cargue
             */
            this.props.dispatch({
                type: 'CLEAN_CENTROCOSTO'
            });
            this.props.dispatch({
                type: 'CLEAN_LISTACENTROCOSTO'
            });
            /**
             * Cargamos la informacion para actualizar el redux
             */
            let res = await axios.get(RutaApi + 'CentroCosto');
            for (let index = 0; index < res.data.length; index++) {
                const element = res.data[index];
                const dataCentroCosto = {
                    id: element.IdCentroCosto,
                    CodCentroCosto: element.CodCentroCosto,
                    CodCentroCostoOld: element.CodCentroCosto,
                    NombreCentroCosto: element.NombreCentroCosto,
                    FkSubCapitulo: element.FkSubCapitulo,
                    NombreSubCapitulo: element.NombreSubCapitulo,
                    CodSubCapitulo: element.CodSubCapitulo,
                    FkEstado: element.FkEstado,
                    Estado: element.Estado,
                    Cargue: element.Cargue,
                    FkUsuarioCreador: element.FkUsuarioCreador,
                    UsuarioCreador: element.UsuarioCreador,
                    FechaCreacion: element.FechaCreacion,
                    FkUsuarioModificador: element.FkUsuarioModificador,
                    UsuarioModificador: element.UsuarioModificador,
                    FechaModificacion: element.FechaModificacion,
                }

                this.props.dispatch({
                    type: 'ADD_CENTROCOSTO',
                    dataCentroCosto
                });
                /**
                 * Cargar listado desplegable
                 */
                if (parseInt(element.FkEstado) !== parseInt(2)) {
                    const dataListaCentroCosto = {
                        value: element.IdCentroCosto,
                        label: element.NombreCentroCosto,
                        codCC: element.CodCentroCosto,
                        // idsubcapitulo: element.FkContratista,
                    }
                    this.props.dispatch({
                        type: 'ADD_LISTACENTROCOSTO',
                        dataListaCentroCosto
                    });
                }
            }
        }
        this.setState({ sw: true, show: true, brrData: true, }, function () {
            this.props.onCreate(this.state.brrData);
        });
    }

    render() {
        return (
            <div>
                <br></br>
                <div className="divBoton">
                    <Button variant="primary" type="button" onClick={() => this.props.onCancel()} disabled={this.state.sw}>Volver</Button>
                    <Button variant="primary" type="submit" onClick={this.handleCreateData} disabled={this.state.sw}>Guardar</Button>
                </div>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        insumo: state.Insumo,
        user: state.Login
    }
}

export default connect(mapStateToProps)(BotonGuardarForm);