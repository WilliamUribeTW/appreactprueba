import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { DataEstadoUsuario, DataCostos } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';
import CampoInput from '../../campoinput/Index';
import CampoRadio from '../../camporadio/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      NombreInsumo: "",
      // FkCentroCosto: "",
      CodInsumo: "",
      // NombreCentroCosto: "",
      UnidadMedidaInsumo: "",
      Estado: "",
      Cargue: "Manual",
      dtinsumofile: null,
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
      sw: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childestado = React.createRef();
    this.childccosto = React.createRef();
  }

  handleInput(e) {
    const { value, name, id } = e;
    // if (name === "NombreCentroCosto") { this.setState({ FkCentroCosto: id }); }
    if (name === "Estado") { this.setState({ FkEstado: id });}
    if (name === "Cargue") { if (value === "Masiva") { this.setState({ sw: true }); } else { this.setState({ sw: false }); } }
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {

      if (this.state.sw === false) {
        /**
         * Se llama la función del componente 
         */
        this.childestado.current.handleClick();
        this.childccosto.current.handleClick()
      };

      this.setState({
        NombreInsumo: "",
        // FkCentroCosto: "",
        // NombreCentroCosto: "",
        UnidadMedidaInsumo: "",
        CodInsumo:"",
        Estado: "",
        Cargue: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: "",
        sw: false,
      });

    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoRadio onResult={this.handleInput} name="Cargue" onChecked={this.state.sw}></CampoRadio>
          {this.state.sw ?
            <CampoInput onResult={this.handleInput} type="file" placeholder="Archivo" name="dtinsumofile" controlId="dtinsumofile" id="dtinsumofile"></CampoInput>
            :
            <div>
              {/* <DataListInput dataselect={this.props.listccosto} onSelectAuto={this.handleInput} valueText={this.state.NombreCentroCosto} ref={this.childccosto} placeholder="Seleccione el actividad" name="NombreCentroCosto" list="NombreCentroCosto" onRequired={true}>
              </DataListInput> */}
              <CampoInput onResult={this.handleInput} value={this.state.NombreInsumo} onRequired={true} onDisabled={false} type="text" placeholder="Insumo" name="NombreInsumo" controlId="NombreInsumo"></CampoInput>
              <CampoInput onResult={this.handleInput} value={this.state.CodInsumo} onRequired={true} onDisabled={false} type="number" placeholder="Código Insumo" name="CodInsumo" controlId="CodInsumo"></CampoInput>
              <CampoInput onResult={this.handleInput} value={this.state.UnidadMedidaInsumo} onRequired={true} onDisabled={false} type="text" placeholder="Unidad de medida" name="UnidadMedidaInsumo" controlId="UnidadMedidaInsumo"></CampoInput>
              <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.dtestadocentrocosto} ref={this.childestado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
              </DataListInput>
            </div>
          }

          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
    return {
        lg: state.Login,
        estado: state.Estado,
        // listccosto: state.ListaCentroCostos,
    }
}
export default connect(mapStateToProps)(FormularioGeneral);