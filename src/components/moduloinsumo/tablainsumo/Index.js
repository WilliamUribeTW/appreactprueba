import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { RutaApi } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import Moment from 'moment';
import ExportarInfo from '../../botonexportar/Index';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaInsumo extends React.Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            sw: false,
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        /**
         * Se crea una lista, para poder agregar la información del maestro
         */
        let dt = [];
        this.props.insumo.forEach(element => {
            const inf = [element.CodInsumo,element.NombreInsumo, element.UnidadMedidaInsumo, element.FkEstado, element.Estado];
            dt.push(inf);
        });
        /**
         * Si la lista no tiene información, se le agrega los campos vacios, 
         * para que se visualice de manera correcta el formato
         */
        if(dt.length <= 0){
            dt.push(["","", "", "", ""]);
        }
        const arrexportInsumo = [
            { 
                columns: ["Código Insumo","Nombre Insumo","Unidad Medida", "Código Estado", "Estado"],
                data: dt
            },
        ];
        /**
         * Array para dar las instrucciones de como actualizar el excel
         */
        const arrinfo = [
            {
                columns: ["Instrucciones para subir el archivo por Carga Masiva"],
                data: [
                    ["- Ir a la Hoja FormatoInsumo."],
                    ["- Diligenciar las columnas Código Insumo, Código Estado, Nombre Insumo y Unidad Medida, Estado"],
                    ["- La columna Estado es informativa."],
                    ["- Tener en cuenta que el código del Estado es: 1 = Activo y 2 = Inactivo."],
                    ["- Eliminar esta hoja (Instrucciones),en el archivo de excel solo debe existir la hoja (FormatoInsumo)."],
                ]
            }
        ]; 
        return (
            <div>
                {parseInt(crud[0].dtc) !== parseInt(0) && <div>
                    <ExportarInfo onData={this.props.insumo} onName="FormatoInsumo" onColumn={arrexportInsumo} onInfo={arrinfo}></ExportarInfo>
                    <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear Insumo</button>
                </div>}
            </div>
        );
    }
    /**
     * Ir a crear
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_INSUMO',
                id: items[i]
            })
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), valcc = 0, valest = 0, valcargue = "";
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'Insumo');
        let dt = res.data.filter(f => f.IdInsumo === row.id);
        dt = dt[0];

        // if (cellName === "NombreCentroCosto") { valcc = this.props.centrocostos.filter(c => c.label === row.NombreCentroCosto).map(c => c.value); valcc = valcc[0]; } else { valcc = row.FkCentroCosto }
        if (cellName === "Estado") { valest = this.props.estado.filter(c => c.label === cellValue).map(c => c.value); valest = valest[0]; } else { valest = row.FkEstado }
        if (row.Cargue === "") { valcargue = "Manual"; } else { valcargue = row.Cargue; }

        if (valest === row.FkEstado && dt.CodInsumo === row.CodInsumo && dt.NombreInsumo === row.NombreInsumo && dt.UnidadMedidaInsumo === row.UnidadMedidaInsumo) {
            // console.log("no actualizar");
        } else {
            if (cellName === "CodInsumo") {
                /**
                 * Validar que el codigo no este repetido
                 */
                let codins = res.data.filter(f => f.CodInsumo === row.CodInsumo);
                if (codins.length > 0) {
                    row.CodInsumo = row.CodInsumoOld;
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado";
                    this.setState({ sw: true, });

                } else {
                    const dataInsumo = {
                        IdInsumo: row.id,
                        CodInsumo: row.CodInsumo,
                        NombreInsumo: row.NombreInsumo,
                        UnidadMedidaInsumo: row.UnidadMedidaInsumo,
                        FkEstado: valest,
                        Cargue: valcargue,
                        FkCentroCosto: valcc,
                        FkUsuarioModificador: usr[0],
                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                    }
                    /**
                     * Modificar la informacón en la base de datos
                     */
                    axios.post(RutaApi + 'Insumo', JSON.stringify(dataInsumo))
                        .then(res => {
                            // console.log(res.data);
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                        });
                    // this.onLoadData();
                    this.UNSAFE_componentWillMount();
                }
            } else {
                const dataInsumo = {
                    IdInsumo: row.id,
                    CodInsumo: row.CodInsumo,
                    NombreInsumo: row.NombreInsumo,
                    UnidadMedidaInsumo: row.UnidadMedidaInsumo,
                    FkEstado: valest,
                    Cargue: valcargue,
                    FkCentroCosto: valcc,
                    FkUsuarioModificador: usr[0],
                    FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                }
                /**
                 * Modificar la informacón en la base de datos
                 */
                axios.post(RutaApi + 'Insumo', JSON.stringify(dataInsumo))
                    .then(res => {
                        // console.log(res.data);
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                    });
                // this.onLoadData();
                this.UNSAFE_componentWillMount();
            }
        }
    }
    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_INSUMO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTAINSUMO'
        });
        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'Insumo');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataInsumo = {
                id: element.IdInsumo,
                CodInsumo: element.CodInsumo,
                CodInsumoOld: element.CodInsumo,
                NombreInsumo: element.NombreInsumo,
                UnidadMedidaInsumo: element.UnidadMedidaInsumo,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                Cargue: element.Cargue,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
                // FkCentroCosto: element.FkCentroCosto,
                // NombreCentroCosto: element.NombreCentroCosto,
            }
            this.props.dispatch({
                type: 'ADD_INSUMO',
                dataInsumo
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaInsumo = {
                    value: element.IdInsumo,
                    label: element.NombreInsumo,
                    codIN: element.CodInsumo,
                    und: element.UnidadMedidaInsumo,
                    // idCosto: element.FkCentroCosto,
                }
                this.props.dispatch({
                    type: 'ADD_LISTAINSUMO',
                    dataListaInsumo
                });
            }
        }

        setTimeout(() => { this.onChangeLoading(); }, 300);
    };


    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataI = this.props.insumo;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataI} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        {/* <TableHeaderColumn dataField="FkCentroCosto" hidden={true}># CodCentroCosto</TableHeaderColumn> */}
                        <TableHeaderColumn dataField="FkEstado" hidden={true}># CodEstado</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodInsumoOld" hidden={true}># CodInsumoOld</TableHeaderColumn>
                        {/* <TableHeaderColumn dataField="NombreCentroCosto" editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.centrocostos.map(dt => { return dt.label }) } }:false} tdStyle={{ whiteSpace: "normal", width: this.state.wdthCCIN }} thStyle={{ whiteSpace: "normal", width: this.state.wdthCCIN }} >Actividad</TableHeaderColumn> */}
                        <TableHeaderColumn dataField="NombreInsumo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal", width: this.state.wdthCCIN }} thStyle={{ whiteSpace: "normal", width: this.state.wdthCCIN }}>Insumo</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodInsumo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal", width: "9%" }} thStyle={{ whiteSpace: "normal", width: "9%" }} headerText="Código Insumo">Cod.Insumo</TableHeaderColumn>
                        <TableHeaderColumn dataField="UnidadMedidaInsumo" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal", width: "5%" }} thStyle={{ whiteSpace: "normal", width: "5%" }} headerText="Unidad de Medida">Und</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }} thStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }}>Estado Insumo</TableHeaderColumn>
                        <TableHeaderColumn dataField="Cargue" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }} thStyle={{ whiteSpace: "normal", width: this.state.wdthEICA }}>Cargue</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true }  editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true }  editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true }  editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={ true }  editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        insumo: state.Insumo,
        lg: state.Login,
        estado: state.Estado,
        // centrocostos: state.CentroCostos,
    }
}


export default connect(mapStateToProps)(TablaInsumo);