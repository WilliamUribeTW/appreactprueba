import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Login from '../login/Login';
import Menu from './components/menu/Menu';
import decode from 'jwt-decode';

const checkAuth = () => {
    const token = localStorage.getItem('token');
    const refreshToken = localStorage.getItem('refreshToken');

    if(!token || !refreshToken){
        return false;
    }

    try{
        const { exp } = decode(refreshToken);
        console.log(exp);
        console.log(new Date().getTime() / 1000);
        if(exp < new Date().getTime()){
            return false;
        }
    }catch(e){

    }

    return true;
}

const AuthRoute = ({ component: Component, ...rest }) => {
    <Route {...rest} render={props => (
        checkAuth() ? (<Component {...props}></Component>) : (<Redirect to={{ pathname: '/' }}></Redirect>)
    )}></Route>
}

export default () => {
    <BrowserRouter>
        <Switch>AuthRoute
            <Route exact path="/login/" render={props => <Login {...props}></Login>}></Route>
            <AuthRoute exact path="/menu/" component={Menu}></AuthRoute>
        </Switch>
    </BrowserRouter>
}
