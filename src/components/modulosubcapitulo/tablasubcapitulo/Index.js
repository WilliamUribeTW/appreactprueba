import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { RutaApi } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaSubCapitulo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sw: false,
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        return (
            <div>
                {/* <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar item</button> */}
                {parseInt(crud[0].dtc) !== parseInt(0) && <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear SubCapitulo</button>}
            </div>
        );
    }
    /**
     * Ir a crear
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_SUBCAPITULO',
                id: items[i]
            })
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), valcap = 0, idest = 0;
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'SubCapitulo');
        let dt = res.data.filter(f => f.IdSubCapitulo === row.id);
        dt = dt[0];

        if (cellName === "NombreCapitulo") { valcap = this.props.listcapitulo.filter(c => c.label === row.NombreCapitulo).map(c => c.value); valcap = valcap = valcap[0] } else { valcap = row.FkCapitulo }
        if (cellName === "Estado") { idest = this.props.estado.filter(e => e.label === cellValue).map(e => e.value); idest = idest[0]; } else { idest = row.FkEstado }

        if (valcap === row.FkCapitulo && idest === row.FkEstado && dt.CodSubCapitulo === row.CodSubCapitulo && dt.NombreSubCapitulo === row.NombreSubCapitulo) {
            // console.log("no actualizar");
        } else {

            if (cellName === "CodSubCapitulo") {
                /**
                 * Validar que el codigo no este repetido
                 */
                let codobr = res.data.filter(f => f.CodSubCapitulo === row.CodSubCapitulo);
                if (codobr.length > 0) {
                    row.CodSubCapitulo = row.CodSubCapituloOld;
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado";
                    this.setState({ sw: true, });

                } else {
                    const dataSubCapitulo = {
                        IdSubCapitulo: row.id,
                        CodSubCapitulo: row.CodSubCapitulo,
                        NombreSubCapitulo: row.NombreSubCapitulo,
                        FkCapitulo: valcap,
                        FkEstado: idest,
                        FkUsuarioModificador: usr[0],
                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                    }
                    /**
                     * Modificar la informacón en la base de datos
                     */
                    axios.post(RutaApi + 'SubCapitulo', JSON.stringify(dataSubCapitulo))
                        .then(res => {
                            // console.log(res.data);
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                        });
                    // this.onLoadData();
                    this.UNSAFE_componentWillMount();
                }
            } else {
                const dataSubCapitulo = {
                    IdSubCapitulo: row.id,
                    CodSubCapitulo: row.CodSubCapitulo,
                    NombreSubCapitulo: row.NombreSubCapitulo,
                    FkCapitulo: valcap,
                    FkEstado: idest,
                    FkUsuarioModificador: usr[0],
                    FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                }
                /**
                 * Modificar la informacón en la base de datos
                 */
                axios.post(RutaApi + 'SubCapitulo', JSON.stringify(dataSubCapitulo))
                    .then(res => {
                        // console.log(res.data);
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                    });
                // this.onLoadData();
                this.UNSAFE_componentWillMount();
            }
        }
    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }

    /**
     * Carga de datos 
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_SUBCAPITULO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTASUBCAPITULOS'
        });
        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'SubCapitulo');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataSubCapitulo = {
                id: element.IdSubCapitulo,
                CodSubCapitulo: element.CodSubCapitulo,
                CodSubCapituloOld: element.CodSubCapitulo,
                NombreSubCapitulo: element.NombreSubCapitulo,
                FkCapitulo: element.FkCapitulo,
                NombreCapitulo: element.NombreCapitulo,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_SUBCAPITULO',
                dataSubCapitulo
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaSubcapitulo = {
                    value: element.IdSubCapitulo,
                    label: element.NombreSubCapitulo,
                    idCapitulo: element.FkCapitulo,
                }
                this.props.dispatch({
                    type: 'ADD_LISTASUBCAPITULOS',
                    dataListaSubcapitulo
                });
            }
        }

        setTimeout(() => { this.onChangeLoading(); }, 300);
    };

    onChangeLoading = () => {
        this.setState({ loading: false });
    }


    render() {
        const dataSbCp = this.props.subcapitulo;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        // const selectRow = {
        //     mode: 'checkbox',
        //     onSelect: this.onSelectDelete

        // };//deleteRow selectRow={selectRow}
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataSbCp} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkCapitulo" hidden={true}># CodCapitulo</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkEstado" hidden={true}># CodEstado</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodSubCapituloOld" hidden={true}># CodSubCapituloOld</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreSubCapitulo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Subcapitulo</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodSubCapitulo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Codigo Subcapitulo</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreCapitulo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.listcapitulo.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Capítulo</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Estado Subcapitulo</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        subcapitulo: state.SubCapitulos,
        lg: state.Login,
        estado: state.Estado,
        listcapitulo: state.ListaCapitulo,
    }
}


export default connect(mapStateToProps)(TablaSubCapitulo);