import React from 'react';
import Formulario from './formulario/Index';
import TablaSubCapitulo from './tablasubcapitulo/Index';

class ModSubCapitulo extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      swD:true
    }
    this.handleCreateSoli = this.handleCreateSoli.bind(this);
    this.handleCancelSoli = this.handleCancelSoli.bind(this);
  }

  handleCreateSoli(){
    this.setState({swD:false});
  }
  handleCancelSoli(){
    this.setState({swD:true});
  }
  render() {
    return (
      <div className="container">
        {this.state.swD ? <TablaSubCapitulo onCreate={this.handleCreateSoli}></TablaSubCapitulo>:<Formulario onCancel={this.handleCancelSoli}></Formulario>}
      </div>
    )
  }
}

export default ModSubCapitulo;