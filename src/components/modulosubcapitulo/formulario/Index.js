import React from 'react';
import { Form } from 'react-bootstrap';
//import axios from "axios";
import { connect } from 'react-redux';
// import { DataEstadoUsuario, DataCapitulo } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';
import CampoInput from '../../campoinput/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      CodSubCapitulo: "",
      NombreSubCapitulo: "",
      FkCapitulo: "",
      NombreCapitulo: "",
      FkEstado: "",
      Estado: "",
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
      datacapitulo: []
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childestado = React.createRef();
    this.childcapitulo = React.createRef();
  }

  handleInput(e) {
    const { value, name, id } = e;
    if (name === "NombreCapitulo") { this.setState({ FkCapitulo: id }); }
    if(name === "Estado"){ this.setState({FkEstado: id});}
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {
      this.setState({
        NombreSubCapitulo: "",
        CodSubCapitulo: "",
        FkCapitulo: "",
        NombreCapitulo: "",
        Estado: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: "",
      });

      /**
       * Se llama la función del componente 
       */
      this.childestado.current.handleClick();
      this.childcapitulo.current.handleClick();
    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }
  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoInput onResult={this.handleInput} value={this.state.NombreSubCapitulo} onRequired={true} onDisabled={false} type="text" placeholder="SubCapitulo" name="NombreSubCapitulo" controlId="NombreSubCapitulo"></CampoInput>
          <CampoInput onResult={this.handleInput} value={this.state.CodSubCapitulo} onRequired={true} onDisabled={false} type="text" placeholder="Codigo SubCapitulo" name="CodSubCapitulo" controlId="CodSubCapitulo"></CampoInput>
          <DataListInput dataselect={this.props.listcapitulo} onSelectAuto={this.handleInput} valueText={this.state.NombreCapitulo} ref={this.childcapitulo} placeholder="Seleccione el capitulo" name="NombreCapitulo" list="NombreCapitulo" onRequired={true}>
          </DataListInput>
          <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.Estado} ref={this.childestado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
          </DataListInput>
          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
      listcapitulo: state.ListaCapitulo,
      lg: state.Login,
      estado: state.Estado,
      listclasificaciones: state.ListaClasificaciones,
  }
}
export default connect(mapStateToProps)(FormularioGeneral);