import React from 'react';

export const Contact = (props) => {
    return (
        <div className="col-lg-auto col-md-6 col-sm-5 col-xs-6 contenidoInternaContact" >
            <label>Pagina inicial Contacto</label>
        </div>
    )
}
export const Home = () => (
    < div className="col-lg-auto col-md-6 col-sm-5 col-xs-6 contenidoInternaHome" >
        <label>Bienvenido al sistema de administración de solicitudes</label>
            {this.onLoadData()}
    </div >
)

export const Mision = () => (
    <div className="col-lg-auto col-md-6 col-sm-5 col-xs-6 contenidoInternaMision">
        <label>Pagina inicial Mision</label>
    </div>
)

export const Quienessomos = () => (
    <div className="col-lg-auto col-md-6 col-sm-5 col-xs-6 contenidoInternaQuienessomos">
        <label>Pagina inicial Quienes Somos</label>
    </div>
)

export const Vision = () => (
    <div className="col-lg-auto col-md-6 col-sm-5 col-xs-6 contenidoInternaVision">
        <label>Pagina inicial Vision</label>
    </div>
)