import React from 'react';
import axios from "axios";
import { connect } from 'react-redux';
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';
import { RutaApi } from '../../const/Index';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.arrList = []
        this.state = {
            loading: true,
        }
    }
    /**
     * Carga la informacion
     */
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    UNSAFE_componentDidMount() {
        this.onLoadData();
    }
    onLoadData = async () => {
        /**
         * Limpiar los datos del redux
         */

        this.props.dispatch({
            type: 'CLEAN_LISTACAPITULOS'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTASUBCAPITULOS'
        });
        this.props.dispatch({
            type: 'CLEAN_ESTADO'
        });
        this.props.dispatch({
            type: 'CLEAN_ESTADOSOLICITUD'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTAOBRA'
        });
        this.props.dispatch({
            type: 'CLEAN_OBRA'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTACONTRATISTA'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTACLASIFICACION'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTAINSUMO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTACENTROCOSTO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTAUSUARIO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTARELACIONCIO'
        });


        // Cargar el listado de Capitulo
        let resCap = await axios.get(RutaApi + 'Capitulo');
        for (let index = 0; index < resCap.data.length; index++) {
            const element = resCap.data[index];
            const dataCapitulo = {
                id: element.IdCapitulo,
                CodCapitulo: element.CodCapitulo,
                CodCapituloOld: element.CodCapitulo,
                NombreCapitulo: element.NombreCapitulo,
                FkClasificacion: element.FkClasificacion,
                DescripcionClasificacion: element.DescripcionClasificacion,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_CAPITULO',
                dataCapitulo
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaCapitulo = {
                    value: element.IdCapitulo,
                    label: element.NombreCapitulo,
                    idClasiSoli: element.FkClasificacion,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACAPITULOS',
                    dataListaCapitulo
                });
            }
        }
        // Cargar el listado de Subcapitulo
        let resSubcap = await axios.get(RutaApi + 'SubCapitulo');
        for (let index = 0; index < resSubcap.data.length; index++) {
            const element = resSubcap.data[index];
            const dataSubCapitulo = {
                id: element.IdSubCapitulo,
                CodSubCapitulo: element.CodSubCapitulo,
                CodSubCapituloOld: element.CodSubCapitulo,
                NombreSubCapitulo: element.NombreSubCapitulo,
                FkCapitulo: element.FkCapitulo,
                NombreCapitulo: element.NombreCapitulo,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_SUBCAPITULO',
                dataSubCapitulo
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaSubcapitulo = {
                    value: element.IdSubCapitulo,
                    label: element.NombreSubCapitulo,
                    idCapitulo: element.FkCapitulo,
                }
                this.props.dispatch({
                    type: 'ADD_LISTASUBCAPITULOS',
                    dataListaSubcapitulo
                });
            }
        }

        // Cargar el listado de Estado
        axios.get(RutaApi + 'Estado')
            .then(res => {
                for (let index = 0; index < res.data.length; index++) {
                    const element = res.data[index];
                    const dataEstado = {
                        value: element.IdEstado,
                        label: element.NombreEstado,
                    }
                    this.props.dispatch({
                        type: 'ADD_ESTADO',
                        dataEstado
                    });
                }
            });
        // Cargar el listado de Estados Solicitud
        axios.get(RutaApi + 'EstadoSolicitud')
            .then(res => {
                for (let index = 0; index < res.data.length; index++) {
                    const element = res.data[index];
                    const dataListSoliEstado = {
                        id: element.IdEstadoSolicitud,
                        value: element.NombreEstadoSolicitud,
                        icon: element.icon,
                        style: element.style,
                    }
                    this.props.dispatch({
                        type: 'ADD_ESTADOSOLICITUD',
                        dataListSoliEstado
                    });
                }
            });
        // Cargar el listado de Obras
        let res = await axios.get(RutaApi + 'Obra');
        //let { data } = res.data;
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataObra = {
                id: element.IdObra,
                CodObra: element.CodObra,
                CodObraOld: element.CodObra,
                NombreObra: element.NombreObra,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_OBRA',
                dataObra
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaObra = {
                    value: element.IdObra,
                    label: element.NombreObra,
                    dtestppae: element.PendienteAprobarEntrega,
                    dtestppav: element.PendienteAprobarValidacion,
                }
                this.props.dispatch({
                    type: 'ADD_LISTAOBRA',
                    dataListaObra
                });
            }

        }
        // Cargar el listado de Contratistas
        let resContra = await axios.get(RutaApi + 'Contratista');
        for (let index = 0; index < resContra.data.length; index++) {
            const element = resContra.data[index];
            const dataContratista = {
                id: element.IdContratista,
                NombreContratista: element.NombreContratista,
                Nit: element.Nit,
                NitOld: element.Nit,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_CONTRATISTA',
                dataContratista
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaContratista = {
                    value: element.IdContratista,
                    label: element.NombreContratista,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACONTRATISTA',
                    dataListaContratista
                });
            }
        }
        // Cargar el listado de Clasificacion
        let resCla = await axios.get(RutaApi + 'Clasificacion');
        for (let index = 0; index < resCla.data.length; index++) {
            const element = resCla.data[index];
            const dataClasificacion = {
                id: element.IdClasificacion,
                CodClasificacion: element.CodClasificacion,
                CodClasificacionOld: element.CodClasificacion,
                DescripcionClasificacion: element.DescripcionClasificacion,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_CLASIFICACION',
                dataClasificacion
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaClasificacion = {
                    value: element.IdClasificacion,
                    label: element.DescripcionClasificacion,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACLASIFICACION',
                    dataListaClasificacion
                });
            }
        }
        // Cargar el listado de Insumo
        let resIns = await axios.get(RutaApi + 'Insumo');
        for (let index = 0; index < resIns.data.length; index++) {
            const element = resIns.data[index];
            const dataInsumo = {
                id: element.IdInsumo,
                CodInsumo: element.CodInsumo,
                CodInsumoOld: element.CodInsumo,
                NombreInsumo: element.NombreInsumo,
                UnidadMedidaInsumo: element.UnidadMedidaInsumo,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                Cargue: element.Cargue,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
                // FkCentroCosto: element.FkCentroCosto,
                // NombreCentroCosto: element.NombreCentroCosto,
            }
            this.props.dispatch({
                type: 'ADD_INSUMO',
                dataInsumo
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaInsumo = {
                    value: element.IdInsumo,
                    label: element.NombreInsumo,
                    codIN: element.CodInsumo,
                    und: element.UnidadMedidaInsumo,
                    // idCosto: element.FkCentroCosto,
                }
                this.props.dispatch({
                    type: 'ADD_LISTAINSUMO',
                    dataListaInsumo
                });
            }
        }
        // Cargar el listado de Centro de Costos(Actividad)
        let resCC = await axios.get(RutaApi + 'CentroCosto');
        for (let index = 0; index < resCC.data.length; index++) {
            const element = resCC.data[index];
            const dataCentroCosto = {
                id: element.IdCentroCosto,
                CodCentroCosto: element.CodCentroCosto,
                CodCentroCostoOld: element.CodCentroCosto,
                NombreCentroCosto: element.NombreCentroCosto,
                FkSubCapitulo: element.FkSubCapitulo,
                NombreSubCapitulo: element.NombreSubCapitulo,
                CodSubCapitulo: element.CodSubCapitulo,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                Cargue: element.Cargue,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_CENTROCOSTO',
                dataCentroCosto
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaCentroCosto = {
                    value: element.IdCentroCosto,
                    label: element.NombreCentroCosto,
                    codCC: element.CodCentroCosto,
                    // idsubcapitulo: element.FkContratista,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACENTROCOSTO',
                    dataListaCentroCosto
                });
            }
        }
        // Cargar el listado de Usuarios
        let resUsr = await axios.get(RutaApi + 'Usuarios');
        for (let index = 0; index < resUsr.data.length; index++) {
            const element = resUsr.data[index];
            const dataUsuario = {
                id: element.IdUsuario,
                Nombre: element.Nombre,
                Usuario: element.Usuario,
                Email: element.Email,
                Contrasenia: element.Contrasenia,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_USUARIO',
                dataUsuario
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaUsuario = {
                    value: element.IdUsuario,
                    label: element.Nombre,
                }
                this.props.dispatch({
                    type: 'ADD_LISTAUSUARIO',
                    dataListaUsuario
                });
            }
        }
        //cargar el listado de Relacion Costo-Insumo-Obra(Insumos asociados)
        axios.get(RutaApi + 'RelacionCostoInsumoObra')
            .then(res => {
                for (let index = 0; index < res.data.length; index++) {
                    const element = res.data[index];
                    const dataListaRCIO = {
                        IdRelacionCIO: element.IdRelacionCIO,
                        FkCentroCosto: element.FkCentroCosto,
                        NombreCentroCosto: element.NombreCentroCosto,
                        FkInsumo: element.FkInsumo,
                        NombreInsumo: element.NombreInsumo,
                        FkObra: element.FkObra,
                        NombreObra: element.NombreObra,
                        FkEstado: element.FkEstado,
                        Estado: element.Estado,
                        Cargue: element.Cargue,
                        FkUsuarioCreador: element.FkUsuarioCreador,
                        UsuarioCreador: element.UsuarioCreador,
                        FechaCreacion: element.FechaCreacion,
                        FkUsuarioModificador: element.FkUsuarioModificador,
                        UsuarioModificador: element.UsuarioModificador,
                        FechaModificacion: element.FechaModificacion,
                    }
                    this.props.dispatch({
                        type: 'ADD_LISTARELACIONCIO',
                        dataListaRCIO
                    });
                }
            });


        setTimeout(() => { this.onChangeLoading(); }, 400);
    }

    onChangeLoading = () => {
        this.setState({ loading: false });
    }
    render() {
        const override = css`
            display: block;
            margin: 0 auto;
            border-color: red;
        `;
        return (

            <div className="col-lg-auto col-md-6 col-sm-5 col-xs-6 contenidoInternaHome">
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    // <label>Bienvenido al sistema de administración de solicitudes</label>
                    <label></label>
                }
            </div >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.List,
        lg: state.Login,
        observ: state.Observacion,
    }
}


export default connect(mapStateToProps)(Home);