import React from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class Herramientas extends React.Component {
    constructor(props) {
        super(props);
        this.onSignInMenu = this.onSignInMenu.bind(this);
    }
    onSignInMenu() {
        //console.log("Aqui");
        this.props.onSignInMenu();
        //e.preventDefault();//Esto es para no recargarla pagina
    }

    render() {
        const settings = {
            dots: true,
            variableWidth: true,//Se coloca en true para que no sea muy amplio el campo entre las imagenes
            // centerPadding: '0.9%',
            centerMode: true,
            infinite: false,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: false,
                        dots: true,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };

        return (
            <div className="herramientas">
                {/* <label>Listado de herramientas</label> */}
                <br></br>
                <Slider {...settings}>
                    <div><img src={process.env.PUBLIC_URL + '/image/solicitudmateriales.png'} className="" alt="" onClick={this.onSignInMenu} /></div>
                    <div><img src={process.env.PUBLIC_URL + '/image/bitacoraobra.png'} className="" alt="" /></div>
                    <div><img src={process.env.PUBLIC_URL + '/image/leancontruction.png'} className="" alt="" /></div>
                </Slider>
            </div>
        )
    }

}

export default Herramientas;