import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
// import Firma from '../../../formfirma/Index';
// import Moment from 'moment';

class TablaDatosItemsID extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            estado: "",
            dataUpd: {
                DescripcionClasificacion: "",
                NombreCentroCosto: "",
                NombreInsumo: "",
                Unidad: "",
                Cantidad: "",
            },
            dataItems: this.props.onDataItems
        };
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => { }
    render() {
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
        };
        return (
            <div>
                <BootstrapTable data={this.state.dataItems} options={options} pagination={true} cellEdit={cellEditProp}
                    search={true} >
                    <TableHeaderColumn dataField="id" isKey={true} hidden={true}>Id</TableHeaderColumn>
                    <TableHeaderColumn dataField="fkidlist" hidden={true}>FkId</TableHeaderColumn>
                    <TableHeaderColumn dataField="DescripcionClasificacion" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Clasificación</TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="NombreCentroCosto" editable={false}>Centro de Costos</TableHeaderColumn> */}
                    <TableHeaderColumn dataField="NombreCentroCosto" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Actividad</TableHeaderColumn>
                    <TableHeaderColumn dataField="CodCentroCosto" editable={false} headerText="Código Actividad">Cod. Actividad</TableHeaderColumn>
                    <TableHeaderColumn dataField="NombreInsumo" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="CodInsumo" editable={false} headerText="Código Insumo">Cod. Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="Unidad" editable={false}>Unidad</TableHeaderColumn>
                    <TableHeaderColumn dataField="Cantidad" editable={false} >Cantidad solicitada</TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="CantidadEntregada" editable={false} >Cantidad entregada</TableHeaderColumn> */}
                </BootstrapTable>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        lg: state.Login,
        list: state.List,
        observ: state.Observacion,
    }
}

export default connect(mapStateToProps)(TablaDatosItemsID);