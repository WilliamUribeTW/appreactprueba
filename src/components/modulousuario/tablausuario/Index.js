import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { /*secret,*/ RutaApi } from '../../../const/Index';
// import { AES } from 'crypto-js';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaUsuario extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        return (
            <div>
                {parseInt(crud[0].dtc) !== parseInt(0) && <button className="botonCrear" onClick={this.handleGoCreate}>Crear usuario</button>}
            </div>
        );
    }
    handleGoCreate() {
        this.props.onCreate();
    }

    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        //console.log(row);
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), idest = 0;
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'Usuarios');
        let dt = res.data.filter(f => f.IdUsuario === row.id);
        dt = dt[0];

        if (cellName === "Contrasenia") { var md5 = require('md5'); let pass = md5(cellValue); row.Contrasenia = pass; }
        if (cellName === "Estado") { idest = this.props.estado.filter(e => e.label === cellValue).map(e => e.value); idest = idest[0]; } else { idest = row.FkEstado }

        if (idest === row.FkEstado && dt.Contrasenia === row.Contrasenia && dt.Email === row.Email) {
            // console.log("no actualizar");
        } else {
            const dataUsuario = {
                IdUsuario: row.id,
                Nombre: row.Nombre,
                Usuario: row.Usuario,
                Email: row.Email,
                Contrasenia: row.Contrasenia,
                FkEstado: idest,
                FkUsuarioModificador: usr[0],
                FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
            }
            /**
             * Modificar la informacón en la base de datos
             */
            axios.post(RutaApi + 'Usuarios', JSON.stringify(dataUsuario))
                .then(res => {
                    // console.log(res.data);
                })
                .catch(error => {
                    console.log("Se presento un error; se detalla a continuación " + error.response);
                });
            // this.onLoadData();
            this.UNSAFE_componentWillMount();
        }
    }

    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_USUARIO'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTAUSUARIO'
        });

        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'Usuarios');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataUsuario = {
                id: element.IdUsuario,
                Nombre: element.Nombre,
                Usuario: element.Usuario,
                Email: element.Email,
                Contrasenia: element.Contrasenia,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_USUARIO',
                dataUsuario
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaUsuario = {
                    value: element.IdUsuario,
                    label: element.Nombre,
                }
                this.props.dispatch({
                    type: 'ADD_LISTAUSUARIO',
                    dataListaUsuario
                });
            }
        }

        setTimeout(() => { this.onChangeLoading(); }, 300);
    };


    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataUser = this.props.user;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };

        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        //DataListPrueba = datos de pruebas
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataUser} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} deleteRow>
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkEstado" dataSort={ true } hidden={true}># CodEstado</TableHeaderColumn>
                        <TableHeaderColumn dataField="Nombre" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Nombre</TableHeaderColumn>
                        <TableHeaderColumn dataField="Usuario" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Usuario</TableHeaderColumn>
                        <TableHeaderColumn dataField="Contrasenia" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'password' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Contraseña</TableHeaderColumn>
                        <TableHeaderColumn dataField="Email" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'email' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Email</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Estado</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
            </div>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.User,
        lg: state.Login,
        estado: state.Estado,
    }
}


export default connect(mapStateToProps)(TablaUsuario);