import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import './Index.css';
import Moment from 'moment';
// import { AES } from 'crypto-js';
// import md5 from 'md5';
import { /*secret,*/ RutaApi } from '../../../../const/Index';
import axios from 'axios';

class BotonGuardarForm extends React.Component {
    constructor(props) {
        super(props);
        this.titleMsn = "";
        this.textMsn = "";
        this.state = {
            sw: false,
            show: false,
            textMsn: "",
            typeMsn: "",
            brrData: false,
        };
        this.handleCreateData = this.handleCreateData.bind(this);
    }

    handleChange = () => {
        this.setState({ sw: false });
    }
    handleCreateData(e) {
        const { Nombre, Usuario, Email, Contrasenia, Estado, FkEstado } = this.props.onDataState;

        if (Nombre === "" || Usuario === "" || Email === "" || Contrasenia === "" || Estado === "") {
            this.setState({ sw: true, show: true, brrData: false, })
            this.titleMsn = "Advertencia";
            this.textMsn = "Se debe diligenciar el formulario completo.";
        } else {
            //Agregar informacion a store de 
            var md5 = require('md5');
            let pass = md5(Contrasenia);
            this.setState({ sw: false, });
            let usr = this.props.userLog.map(dtuser => dtuser.dtuserid);
            const dataUsuario = {
                Usuario: Usuario,
                Nombre: Nombre,
                Email: Email,
                // Contrasenia: AES.encrypt(Contrasenia, secret).toString(),
                Contrasenia: pass,
                FkEstado: FkEstado,
                FkUsuarioCreador: usr[0],
                FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),
            }
            /**
             * Guardar la informacón en la base de datos
             */
            let result = 0;
            axios.post(RutaApi + 'Usuarios', JSON.stringify(dataUsuario))
                .then(res => {
                    result = res.data;
                    // console.log(res.data);
                })
                .catch(error => {
                    console.log("Se presento un error; se detalla a continuación " + error.response);
                });

            setTimeout(() => { this.handleShowMsj(result); }, 300);
        }
    }

    handleShowMsj = async (result) => {
        // Validamos la variable para poder mostrar el mensaje
        if (parseInt(result) === parseInt(0)) {
            this.titleMsn = "Advertencia";
            this.textMsn = "Se presento un inconveniente al insertar la información.";
        } else {
            this.titleMsn = "Registro exitoso";
            this.textMsn = "Se registro la información";
            /**
             * Se limpia los datos para que no se dupliquen cada ves que se cargue
             */
            this.props.dispatch({
                type: 'CLEAN_USUARIO'
            });
            this.props.dispatch({
                type: 'CLEAN_LISTAUSUARIO'
            });
            let res = await axios.get(RutaApi + 'Usuarios');
            for (let index = 0; index < res.data.length; index++) {
                const element = res.data[index];
                const dataUsuario = {
                    id: element.IdUsuario,
                    Nombre: element.Nombre,
                    Usuario: element.Usuario,
                    Email: element.Email,
                    Contrasenia: element.Contrasenia,
                    FkEstado: element.FkEstado,
                    Estado: element.Estado,
                    FkUsuarioCreador: element.FkUsuarioCreador,
                    UsuarioCreador: element.UsuarioCreador,
                    FechaCreacion: element.FechaCreacion,
                    FkUsuarioModificador: element.FkUsuarioModificador,
                    UsuarioModificador: element.UsuarioModificador,
                    FechaModificacion: element.FechaModificacion,
                }

                this.props.dispatch({
                    type: 'ADD_USUARIO',
                    dataUsuario
                });
                /**
                 * Cargar listado desplegable
                 */
                if (parseInt(element.FkEstado) !== parseInt(2)) {
                    const dataListaUsuario = {
                        value: element.IdUsuario,
                        label: element.Nombre,
                    }
                    this.props.dispatch({
                        type: 'ADD_LISTAUSUARIO',
                        dataListaUsuario
                    });
                }
            }
        }
        this.setState({ sw: true, show: true, brrData: true, }, function () {
            this.props.onCreate(this.state.brrData);
        });
    }
    render() {
        return (
            <div>
                <br></br>
                <div className="divBoton">
                    <Button variant="primary" type="button" onClick={() => this.props.onCancel()}>Volver</Button>
                    <Button variant="primary" type="submit" onClick={this.handleCreateData}>Guardar</Button>
                </div>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.User,
        userLog: state.Login
    }
}

export default connect(mapStateToProps)(BotonGuardarForm);