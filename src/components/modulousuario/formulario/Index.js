import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { DataEstadoUsuario } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import CampoInput from '../../campoinput/Index';
import BotonGuardarForm from './botonguardarform/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Nombre: "",
      Usuario: "",
      Email: "",
      Contrasenia: "",
      Estado: "",
      FkEstado: "",
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
      cont: 1,
      sw: false,
      show: false,
      textMsn: "",
      typeMsn: "",
      brrData: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);
  }

  handleInput(e) {
    const { value, name, id } = e;
    if(name === "Estado"){ this.setState({FkEstado: id});}
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {
      this.setState({
        Nombre: "",
        Usuario: "",
        Email: "",
        Contrasenia: "",
        Estado: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: ""
      });
    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoInput type="text" placeholder="Nombre" name="Nombre" controlId="controlId" onResult={this.handleInput} value={this.state.Nombre} onRequired={true}></CampoInput>
          <CampoInput type="text" placeholder="Usuario" name="Usuario" controlId="formFecha" onResult={this.handleInput} value={this.state.Usuario} onRequired={true}></CampoInput>
          <CampoInput type="email" placeholder="Correo" name="Email" controlId="Email" onResult={this.handleInput} value={this.state.Email} onRequired={true}></CampoInput>
          <CampoInput type="password" placeholder="Contraseña" name="Contrasenia" controlId="Contrasenia" onResult={this.handleInput} value={this.state.Contrasenia} onRequired={true}></CampoInput>
          <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.Estado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
          </DataListInput>
          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
      estado: state.Estado,
  }
}

export default connect(mapStateToProps)(FormularioGeneral);