import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import DataListInput from '../autocompletado/Index';
import CampoInput from '../campoinput/Index';
import ModalGeneral from '../../utils/modalgeneral/Index';
import { RutaApi } from '../../const/Index';
import axios from "axios";

class FormItem extends React.Component {
    constructor(props) {
        super(props);
        this.arrccosto = [];
        this.titleMsn = "";
        this.textMsn = "";
        this.state = {
            DescripcionClasificacion: "",
            NombreCentroCosto: "",
            NombreInsumo: "",
            Unidad: "",
            Cantidad: "",
            CantidadEntregada: "",
            show: false,
            showInsu: false,
        };

        this.handleInput = this.handleInput.bind(this);
        this.handleOnSumit = this.handleOnSumit.bind(this);

        /**
         * Se crean estas funciones para poder llamar una funcion
         * que se encuentra en el componente
         */
        this.childclassoli = React.createRef();
        this.childccostos = React.createRef();
        this.childinsumo = React.createRef();
    }

    handleInput(e) {
        // este e.target me trae lo que tenga el campo
        const { value, name, id } = e;
        //Seleccionar los datos asociados a la clasificacion
        if (name === 'DescripcionClasificacion') {
            this.childccostos.current.handleClick();
            this.childinsumo.current.handleClick();
            this.setState({ Unidad: "" });
            this.arrccosto = [];
            let idrcio = "", sinact = "";
            // Cargar el listado de Centro Costos(Actividad)
            axios.get(RutaApi + 'CentroCosto?_id=' + id)
                .then(res => {

                    for (let index = 0; index < res.data.length; index++) {
                        const element = res.data[index];

                        idrcio = this.props.listrelacioncio.filter(r => r.FkObra === this.props.onDataState.FkObra && r.FkCentroCosto === element.IdCentroCosto);

                        if (idrcio.length > 0) {

                            const dataListaCentroCosto = {
                                value: element.IdCentroCosto,
                                label: element.NombreCentroCosto,
                            }
                            this.arrccosto.push(dataListaCentroCosto);
                        }
                    }
                    /**
                     * Validamos que el arrccosto no este vacio
                     * Si esta vacio le asignamos Sin actividad
                     */
                    if (this.arrccosto.length === 0) {
                        sinact = this.props.listccostos.filter(c => c.label === "Sin actividad");
                        const dataListaCentroCosto = {
                            value: sinact[0].value,
                            label: sinact[0].label,
                        }
                        this.arrccosto.push(dataListaCentroCosto);
                    }
                });
        }
        if (name === "NombreCentroCosto") { this.childinsumo.current.handleClick(); }
        if (name === "NombreInsumo") {
            //Capturamos el id del Centro de costo selecionado
            let cc = this.props.onDataState.dtccosto.filter(u => u.label === this.state.NombreCentroCosto).map(u => u);
            let und = this.props.onDataState.dtinsumo.filter(u => u.label === value).map(u => u.und);
            if (und !== "") { this.setState({ Unidad: und[0] }) }
            // if (cc > 0) {
            //Capturamos los insumos asociados al centro de costos
            let ins = this.props.onDataState.dtinsumo.filter(u => u.idCosto === cc[0].value).map(u => u);
            //Validamos si el insumo seleccionado se encuentra asociado al centro de costos
            let insxcc = ins.filter(u => u.idCosto === id).map(u => u);

            if (insxcc.length === 0) { this.setState({ showInsu: true }) }
            // }

        }

        //Este evento sera el encargado de poder cambiar el estado de los campos de this.state
        this.setState({ [name]: value });
    }
    handleChange = () => {
        this.setState({ show: false });
    }
    handleCloseInsu = (e) => {
        if (e === "No") {
            this.childinsumo.current.handleClick();
            this.setState({ formunidad: "" })
        }
        this.setState({ showInsu: false });
    }
    /**
     * Guardar los datos de items seleccionados
     */
    handleOnSumit(e) {
        const { FechaCreacion, NombreObra, NombreEncargado, NombreContratista } = this.props.onDataState;
        const { CantidadEntregada, DescripcionClasificacion, NombreCentroCosto, NombreInsumo, Cantidad, Unidad } = this.state;

        if (FechaCreacion === "" || NombreObra === "" || NombreEncargado === "" || NombreContratista === "") {
            this.titleMsn = "Advertencia";
            this.textMsn = "Se deben diligenciar los campos de Fecha Solicitud, Obra, Contratista y Encargado.";
            this.setState({ show: true, });
        } else {
            if (DescripcionClasificacion === null || NombreCentroCosto === "" || NombreInsumo === "" || parseFloat(Cantidad) <= parseFloat(0) || Unidad === "") {
                this.titleMsn = "Advertencia";
                this.textMsn = "Se deben seleccionar los campos y la cantidad solicitada no puede ser menor o igual a 0(cero).";
                this.setState({ show: true, Cantidad: "", });
            } else {
                /**
                 * Guardar los items de la solicitud que se esta creando
                 */
                let contnextitems = getIdStoreSoli(this.props.items);
                let contnext = getIdStoreSoli(this.props.list);
                const dataSend = {
                    id: contnextitems,
                    fkidlist: contnext,
                    DescripcionClasificacion,
                    NombreCentroCosto,
                    NombreInsumo,
                    Cantidad,
                    CantidadEntregada,
                    Unidad,
                }
                //Agregar los datos al Store de react-redux
                this.props.dispatch({
                    type: 'ADD_ITEMS',
                    dataSend
                });

                /**
                 * Se llama la función del componente 
                 */
                this.childclassoli.current.handleClick();
                this.childccostos.current.handleClick();
                this.childinsumo.current.handleClick();
                this.setState({
                    // DescripcionClasificacion: null,
                    // NombreCentroCosto: null,
                    // NombreInsumo: null,
                    Cantidad: "",
                    Unidad: "",
                    cl: true,
                });
                this.props.onDisabledDataLis(true);
            }
        }
        e.preventDefault();
    }

    render() {
        return (
            <Form onSubmit={this.handleOnSumit} ref="formItem">
                <DataListInput onSelectAuto={this.handleInput} dataselect={this.props.onDataState.dtclasificacion} onValue={this.state.DescripcionClasificacion} ref={this.childclassoli} placeholder="Clasificación Solicitud" name="DescripcionClasificacion" onRequired={true}>
                </DataListInput>
                {/* <DataListInput onSelectAuto={this.handleInput} dataselect={this.props.onDataState.dtccosto} onValue={this.state.NombreCentroCosto} ref={this.childccostos} placeholder="Centro de Costos" name="NombreCentroCosto" onRequired={true}> */}
                <DataListInput onSelectAuto={this.handleInput} dataselect={this.arrccosto} onValue={this.state.NombreCentroCosto} ref={this.childccostos} placeholder="Actividad" name="NombreCentroCosto" onRequired={true}>
                </DataListInput>
                <Form.Row>
                    <DataListInput onSelectAuto={this.handleInput} dataselect={this.props.onDataState.dtinsumo} onValue={this.state.NombreInsumo} ref={this.childinsumo} onDisabled={false} placeholder="Insumo" name="NombreInsumo" onRequired={true}>
                    </DataListInput>
                    <CampoInput onResult={this.handleInput} value={this.state.Unidad} onRequired={true} onDisabled={true} type="text" placeholder="Unidad" name="Unidad" controlId="unidad">
                    </CampoInput>
                    <CampoInput onResult={this.handleInput} value={this.state.Cantidad} onRequired={true} onDisabled={false} type="number" placeholder="Cantidad" name="Cantidad" controlId="cantidad">
                    </CampoInput>
                </Form.Row>
                <div className="divBoton">
                    <Button variant="primary" type="submit">Agregar</Button>
                </div>
                {this.state.show ? <ModalGeneral onShow={this.state.show} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
                {this.state.showInsu ? <ModalGeneral onShow={this.state.showInsu} onChange={this.handleCloseInsu} onTitle="Insumo" onBody="Este Insumo no esta contemplado en esta actividad, ¿Desea agregarlo?" onIf={true} onClose={true} ></ModalGeneral> : null}
            </Form>
        )
    }
}

function getIdStoreSoli(data) {
    let cont = 1;
    let contO = data.map(dtid => dtid.id);
    if (contO.length > 0) { cont = contO.length + 1 }
    return cont;
}
const mapStateToProps = (state) => {
    return {
        list: state.List,
        items: state.Items,
        listrelacioncio: state.ListaRelacionCIO,
        listccostos: state.ListaCentroCostos,
    }
}
export default connect(mapStateToProps)(FormItem);