import React from 'react';
//import Formulario from './formulario/Index';
import TablaEntregaMaterial from './tablaentregamaterial/Index';

class ModEntregaMaterial extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      swD:true
    }
    this.handleCreateSoli = this.handleCreateSoli.bind(this);
    this.handleCancelSoli = this.handleCancelSoli.bind(this);
  }

  handleCreateSoli(){
    this.setState({swD:false});
  }
  handleCancelSoli(){
    this.setState({swD:true});
  }
  render() {
    return (
      <div className="container">
        {/* {this.state.swD ? <TablaEntregaMaterial onCreate={this.handleCreateSoli}></TablaEntregaMaterial>:<Formulario onCancel={this.handleCancelSoli}></Formulario>} */}
        <TablaEntregaMaterial onCreate={this.handleCreateSoli}></TablaEntregaMaterial>
      </div>
    )
  }
}

export default ModEntregaMaterial;