import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { RutaApi } from '../../../const/Index';
import TablaDatosItemsID from './tablaitemsid/Index';
import Observacion from '../../campoobservacion/Index';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaEntregaMaterial extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expan: false,
            dtestadosolicitud: [],
            loading: true,
        }
        this.dtlis = this.props.list.length;
        this.dtestsoli = this.props.estsoli;
    }
    handleChangeObser = (dt) => {
        // Validaremos en que va el contador del store
        let cont = 1;
        let contO = this.props.observ.map(dtid => dtid.id);
        //Capturamos el nombre del usuario que esta creando la solicitud
        let usr = this.props.lg.map(dtuser => dtuser.dtuser);
        let idusr = this.props.lg.map(dtuser => dtuser.dtuserid);
        if (contO.length > 0) { cont = contO.length + 1 }
        const dataListObserva = {
            id: cont,
            FkSolicitudMaterial: dt.idOb,
            FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
            FkUsuario: idusr[0],
            Usuario: usr[0],//Usuario logueado
            Observaciones: "Comentario: " + dt.valueOb,
        }
        //Guardar los datos observacion asociada a la solicitud
        axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
            .then(res => {
                if (res.data !== 0) {
                    this.props.dispatch({
                        type: 'ADD_OBSERVACION',
                        dataListObserva
                    })
                }
            })
            .catch(error => {
                console.log(error.response);
            });
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onBeforeSaveCell(row, cellName, cellValue) {
        // You can do any validation on here for editing value,
        // return false for reject the editing
        return true;
    }
    /**
    * Guardar la información de la celda
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        //dataIcon(cellValue, );
        if (cellValue !== "Anulada") {
            // const dataSendList = {
            //     dtnumerosolicitud: row.dtnumerosolicitud,
            //     dtfecha: row.dtfecha,
            //     dtobra: row.dtobra,
            //     dtencargado: row.dtencargado,
            //     dtcontratista: row.dtcontratista,
            //     dtsolicitadopor: row.dtsolicitadopor,
            //     dtestado: row.dtestado
            // }
            // //Editar los datos al Store de react-redux
            // this.props.dispatch({
            //     type: 'UPDATE_LIST',
            //     id: row.id,
            //     dataSendList
            // });
        }
    }

    onLengthData = () => {

    }

    isExpandableRow(row) {
        //if (row.id < DataListPrueba.length+1) return true;
        // if (row.id < this.dtlis+1) return true;
        // else return false;
        return true;
    }

    expandComponent(row) {
        /**
         * Creamos una variable let tipo lista
         * Recorremos el row.DataListItems para poder colocar el campo de CantidadEntregada en 0
         * de esta manera poder validar si los items estan vacios
         * Se realiza esto, ya que en la base de datos el campo de CantidadEntregada es de tipo entero(int)
         * y su valor por defecto es 0
         */
        let dtItem = [];
        for (let index = 0; index < row.DataListItems.length; index++) {
            const element = row.DataListItems[index];
            const dataUpdete = {
                Cantidad: element.Cantidad,
                CantidadEntregada: "",
                DescripcionClasificacion: element.DescripcionClasificacion,
                NombreCentroCosto: element.NombreCentroCosto,
                CodCentroCosto: element.CodCentroCosto,
                NombreInsumo: element.NombreInsumo,
                CodInsumo: element.CodInsumo,
                Unidad: element.Unidad,
                id: element.id,
            }
            dtItem.push(dataUpdete);
        }

        return (
            <TablaDatosItemsID onDataItems={dtItem} onEncargado={row.NombreEncargado} onNroSolici={row.NumeroSolicitud} onIdSolici={row.id} key={row.id} />
        );
    }
    /**
     * Carga de datos 
     */

    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = async () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        this.props.dispatch({
            type: 'CLEAN_LIST'
        });

        this.props.dispatch({
            type: 'CLEAN_OBSERVACION'
        });
        const accObra = this.props.lg;
        //Cargar la información del usuario logeado para poder sacar que accesos a obras tiene
        //Consultar que obras tiene asociadas
        let resObr = await axios.get(RutaApi + 'AccesoObra?_id=' + accObra[0].dtuserid);
        let dataSendListItem = {}, acobradata = resObr.data.map(a => a.FkObra);
        //Cargar la informacion de las solicitudes y  las guarda en el store
        let res = await axios.get(RutaApi + 'SolicitudMaterial');
        for (let index = 0; index < res.data.length; index++) {
            this.arrList = [];
            const element = res.data[index];
            //Consultamos la obra para darnos cuenta si se encuentra inactiva 
            let estobra = this.props.obra.filter(ob => ob.id === element.FkObra).map(ob => ob.FkEstado);
            estobra = estobra[0];
            //Validamos que la obra no este inactiva
            if (parseInt(estobra) !== parseInt(2)) {
                //Recorremos las obras asignadas al usuario logeado
                for (let index = 0; index < acobradata.length; index++) {
                    const elementAccObra = acobradata[index];
                    if (element.FkObra === elementAccObra) {

                        for (let i = 0; i < element.DataListItems.length; i++) {
                            const e = element.DataListItems[i];
                            dataSendListItem = {
                                id: e.IdSolicitudMaterialItem,
                                fkidlist: e.FkSolicitudMaterial,
                                DescripcionClasificacion: e.DescripcionClasificacion,
                                NombreCentroCosto: e.NombreCentroCosto,
                                CodCentroCosto: e.CodCentroCosto,
                                NombreInsumo: e.NombreInsumo,
                                CodInsumo: e.CodInsumo,
                                Cantidad: e.Cantidad,
                                CantidadEntregada: e.CantidadEntregada,
                                Unidad: e.Unidad,
                            }
                            // this.setState({ arrList: [...this.state.arrList, dataSendListItem] });
                            this.arrList.push(dataSendListItem);
                        }
                        const dataSendList = {
                            id: element.IdSolicitudMaterial,
                            NumeroSolicitud: element.NumeroSolicitud,
                            FechaCreacion: element.FechaCreacion,
                            NombreObra: element.NombreObra,
                            NombreEncargado: element.NombreEncargado,
                            NombreContratista: element.NombreContratista,
                            Usuario: element.Usuario,//Usuario logueado
                            FkObservacion: element.IdSolicitudMaterial,
                            // DataListItems: this.state.arrList,
                            DataListItems: this.arrList,
                            EstadoSolicitud: element.EstadoSolicitud,
                        }
                        this.props.dispatch({
                            type: 'ADD_LIST',
                            dataSendList
                        });

                    }
                }

            }
        }
        //Cargar la informacion de las observaciones y las guarda en el store
        let resObs = await axios.get(RutaApi + 'Observacion');
        for (let index = 0; index < resObs.data.length; index++) {
            const element = resObs.data[index];
            const dataListObserva = {
                id: element.IdObservacion,
                FkSolicitudMaterial: element.FkSolicitudMaterial,
                FechaCreacion: element.FechaCreacion,
                FkUsuario: element.FkUsuario,
                Usuario: element.Usuario,//Usuario logueado
                Observaciones: element.Observaciones,
            }
            this.props.dispatch({
                type: 'ADD_OBSERVACION',
                dataListObserva
            });
        }
        // axios.get(RutaApi + 'Observacion')
        //     .then(resObs => {
        //     })
        setTimeout(() => { this.onChangeLoading(); }, 300);
    }
    onChangeLoading = () => {
        this.setState({ loading: false });
    }
    render() {
        let dataList = this.props.list.filter(p => p.EstadoSolicitud === 'Pendiente por Entregar'), dataObsvr = this.props.observ;
        // const dataObsvr = DataObservacionPrueba;
        //Para poder generar el comentario
        const ref = React.createRef();
        const createObservationEditor = (onUpdate, props) => (<Observacion onUpdate={onUpdate}  {...props} ref={ref} onChangeObser={this.handleChangeObser} onObser={dataObsvr} />)

        //Mostrar los iconos dependiendo del estado
        const dataIcon = (cell, row, file) => {
            let ele = null;
            this.props.listestsolicitud.map(dt => {
                if (row.EstadoSolicitud === dt.value) {
                    ele = `<i class='fa fa-${dt.icon}' aria-hidden='true' style='${dt.style}'></i>`;
                }
                return ele;
            });
            return ele;
        }
        const dataIconObserva = (cell, row, rowIndex) => {
            return `<i class='fa fa-pencil' aria-hidden='true' style='cursor:pointer'></i>`;
        }
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell,
            nonEditableRows: function () {
                // Se realiza el filtro para para saber que fila se debe inhabilitar
                // let dtsw = dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada').map(p => p.id);
                // if (dtsw.length > 0) { return dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada').map(p => p.id); }

            }
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            expandBy: 'column',//Esto es para saber que columnas son expandable true o false
        };
        // const createNameEditor = (onUpdate, props) => (<Observacion onUpdate={onUpdate}  {...props} />);
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable
                        data={dataList}
                        bodyStyle={{ overflow: "overflow" }}
                        options={options}
                        pagination={true}
                        cellEdit={cellEditProp}
                        search={true}
                        expandableRow={this.isExpandableRow}
                        expandComponent={this.expandComponent}
                        expandColumnOptions={{ expandColumnVisible: true }}
                    >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="NumeroSolicitud" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Nro</TableHeaderColumn>
                        <TableHeaderColumn dataField="FechaCreacion" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Fecha</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObra" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Obra</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreContratista" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Contratista</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreEncargado" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Encargado</TableHeaderColumn>
                        <TableHeaderColumn dataField="Usuario" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Solicitado por</TableHeaderColumn>
                        <TableHeaderColumn dataField="EstadoSolicitud" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Estado</TableHeaderColumn>
                        <TableHeaderColumn dataField="dticono" dataSort={true} editable={false} dataFormat={dataIcon} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Icono</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkObservacion" dataFormat={dataIconObserva} customEditor={{ getElement: createObservationEditor }} expandable={false} editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' editable={this.isEditable} dataSort={true}>Observación</TableHeaderColumn>
                    </BootstrapTable>
                }
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.List,
        lg: state.Login,
        observ: state.Observacion,
        listestsolicitud: state.EstadoSolicitud,
        obra: state.Obra,
    }
}


export default connect(mapStateToProps)(TablaEntregaMaterial);