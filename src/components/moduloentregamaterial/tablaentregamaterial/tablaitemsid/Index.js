import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { RutaApi } from '../../../../const/Index';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import Firma from '../../../formfirma/Index';
import Moment from 'moment';
import axios from "axios";

class TablaDatosItemsID extends React.Component {
    constructor(props) {
        super(props);
        this.dtcantientregada = []
        this.state = {
            swmodal: false,
            swfirma: false,
            estado: "",
            dataUpd: {
                DescripcionClasificacion: "",
                NombreCentroCosto: "",
                NombreInsumo: "",
                Unidad: "",
                Cantidad: "",
            },
            dataItems: this.props.onDataItems,
            dataEstSoli: this.props.onEstados,
        };
    }

    handleChangeSw = () => {
        this.setState({ swmodal: false });
    }

    handleChangeFirma = () => {
        this.setState({ swfirma: false });
    }

    handleUpdateEst = (data) => {
        /**
         * Capturamos el nombre del usuario que esta creando la solicitud
        */
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid);
        /**
          * Capurar el id siguiente para las observaciones
          */
        let contObs = 0, comObser = "";
        let contObser = this.props.observ.map(dtid => dtid.id);
        if (contObser.length > 0) { contObs = contObser.length + 1 } else { contObs = 1 }
        if (data.est === "Finalizada Sin Entrega") {
            comObser = "Cambio de estado: " + data.est;
        } else {
            comObser = "Cambio de estado: " + data.est;
        }
        const dataListObserva = {
            id: contObs,
            FkSolicitudMaterial: data.idSoli,
            FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
            FkUsuario: usr[0],//Usuario logueado
            Observaciones: comObser,
        }
        axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
            .then(res => {
                //console.log(res.data);
                if (res.data !== 0) {
                    this.props.dispatch({
                        type: 'ADD_OBSERVACION',
                        dataListObserva
                    });
                }
            })
            .catch(error => {
                console.log("Error observación :" + error.response);
            });

        /**
         * Cambiar el estado de la solicitud
         */
        // consultar el id del estado
        let dtestsol = this.props.estsoli.filter(p => p.value === data.est);
        const dataSendListAxios = {
            IdSolicitudMaterial: data.idSoli,
            FkObra: 0,
            FkEstadoSolicitud: dtestsol[0].id,
            DataListItems: this.dtcantientregada
        }
        axios.post(RutaApi + 'SolicitudMaterial', JSON.stringify(dataSendListAxios))
            .then(res => {
                if (res.data !== 0) {
                    const dataSendList = {
                        EstadoSolicitud: data.est
                    }
                    //Editar los datos al Store de react-redux
                    this.props.dispatch({
                        type: 'UPDATE_ESTADOLIST',
                        id: data.idSoli,
                        dataSendList
                    });
                }
            })
            .catch(error => {
                console.log("Error Solicitud Material :" + error.response);
            });
        // /**
        //  * Agregamos la firma
        //  */

        const dataListFirma = {
            NumeroSolicitud: data.idSoli,
            FkUsuario: usr[0],
            FirmaUsuario: data.firma,
            FechaFirma: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
        }
        // Guardar la firma
        axios.post(RutaApi + 'Firma', JSON.stringify(dataListFirma))
            .then(res => {
                if (res.data !== 0) {
                    this.props.dispatch({
                        type: 'ADD_FIRMA',
                        dataListFirma
                    });
                }
            })
            .catch(error => {
                console.log("Error firma :" + error.response);
            });
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        /**
         * Validar si el el numero ingresado es mayor que la cantidad solicitada
         */
        row.CantidadEntregada = row.CantidadEntregada.replace(/[,]/g, ".");
        row.CantidadEntregada = row.CantidadEntregada.replace(/[-]/g, "");
        console.log(row.CantidadEntregada);
        // if (parseFloat(row.Cantidad) < parseFloat(row.CantidadEntregada)) {
        //     this.setState({ swmodal: true });
        //     row.CantidadEntregada = "";
        // } else {
        //     /**
        //     * Validar que todos los campos de entrega no esten vacios
        //     */
        //     let sw = false, est = "", data = this.state.dataItems;
        //     for (let i = 0; i < data.length; i++) {
        //         if (data[i].CantidadEntregada === "") {
        //             sw = false;
        //             break;
        //         } else {
        //             sw = true;
        //         }

        //     }
        //     if (sw === true) {
        //         /**
        //          * Si estan llenos de todos los datos de cantidad de entrega,
        //          * para validar si estan todos en 0
        //          */
        //         for (let i = 0; i < data.length; i++) {
        //             if (data[i].CantidadEntregada === "0") {
        //                 est = "Finalizada Sin Entrega";
        //             } else {
        //                 est = "Pendiente por Validar";
        //                 break;
        //             }
        //         }
        //         /**
        //          * Recorremos los items para sacar la cantidad entregada
        //          */
        //         for (let index = 0; index < data.length; index++) {
        //             const element = data[index];
        //             const dtcantientre = {
        //                 IdSolicitudMaterialItem: element.id,
        //                 FkSolicitudMaterial: this.props.onIdSolici,
        //                 CantidadEntregada: element.CantidadEntregada
        //             }
        //             this.dtcantientregada.push(dtcantientre);
        //         }
        //         /**
        //          * Si el estado es 'Pendiente por Validar', debe mostrar el formulario de 'firma digital'.
        //          * Si el estado es 'Finalizada Sin Entrega', se cambia el estado en el store.
        //          * 
        //          */
        //         if (est === 'Pendiente por Validar') {
        //             this.setState({ swfirma: true, estado: est });
        //         } else {
        //             let chngest = { "est": est, "idSoli": this.props.onIdSolici }
        //             this.handleUpdateEst(chngest);
        //         }
        //     }
        // }
    }

    render() {
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
        };
        // let lg = this.props.lg;
        // let ed = false;
        // lg.map(dt => {
        //     if (dt.dtu === "1") {
        //         ed = { type: 'number' };
        //     } else {
        //         ed = false;
        //     }
        //     return ed;
        // });
        return (
            <div>
                <BootstrapTable data={this.state.dataItems} options={options} pagination={true} cellEdit={cellEditProp}
                    search={true} >
                    <TableHeaderColumn dataField="id" isKey={true} hidden={true}>Id</TableHeaderColumn>
                    <TableHeaderColumn dataField="fkidlist" hidden={true}>FkId</TableHeaderColumn>
                    <TableHeaderColumn dataField="DescripcionClasificacion" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Clasificación</TableHeaderColumn>
                    <TableHeaderColumn dataField="NombreCentroCosto" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Actividad</TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="CodCentroCosto" editable={false} headerText="Código Actividad">Cod. Actividad</TableHeaderColumn> */}
                    <TableHeaderColumn dataField="NombreInsumo" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="CodInsumo" editable={false} headerText="Código Insumo">Codi. Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="Unidad" editable={false}>Unidad</TableHeaderColumn>
                    <TableHeaderColumn dataField="Cantidad" editable={false} >Cantidad solicitada</TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="CantidadEntregada" editable={ed} >Cantidad entregada</TableHeaderColumn> */}
                    <TableHeaderColumn dataField="CantidadEntregada" editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'number' }:false} >Cantidad entregada</TableHeaderColumn>
                </BootstrapTable>
                {this.state.swmodal ? <ModalGeneral onShow={this.state.swmodal} onChange={this.handleChangeSw} onTitle="Cantidad de entrega es mayor" onBody="La cantidad de entrega no puede ser mayor a la cantidad solicitada" onIf={false} onClose={false} ></ModalGeneral> : null}
                {this.state.swfirma ? <Firma onShow={this.state.swfirma} onChange={this.handleChangeFirma} onEncargado={this.props.onEncargado} onNroSolici={this.props.onNroSolici} onUpdateEst={this.handleUpdateEst} onIdSoli={this.props.onIdSolici} onEstadoSoli={this.state.estado}></Firma> : null}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        lg: state.Login,
        list: state.List,
        observ: state.Observacion,
        estsoli: state.EstadoSolicitud,
    }
}

export default connect(mapStateToProps)(TablaDatosItemsID);