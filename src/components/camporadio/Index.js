import React from 'react';
import { Form, Col } from 'react-bootstrap';

class CampoRadio extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
    }
    handleInput(e) {
        this.props.onResult(e.target);
    }
    render() {
        return (
            <Form.Group as={Col} controlId={this.props.controlId}>
                <Form.Label></Form.Label>
                <Form.Check inline label="Manual" type="radio" id="manual" name={this.props.name} value="Manual" onChange={this.handleInput} checked={!this.props.onChecked}></Form.Check>
                <Form.Check inline label="Masiva" type="radio" id="masiva" name={this.props.name} value="Masiva" onChange={this.handleInput} checked={this.props.onChecked}></Form.Check>
            </Form.Group>
        )
    }
}


export default CampoRadio;