import React from "react";
import Select from "react-select";
import { Form, Col } from 'react-bootstrap';
import './Index.css';


class DataListInput extends React.Component {
    constructor(props) {
        super(props);
        const options = this.props.dataselect;
        this.state = {
            select: {
                value: null,
                options
            }
        };
    }
    setValue = value => {
        this.setState(prevState => ({
            select: {
                ...prevState.select,
                value
            }
        }));
    };

    handleChange = value => {
        this.setValue(value);
        const name = { 'name': this.props.name, 'value': value.label, 'id': value.value };
        this.props.onSelectAuto(name);
    };

    handleClick = () => {
        this.setValue(null);
    };

    render() {
        const { select } = this.state;
        return (
            <Form.Group as={Col} controlId={this.props.name}>
                <Form.Label></Form.Label>
                <Select name={this.props.name} value={select.value} onChange={this.handleChange} options={this.props.dataselect} placeholder={this.props.placeholder} isDisabled={this.props.onDisabled}/>
            </Form.Group >
        );
    }
}
export default DataListInput;