import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import './Index.css';
import Moment from 'moment';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import readXlsFile from 'read-excel-file';
import axios from 'axios';
import { RutaApi } from '../../../../const/Index';

class BotonGuardarForm extends React.Component {
    constructor(props) {
        super(props);
        this.arrnoload = [];
        this.arrexportInsumoAsociado = [];
        this.arrinfo = [];
        this.state = {
            sw: false,
            btn: false,
            btnExport: false,
            brrData: false,
            title: "",
            msj: "",
        };
        this.handleCreateData = this.handleCreateData.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    handleCreateData = async (e) => {
        const { FkObra, NombreObra, FkCentroCosto, NombreCentroCosto, FkInsumo, NombreInsumo, Estado, sw, FkEstado } = this.props.onDataState;
        let result = 0;
        if (sw === true) {
            const input = document.getElementById('dtinsumoasocidofile');
            if (input.files.length === 0) {
                this.setState({ sw: true, brrData: false, })
                this.titleMsn = "Advertencia";
                this.textMsn = "Se debe seleccionar el archivo.";
            } else {
                this.setState({ sw: true, btn: true });
                this.titleMsn = "Carga";
                this.textMsn = "Cargando la informacion, por favor espere mientras termina el proceso...";
                let usr = this.props.user.map(dtuser => dtuser.dtuserid), ccosto = this.props.lstccosto, insumo = this.props.lstinsumo, obra = this.props.lstobra, contRIA = this.props.insumoasociado;
                readXlsFile(input.files[0]).then((rows) => {
                    rows.map(fil => {
                        if (fil[0] !== "Id") {
                            let contIn = contRIA.filter(c => c.CodObra === fil[1] && c.CodCentroCosto === fil[3] && c.CodInsumo === fil[5]);
                            /**
                             * Buscamos el id de los centro de costos, insumos y obras
                             */
                            let idobra = obra.filter(sb => sb.CodObra === fil[1]).map(e => e.value);
                            idobra = idobra[0];
                            let idccosto = ccosto.filter(sb => sb.codCC === fil[3]).map(e => e.value);
                            idccosto = idccosto[0];
                            let idinsumo = insumo.filter(sb => sb.codIN === fil[5]).map(e => e.value);
                            idinsumo = idinsumo[0];
                            if (contIn.length === 0) {
                                /**
                                 * Crear los datos
                                 * Validamos que los campos del excel no esten null
                                 */
                                if (!isNaN(fil[1]) && !isNaN(fil[3]) && !isNaN(fil[5])) {
                                    /**
                                     * Validamos que la consulta a los campos de la bd no esten vacios
                                     */
                                    if (!isNaN(idobra) && idobra !== undefined && !isNaN(idccosto) && idccosto !== undefined && !isNaN(idinsumo) && idinsumo !== undefined) {
                                        const dataIAsociado = {
                                            // IdRelacionCIO: 0,
                                            // FkObra: fil[1],
                                            FkObra: idobra,
                                            NombreObra: fil[2],
                                            // FkCentroCosto: fil[3],
                                            FkCentroCosto: idccosto,
                                            NombreCentroCosto: fil[4],
                                            // FkInsumo: fil[5],
                                            FkInsumo: idinsumo,
                                            NombreInsumo: fil[6],
                                            FkEstado: fil[7],
                                            Estado: fil[8],
                                            Cargue: "Masiva",
                                            FkUsuarioCreador: usr[0],
                                            FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),
                                        }
                                        /**
                                        * Guardar la informacón en la base de datos
                                        */
                                        axios.post(RutaApi + 'RelacionCostoInsumoObra', JSON.stringify(dataIAsociado))
                                            .then(res => {
                                                // console.log(res.data);
                                                result = res.data;
                                            })
                                            .catch(error => {
                                                console.log(error.response);
                                                this.titleMsn = "Error";
                                                this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                                            });
                                    } else {
                                        const dataIAsociado = {
                                            id: fil[0],
                                            CodObra: fil[1],
                                            // FkObra: idobra,
                                            NombreObra: fil[2],
                                            CodCentroCosto: fil[3],
                                            // FkCentroCosto: idccosto,
                                            NombreCentroCosto: fil[4],
                                            CodInsumo: fil[5],
                                            // FkInsumo: idinsumo,
                                            NombreInsumo: fil[6],
                                            FkEstado: fil[7],
                                            Estado: fil[8],
                                        }
                                        this.arrnoload.push(dataIAsociado);
                                    }

                                } else {
                                    const dataIAsociado = {
                                        id: fil[0],
                                        CodObra: fil[1],
                                        // FkObra: idobra,
                                        NombreObra: fil[2],
                                        CodCentroCosto: fil[3],
                                        // FkCentroCosto: idccosto,
                                        NombreCentroCosto: fil[4],
                                        CodInsumo: fil[5],
                                        // FkInsumo: idinsumo,
                                        NombreInsumo: fil[6],
                                        FkEstado: fil[7],
                                        Estado: fil[8],
                                    }
                                    this.arrnoload.push(dataIAsociado);
                                }

                            } else {
                                /**
                                 * Modificar los datos
                                 * Validamos que los campos del excel no esten null
                                 */
                                if (!isNaN(fil[0]) && !isNaN(fil[1]) && !isNaN(fil[3]) && !isNaN(fil[5])) {
                                    /**
                                     * Validamos que la consulta a los campos de la bd no esten vacios
                                     */
                                    if (!isNaN(idobra) && idobra !== undefined && !isNaN(idccosto) && idccosto !== undefined && !isNaN(idinsumo) && idinsumo !== undefined) {
                                        const dataIAsociado = {
                                            IdRelacionCIO: fil[0],
                                            // FkObra: fil[1],
                                            FkObra: idobra,
                                            NombreObra: fil[2],
                                            // FkCentroCosto: fil[3],
                                            FkCentroCosto: idccosto,
                                            NombreCentroCosto: fil[4],
                                            // FkInsumo: fil[5],
                                            FkInsumo: idinsumo,
                                            NombreInsumo: fil[6],
                                            FkEstado: fil[7],
                                            Estado: fil[8],
                                            Cargue: "Masiva",
                                            FkUsuarioModificador: usr[0],
                                            FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                                        }
                                        /**
                                        * Modificar la informacón en la base de datos
                                        */
                                        axios.post(RutaApi + 'RelacionCostoInsumoObra', JSON.stringify(dataIAsociado))
                                            .then(res => {
                                                //console.log(res.data);
                                                result = res.data;
                                            })
                                            .catch(error => {
                                                //console.log(error.response);
                                                this.titleMsn = "Error";
                                                this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                                            });
                                    } else {
                                        const dataIAsociado = {
                                            id: fil[0],
                                            CodObra: fil[1],
                                            // FkObra: idobra,
                                            NombreObra: fil[2],
                                            CodCentroCosto: fil[3],
                                            // FkCentroCosto: idccosto,
                                            NombreCentroCosto: fil[4],
                                            CodInsumo: fil[5],
                                            // FkInsumo: idinsumo,
                                            NombreInsumo: fil[6],
                                            FkEstado: fil[7],
                                            Estado: fil[8],
                                        }
                                        this.arrnoload.push(dataIAsociado);
                                    }
                                } else {
                                    const dataIAsociado = {
                                        id: fil[0],
                                        CodObra: fil[1],
                                        // FkObra: idobra,
                                        NombreObra: fil[2],
                                        CodCentroCosto: fil[3],
                                        // FkCentroCosto: idccosto,
                                        NombreCentroCosto: fil[4],
                                        CodInsumo: fil[5],
                                        // FkInsumo: idinsumo,
                                        NombreInsumo: fil[6],
                                        FkEstado: fil[7],
                                        Estado: fil[8],
                                    }
                                    this.arrnoload.push(dataIAsociado);
                                }
                            }
                            return contIn;

                        }else{
                            return null;
                        }
                    })
                })
                setTimeout(() => { this.handleShowMsj(result); }, 700);
                // setTimeout(() => { this.handleShowMsj(1); }, 700);
            }
        } else {
            if (NombreObra === "" || NombreCentroCosto === "" || NombreInsumo === "" || Estado === "") {
                this.setState({ sw: true, brrData: false, title: "Advertencia", msj: "Se debe diligenciar el formulario completo." })
            } else {
                //Agregar informacion a store de 
                this.setState({ sw: false, });
                let usr = this.props.user.map(dtuser => dtuser.dtuserid);
                const dataIAsociado = {
                    FkObra: FkObra,
                    NombreObra: NombreObra,
                    FkCentroCosto: FkCentroCosto,
                    NombreCentroCosto: NombreCentroCosto,
                    FkInsumo: FkInsumo,
                    NombreInsumo: NombreInsumo,
                    FkEstado: FkEstado,
                    Estado: Estado,
                    Cargue: "Manual",
                    FkUsuarioCreador: usr[0],//this.props.onDataState.formcreadopor,
                    FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),//this.props.onDataState.formfechacreacion,
                }
                /**
                * Guardar la informacón en la base de datos
                */
                axios.post(RutaApi + 'RelacionCostoInsumoObra', JSON.stringify(dataIAsociado))
                    .then(res => {
                        //console.log(res.data);
                        result = res.data;
                    })
                    .catch(error => {
                        //console.log(error.response);
                        this.titleMsn = "Error";
                        this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                    });
                setTimeout(() => { this.handleShowMsj(result); }, 700);
            }
        }
    }

    handleShowMsj = async (result) => {
        this.handleChange();
        if (this.arrnoload.length > 0) {
            /**
             * Se crea una lista, para poder agregar la información del maestro
             */
            let dt = [];
            this.arrnoload.forEach(element => {
                const inf = [element.id, element.CodObra, element.NombreObra, element.CodCentroCosto, element.NombreCentroCosto, element.CodInsumo, element.NombreInsumo, element.FkEstado, element.Estado];
                dt.push(inf);
            });
            /**
             * Si la lista no tiene información, se le agrega los campos vacios, 
             * para que se visualice de manera correcta el formato
             */
            if (dt.length <= 0) {
                dt.push(["", "", "", "", ""]);
            }
            // const arrexportInsumoAsociado = [
            this.arrexportInsumoAsociado = [
                {
                    columns: ["Id", "Código Obra", "Nombre Obra", "Código Actividad", "Nombre Actividad", "Código Insumo", "Nombre Insumo", "Código Estado", "Estado"],
                    data: dt
                },
            ];

            // // const arrinfo = [
            this.arrinfo = [
                {
                    columns: ["Instrucciones para subir el archivo por Carga Masiva"],
                    data: [
                        ["- Ir a la Hoja FormatoInsumoAsociado."],
                        ["- Borrar los registros que no se necesiten actualizar."],
                        ["- Diligenciar las columnas Código  Obra, Código Actividad, Código Insumo y Código Estado."],
                        ["- La columna Nombre Obra,  Nombre Actividades, Nombre Insumo y Estado son informativos, no debes diligenciarlos para la carga."],
                        ["- Tener en cuenta que el codigo del Estado es: 1 = Activo y 2 = Inactivo."],
                        ["- Eliminar esta hoja (Instrucciones),en el archivo de excel solo debe existir la hoja (FormatoInsumoAsociado)."],
                    ]
                }
            ];
            // console.log(this.arrexportInsumoAsociado);
            this.setState({ btnExport: true });
        }
        // Validamos la variable para poder mostrar el mensaje
        if (parseInt(result) === parseInt(0)) {
            this.titleMsn = "Advertencia";
            this.textMsn = "Se presento un inconveniente al insertar la información.";
        } else {
            this.titleMsn = "Registro exitoso";
            this.textMsn = "Se registro la información";
            /**
             * Se limpia los datos para que no se dupliquen cada ves que se cargue
             */
            this.props.dispatch({
                type: 'CLEAN_INSUMOASOCIADO'
            });
            let res = await axios.get(RutaApi + 'RelacionCostoInsumoObra');
            for (let index = 0; index < res.data.length; index++) {
                const element = res.data[index];
                const dataIAsociado = {
                    id: element.IdRelacionCIO,
                    FkCentroCosto: element.FkCentroCosto,
                    NombreCentroCosto: element.NombreCentroCosto,
                    CodCentroCosto: element.CodCentroCosto,
                    FkInsumo: element.FkInsumo,
                    NombreInsumo: element.NombreInsumo,
                    CodInsumo: element.CodInsumo,
                    FkObra: element.FkObra,
                    NombreObra: element.NombreObra,
                    CodObra: element.CodObra,
                    FkEstado: element.FkEstado,
                    Estado: element.Estado,
                    Cargue: element.Cargue,
                    FkUsuarioCreador: element.FkUsuarioCreador,
                    UsuarioCreador: element.UsuarioCreador,
                    FechaCreacion: element.FechaCreacion,
                    FkUsuarioModificador: element.FkUsuarioModificador,
                    UsuarioModificador: element.UsuarioModificador,
                    FechaModificacion: element.FechaModificacion,
                }

                this.props.dispatch({
                    type: 'ADD_INSUMOASOCIADO',
                    dataIAsociado
                });
            }
        }

        this.setState({ sw: true, show: true, btn: false, brrData: true, }, function () {
            this.props.onCreate(this.state.brrData);
        });
    }

    render() {
        // console.log(this.props.lstinsumo);onName="FormatoInsumoAsociado"
        return (
            <div>
                <br></br>
                <div className="divBoton">
                    <Button variant="primary" type="button" onClick={() => this.props.onCancel()} disabled={this.state.btn}>Volver</Button>
                    <Button variant="primary" type="submit" onClick={this.handleCreateData} disabled={this.state.btn}>Guardar</Button>
                </div>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} onExport={this.state.btnExport} onExportColumn={this.arrexportInsumoAsociado} onExportInfo={this.arrinfo} onExportName="FormatoInsumoAsociado"></ModalGeneral> : null}

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        insumoasociado: state.InsumosAsociados,
        user: state.Login,
        lstccosto: state.ListaCentroCostos,
        lstinsumo: state.ListaInsumos,
        lstobra: state.ListaObra,
    }
}

export default connect(mapStateToProps)(BotonGuardarForm);