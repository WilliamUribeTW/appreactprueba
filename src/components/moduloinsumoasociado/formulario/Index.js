import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { DataEstadoUsuario, DataObra, DataCostos, DataInsumo } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';
import CampoInput from '../../campoinput/Index';
import CampoRadio from '../../camporadio/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      FkObra: "",
      NombreObra: "",
      FkCentroCosto: "",
      NombreCentroCosto: "",
      FkInsumo: "",
      NombreInsumo: "",
      Estado: "",
      FkEstado: "",
      Cargue: "Manual",
      dtinsumoasocidofile: null,
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
      sw: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childobra = React.createRef();
    this.childccosto = React.createRef();
    this.childinsumo = React.createRef();
    this.childestado = React.createRef();
  }

  handleInput(e) {
    const { value, name, id } = e;
    if (name === "NombreObra" ) { this.setState({ FkObra: id }); }
    if (name === "NombreCentroCosto" ) { this.setState({ FkCentroCosto: id }); }
    if (name === "NombreInsumo" ) { this.setState({ FkInsumo: id }); }
    if (name === "Estado" ) { this.setState({ FkEstado: id }); }
    if (name === "Cargue") { if (value === "Masiva") { this.setState({ sw: true }); } else { this.setState({ sw: false }); } }
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {

      if (this.state.sw === false) {
        /**
         * Se llama la función del componente 
         */
        this.childobra.current.handleClick();
        this.childccosto.current.handleClick();
        this.childinsumo.current.handleClick();
        this.childestado.current.handleClick();
      };

      this.setState({
        FkObra: "",
        NombreObra: "",
        FkCentroCosto: "",
        NombreCentroCosto: "",
        FkInsumo: "",
        NombreInsumo: "",
        FkEstado: "",
        Estado: "",
        Cargue: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: "",
        sw: false,
      });

    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoRadio onResult={this.handleInput} name="Cargue" onChecked={this.state.sw}></CampoRadio>
          {this.state.sw ?
            <CampoInput onResult={this.handleInput} type="file" placeholder="Archivo" name="dtinsumoasocidofile" controlId="dtinsumoasocidofile" id="dtinsumoasocidofile"></CampoInput>
            :
            <div>
              <DataListInput dataselect={this.props.obra} onSelectAuto={this.handleInput} valueText={this.state.NombreObra} ref={this.childobra} placeholder="Seleccione la obra" name="NombreObra" list="NombreObra" onRequired={true}>
              </DataListInput>
              <DataListInput dataselect={this.props.centrocostos} onSelectAuto={this.handleInput} valueText={this.state.NombreCentroCosto} ref={this.childccosto} placeholder="Seleccione el actividad" name="NombreCentroCosto" list="NombreCentroCosto" onRequired={true}>
              </DataListInput>
              <DataListInput dataselect={this.props.insumo} onSelectAuto={this.handleInput} valueText={this.state.NombreInsumo} ref={this.childinsumo} placeholder="Seleccione el insumo" name="NombreInsumo" list="NombreInsumo" onRequired={true}>
              </DataListInput>
              <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.Estado} ref={this.childestado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
              </DataListInput>
            </div>
          }
          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
      insumoasociado: state.InsumosAsociados,
      lg: state.Login,
      estado: state.Estado,
      centrocostos: state.ListaCentroCostos,
      obra: state.ListaObra,
      insumo: state.ListaInsumos,
  }
}

export default connect(mapStateToProps)(FormularioGeneral);