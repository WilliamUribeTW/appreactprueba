import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { /*DataEstadoUsuario, DataObra, DataCostos, DataInsumo,*/ RutaApi } from '../../../const/Index';
import Moment from 'moment';
import ExportarInfo from '../../botonexportar/Index';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaInsumoAsociado extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        /**
         * Se crea una lista, para poder agregar la información del maestro
         */
        let dt = [];
        this.props.insumoasociado.forEach(element => {
            const inf = [element.id,element.CodObra, element.NombreObra, element.CodCentroCosto, element.NombreCentroCosto, element.CodInsumo, element.NombreInsumo, element.FkEstado, element.Estado];
            dt.push(inf);
        });
        /**
         * Si la lista no tiene información, se le agrega los campos vacios, 
         * para que se visualice de manera correcta el formato
         */
        if(dt.length <= 0){
            dt.push(["","", "", "", ""]);
        }
        const arrexportInsumoAsociado = [
            { 
                columns: ["Id","Código Obra","Nombre Obra", "Código Actividad","Nombre Actividad", "Código Insumo", "Nombre Insumo", "Código Estado", "Estado"],
                data: dt
            },
        ];
        /**
         * Array para dar las instrucciones de como actualizar el excel
         */
        const arrinfo = [
            {
                columns: ["Instrucciones para subir el archivo por Carga Masiva"],
                data: [
                    ["- Ir a la Hoja FormatoInsumoAsociado."],
                    ["- Borrar los registros que no se necesiten actualizar."],
                    ["- Diligenciar las columnas Código  Obra, Código Actividad, Código Insumo y Código Estado."],
                    ["- La columna Nombre Obra,  Nombre Actividades, Nombre Insumo y Estado son informativos, no debes diligenciarlos para la carga."],
                    ["- Tener en cuenta que el codigo del Estado es: 1 = Activo y 2 = Inactivo."],
                    ["- Eliminar esta hoja (Instrucciones),en el archivo de excel solo debe existir la hoja (FormatoInsumoAsociado)."],
                ]
            }
        ];
        return (
            <div>
                {parseInt(crud[0].dtc) !== parseInt(0) && <div>
                    <ExportarInfo onData={this.props.insumoasociado} onName="FormatoInsumoAsociado" onColumn={arrexportInsumoAsociado} onInfo={arrinfo}></ExportarInfo>
                    <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear Insumo Asociado</button>
                </div>}
            </div>
        );
    }
    /**
     * Ir a crear
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_INSUMOASOCIADO',
                id: items[i]
            })
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), valob = 0, valcc = 0, valins = 0, valest = 0, valcargue = "";

        if (cellName === "NombreObra") { valob = this.props.obra.filter(c => c.label === row.NombreObra).map(c => c.value); valob = valob[0] } else { valob = row.FkObra }

        if (cellName === "NombreCentroCosto") { valcc = this.props.centrocostos.filter(c => c.label === row.NombreCentroCosto).map(c => c.value); valcc = valcc[0] } else { valcc = row.FkCentroCosto }

        if (cellName === "NombreInsumo") { valins = this.props.insumo.filter(c => c.label === row.NombreInsumo).map(c => c.value); valins = valins[0] } else { valins = row.FkInsumo }

        if (cellName === "Estado") { valest = this.props.estado.filter(c => c.label === cellValue).map(c => c.value); valest = valest[0]; } else { valest = row.FkEstado }

        if (row.Cargue === "") { valcargue = "Manual"; } else { valcargue = row.Cargue; }
        if (valob === row.FkObra && valcc === row.FkCentroCosto && valins === row.FkInsumo && valest === row.FkEstado) {
            // console.log("no actualizar");
        } else {
            const dataIAsociado = {
                IdRelacionCIO: row.id,
                FkCentroCosto: valcc,
                FkInsumo: valins,
                FkObra: valob,
                FkEstado: valest,
                Cargue: valcargue,
                FkUsuarioModificador: usr[0],
                FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
            }
            /**
             * Modificar la informacón en la base de datos
             */
            axios.post(RutaApi + 'RelacionCostoInsumoObra', JSON.stringify(dataIAsociado))
                .then(res => {
                    // console.log(res.data);
                })
                .catch(error => {
                    console.log("Se presento un error; se detalla a continuación " + error.response);
                });
            // this.onLoadData();
            this.UNSAFE_componentWillMount();
        }
    }

    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_INSUMOASOCIADO'
        });
        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'RelacionCostoInsumoObra');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataIAsociado = {
                id: element.IdRelacionCIO,
                FkCentroCosto: element.FkCentroCosto,
                NombreCentroCosto: element.NombreCentroCosto,
                CodCentroCosto: element.CodCentroCosto,
                FkInsumo: element.FkInsumo,
                NombreInsumo: element.NombreInsumo,
                CodInsumo: element.CodInsumo,
                FkObra: element.FkObra,
                NombreObra: element.NombreObra,
                CodObra: element.CodObra,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                Cargue: element.Cargue,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_INSUMOASOCIADO',
                dataIAsociado
            });
        }
        //ListaCentroCostos
        //ListaInsumos
        //ListaObra
        setTimeout(() => { this.onChangeLoading(); }, 300);
    };


    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataIA = this.props.insumoasociado;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        // const selectRow = {
        //     mode: 'checkbox',
        //     onSelect: this.onSelectDelete

        // };//deleteRow selectRow={selectRow}
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataIA} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkCentroCosto" hidden={true}># FkCentroCosto</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkInsumo" hidden={true}># FkInsumo</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkObra" hidden={true}># FkObra</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkEstado" hidden={true}># FkEstado</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodCentroCosto" hidden={true}># CodCentroCosto</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodInsumo" hidden={true}># CodInsumo</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodObra" hidden={true}># CodObra</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObra" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.obra.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Obra</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreCentroCosto" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.centrocostos.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Actividad</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreInsumo" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.insumo.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Insumo</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Estado Insumo Asociado">Est. Ins. Asociado</TableHeaderColumn>
                        <TableHeaderColumn dataField="Cargue" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Cargue</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        insumoasociado: state.InsumosAsociados,
        lg: state.Login,
        estado: state.Estado,
        centrocostos: state.ListaCentroCostos,
        obra: state.ListaObra,
        insumo: state.ListaInsumos,
    }
}


export default connect(mapStateToProps)(TablaInsumoAsociado);