import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { RutaApi } from '../../const/Index';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class ModEstadoSolicitud extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, }
  }


  /**
   * Carga de datos 
   * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
   * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
   */
  componentDidMount() {
    this._isMounted = true;
    const handler = e => this.setState({ matches: e.matches });
    window.matchMedia("(max-width: 1300px)").addListener(handler);
    if (window.innerWidth < 1280) {
      this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
    } else {
      this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
    }
  }
  UNSAFE_componentWillMount() {
    this.onLoadData();
  }
  onLoadData = () => {
    if (!this.state.loading) {
      this.setState({ loading: true });
    }
    /**
     * Se limpia los datos para que no se dupliquen cada ves que se cargue
     */
    this.props.dispatch({
      type: 'CLEAN_ESTADOSOLICITUD'
    });

    setTimeout(() => { this.getData(); }, 100);
  }
  /**
   * Cargamos la información del maestro 
   */
  getData = async () => {
    let res = await axios.get(RutaApi + 'EstadoSolicitud');
    //let { data } = res.data;
    for (let index = 0; index < res.data.length; index++) {
      const element = res.data[index];
      const dataListSoliEstado = {
        id: element.IdEstadoSolicitud,
        NombreEstadoSolicitud: element.NombreEstadoSolicitud,
        icon: element.icon,
        style: element.style,
      }

      this.props.dispatch({
        type: 'ADD_ESTADOSOLICITUD',
        dataListSoliEstado
      });
    }

    setTimeout(() => { this.onChangeLoading(); }, 100);
  };
  onChangeLoading = () => {
    this.setState({ loading: false });
  }

  render() {
    //Mostrar los iconos dependiendo del estado
    const dataIcon = (cell, row, file) => {
      let ele = null;
      this.props.estsoli.map(dt => {
        if (row.NombreEstadoSolicitud === dt.NombreEstadoSolicitud) {
          ele = `<i class='fa fa-${dt.icon}' aria-hidden='true' style='${dt.style}'></i>`;
        }
        return ele;

      });
      return ele;
    }
    const options = {
      noDataText: 'No hay datos en el momento',
      expandBy: 'column',//Esto es para saber que columnas son expandable true o false
    };
    const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;`;
    return (
      <div className="container">
        {this.state.loading ?
          <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
          :
          <BootstrapTable data={this.props.estsoli} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} search={true} >
            <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
            <TableHeaderColumn dataField="NombreEstadoSolicitud" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Estado</TableHeaderColumn>
            <TableHeaderColumn dataField="icon" dataSort={ true } dataFormat={dataIcon} /*customEditor={{ getElement: createAprobacion }}*/ editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' expandable={false} editable={false}>Icono</TableHeaderColumn>
          </BootstrapTable>
        }

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lg: state.Login,
    estsoli: state.EstadoSolicitud,
  }
}

export default connect(mapStateToProps)(ModEstadoSolicitud);