import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { DataEstado, DataListPrueba, DataObservacionPrueba } from '../../const/Index';
//import Aprovacion from '../../../utils/aprobacion/Index';
import Observacion from './campoobservacion/Index';
import TablaDatosItemsID from './tablaitemsid/Index';
import Moment from 'moment';

class TablaSolicitud extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expan: false,
            swModel: true,
            muesApr: true,
            mst: true,
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        //this.handleModal = this.handleModal.bind(this);
        this.dtlis = this.props.list.length;
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        return (
            <button className="botonCrear" onClick={this.handleGoCreate}>Crear solicitud</button>
        );
    }
    /**
     * Otras funciones
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    handleChangeObser = (dt) => {
        //Capturamos el nombre del usuario que esta creando la solicitud
        let usr = this.props.lg.map(dtuser => dtuser.dtuser);
        //Validaremos en que va el contador del store
        let cont = 0;
        let contO = this.props.observ.map(dtid => dtid.id);
        if (contO.length > 0) { cont = contO.length + 1 } else { cont = 1 }
        //Agregar la observacion inicial
        const dataListObserva = {
            id: cont,
            dtidsolicitud: dt.idOb,
            dtfecha: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
            dtsolicitadopor: usr[0],//Usuario logueado
            dtobservacion: "Comentario: " + dt.valueOb,
        }
        this.props.dispatch({
            type: 'ADD_OBSERVACION',
            dataListObserva
        });
    }
    handleChangeEst = (dt) => {
        //Capturamos el nombre del nuevo estado
        let nextEst = DataEstado.filter(e => e.id === dt.valueEs).map(e => e.value);
        //Actualizamos el estado de la solicitud
        const dataSendList = {
            dtestado: nextEst[0],
        }
        this.props.dispatch({
            type: 'UPDATE_ESTADOLIST',
            id: dt.idSoli,
            dataSendList
        });
        //Capturamos el nombre del usuario que esta creando la solicitud
        let usr = this.props.lg.map(dtuser => dtuser.dtuser);
        //Validaremos en que va el contador del store
        let contObs = 0;
        //Contador para las observaciones
        let contObser = this.props.observ.map(dtid => dtid.id);
        if (contObser.length > 0) { contObs = contObser.length + 1 } else { contObs = 1 }
        //Agregar la observacion inicial
        const dataListObserva = {
            id: contObs,
            dtidsolicitud: dt.idSoli,
            dtfecha: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
            dtsolicitadopor: usr[0],//Usuario logueado
            dtobservacion: "Cambio de estado: Modificada",
        }
        this.props.dispatch({
            type: 'ADD_OBSERVACION',
            dataListObserva
        });
        let dtApr = this.state.muesApr;
        switch (nextEst[0]) {
            case "Cancelada":
                dtApr = false;
                break;
            case "Finalizada Sin Entrega":
                dtApr = false;
                break;
            case "Finalizada":
                dtApr = false;
                break;
            case "Anulada":
                dtApr = false;
                break;
            default:
                break;
        }
        this.setState({ muesApr: dtApr });
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onBeforeSaveCell(row, cellName, cellValue) {
        // You can do any validation on here for editing value,
        // return false for reject the editing
        return true;
    }
    /**
    * Guardar la información de la celda
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        //dataIcon(cellValue, );
        if (cellValue !== "Anulada") {
            const dataSendList = {
                dtnumerosolicitud: row.dtnumerosolicitud,
                dtfecha: row.dtfecha,
                dtobra: row.dtobra,
                dtencargado: row.dtencargado,
                dtcontratista: row.dtcontratista,
                dtsolicitadopor: row.dtsolicitadopor,
                dtestado: row.dtestado
            }
            //Editar los datos al Store de react-redux
            this.props.dispatch({
                type: 'UPDATE_LIST',
                id: row.id,
                dataSendList
            });
        }
    }

    onLengthData = () => {

    }

    isExpandableRow(row) {
        let sw = true
        // switch (row.dtestado) {
        //     case 'Finalizada Sin Entrega':
        //         sw = false;
        //         break;
        //     case 'Finalizada':
        //         sw = false;
        //         break;
        //     case 'Anulada':
        //         sw = false;
        //         break;
        //     case 'Cancelada':
        //         sw = false;
        //         break;
        //     default:
        //         break;
        // }
        return sw;
        //  if (row.dtestado !== 'Cancelada') return true;
        //  else return false;
    }

    expandComponent(row) {
        return (
            <TablaDatosItemsID onDataItems={row.DataListPruebaItems} />
        );
    }
    isEditable = (cell, row) => {
        return true;
    }
    render() {
        // const dataList = this.props.list;
        // const dataObsvr = this.props.observ;
        const dataList = DataListPrueba;
        const dataObsvr = DataObservacionPrueba;
        //Para poder generar el comentario
        const ref = React.createRef();
        const createNameEditor = (onUpdate, props) => (<Observacion onUpdate={onUpdate}  {...props} ref={ref} onChangeObser={this.handleChangeObser} onObser={dataObsvr} />)
        //Cambio de estado
        // const refEst = React.createRef();
        // const createAprobacion = (onUpdate, props) => (<Aprovacion onUpdate={onUpdate}  {...props} ref={refEst} onChangeEst={this.handleChangeEst} onDataEst={DataEstado} />)

        //Mostrar los iconos dependiendo del estado
        const dataIcon = (cell, row, file) => {
            let ele = null;
            DataEstado.map(dt => {
                if (row.dtestado === dt.value) {
                    ele = `<i class='fa fa-${dt.icon}' aria-hidden='true' style='${dt.style}'></i>`;
                }
                return ele;
            });
            return ele;
        }
        const dataIconObserva = (cell, row, rowIndex) => {
            return `<i class='fa fa-pencil' aria-hidden='true' style='cursor:pointer'></i>`;
        }
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell,
            nonEditableRows: function () {

                //console.log(dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada' || p.dtestado === 'Cancelada').map(() => false));
                // Se realiza el filtro para saber que fila se debe inhabilitar
                // let dtsw = dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada' || p.dtestado === 'Cancelada').map(p => p.id);
                // if (dtsw.length > 0) { return dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada' || p.dtestado === 'Cancelada').map(p => p.id); }

            }
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton,
            expandBy: 'column',//Esto es para saber que columnas son expandable true o false
        };


        return (
            <BootstrapTable
                data={dataList}
                bodyStyle={{ overflow: "overflow" }}
                options={options}
                pagination={true}
                cellEdit={cellEditProp}
                search={true}
                expandableRow={this.isExpandableRow}
                expandComponent={this.expandComponent}
                expandColumnOptions={{ expandColumnVisible: true }}
                deleteRow
            >
                <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                <TableHeaderColumn dataField="dtnumerosolicitud" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Nro</TableHeaderColumn>
                <TableHeaderColumn dataField="dtfecha" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Fecha</TableHeaderColumn>
                <TableHeaderColumn dataField="dtobra" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Obra</TableHeaderColumn>
                <TableHeaderColumn dataField="dtencargado" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Encargado</TableHeaderColumn>
                <TableHeaderColumn dataField="dtcontratista" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Contratista</TableHeaderColumn>
                <TableHeaderColumn dataField="dtsolicitadopor" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Solicitado por</TableHeaderColumn>
                <TableHeaderColumn dataField="dtestado" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Estado</TableHeaderColumn>

                <TableHeaderColumn dataField="dticono" dataFormat={dataIcon} /*customEditor={{ getElement: createAprobacion }}*/ editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' expandable={false} editable={false}>Icono</TableHeaderColumn>

                <TableHeaderColumn dataField="dtidobserva" dataFormat={dataIconObserva} customEditor={{ getElement: createNameEditor }} expandable={false} editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' editable={this.isEditable} dataSort={true}>Observación</TableHeaderColumn>
            </BootstrapTable>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        list: state.List,
        observ: state.Observacion,
        lg: state.Login,

    }
}


export default connect(mapStateToProps)(TablaSolicitud);