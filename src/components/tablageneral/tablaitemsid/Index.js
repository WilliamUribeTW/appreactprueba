import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import { DataInsumo, /*DataClasSoli, DataCostos*/  } from '../../../../const/Index';

class TablaDatosItemsID extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataDel: [],
            dataUpd: {
                formclassoli: "",
                formccostos: "",
                forminsumo: "",
                formcantidad: "",
                formunidad: ""
            }
        };
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }

    handleDataDelete(e) {
        const items = this.state.dataDel;
        console.log("items.length");
        console.log(items.length);
        for (var i = 0; i <= items.length; i++) {
            console.log("iteraciones");
            console.log(items[i]);
            console.log(items[i+1]);
            //Eliminar los datos al Store de react-redux
            // this.props.dispatch({
            //     type: 'DELETE_ITEMSLIST',
            //     id: items[i],//Id de los items
            //     fkid: items[i+1]//Id de la solicitud
            // })
        }
    }
    /**
     * Creacion del botón eliminar
     */
    createCustomDeleteButton = (onBtnClick) => {
        return (
            <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar item</button>
        );
    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id, row.fkidlist] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        if (cellName === 'forminsumo') {
            let und = DataInsumo.filter(u => u.label === cellValue).map(u => u.und);
            if (und !== "") { row.formunidad=und }
        }

        const dataSendListItems = {
            formclassoli: row.formclassoli,
            formccostos: row.formccostos,
            forminsumo: row.forminsumo,
            formcantidad: row.formcantidad,
            formunidad: row.formunidad
        }
        //Editar los datos al Store de react-redux
        this.props.dispatch({
            type: 'UPDATE_ITEMSLIST',
            id: row.id,
            fkid: row.fkidlist,
            dataSendListItems
        });
    }

    render() {
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            // btnGroup: this.createCustomDeleteButton,
        };
        // const selectRow = {
        //     mode: 'checkbox',
        //     onSelect: this.onSelectDelete

        // };
        //selectRow={selectRow} deleteRow
        return (
            <BootstrapTable data={this.props.onDataItems} options={options} pagination={true} cellEdit={cellEditProp}
                search={true} >
                <TableHeaderColumn dataField="id" isKey={true} hidden={true}>Id</TableHeaderColumn>
                <TableHeaderColumn dataField="fkidlist" hidden={true}>FkId</TableHeaderColumn>
                {/* <TableHeaderColumn dataField="formclassoli" editable={{ type: 'select', options: { values: DataClasSoli.map(dt => { return dt.label }) } }}>Clasificación</TableHeaderColumn>
                <TableHeaderColumn dataField="formccostos" editable={{ type: 'select', options: { values: DataCostos.map(dt => { return dt.label }) } }}>Centro de Costos</TableHeaderColumn>
                <TableHeaderColumn dataField="forminsumo" editable={{ type: 'select', options: { values: DataInsumo.map(dt => { return dt.label }) } }}>Insumo</TableHeaderColumn>
                <TableHeaderColumn dataField="formunidad" editable={false}>Unidad</TableHeaderColumn>
                <TableHeaderColumn dataField="formcantidad" editable={{ type: 'number' }} >Cantidad</TableHeaderColumn> */}
                <TableHeaderColumn dataField="formclassoli" editable={false}>Clasificación</TableHeaderColumn>
                <TableHeaderColumn dataField="formccostos" editable={false}>Centro de Costos</TableHeaderColumn>
                <TableHeaderColumn dataField="forminsumo" editable={false}>Insumo</TableHeaderColumn>
                <TableHeaderColumn dataField="formunidad" editable={false}>Unidad</TableHeaderColumn>
                <TableHeaderColumn dataField="formcantidad" editable={false} >Cantidad</TableHeaderColumn>
            </BootstrapTable>
        )
    }
}


export default connect()(TablaDatosItemsID);