import { combineReducers } from 'redux';


//De esta manera se crea un store, pero se pueden generar varios, con distintos nombres 
const dataVacio = [];

/**
 * MAESTROS
 */

/**
 * Maestro de Usuarios
 */
const dataUsuarios = (stateUser = [], action) => {
  switch (action.type) {
    case 'ADD_USUARIO':
      return stateUser.concat([action.dataUsuario])
    case 'DELETE_USUARIO':
      return stateUser.filter((item) => item.id !== action.id)
    case 'UPDATE_USUARIO':
      return stateUser.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            Nombre: action.dataUsuario.Nombre,
            Usuario: action.dataUsuario.Usuario,
            Email: action.dataUsuario.Email,
            Contrasenia: action.dataUsuario.Contrasenia,
            Estado: action.dataUsuario.Estado,
            UsuarioModificador: action.dataUsuario.UsuarioModificador,
            FechaModificacion: action.dataUsuario.FechaModificacion
          }
        } else return item;
      })
    case 'CLEAN_USUARIO':
      return stateUser = dataVacio
    default:
      return stateUser;
  }
}
/**
 * Maestro de Obras
 */
const dataObras = (stateObras = [], action) => {
  switch (action.type) {
    case 'ADD_OBRA':
      return stateObras.concat([action.dataObra])
    case 'DELETE_OBRA':
      return stateObras.filter((item) => item.id !== action.id)
    case 'UPDATE_OBRA':
      return stateObras.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            dtnombreobra: action.dataObra.dtnombreobra,
            dtcodobra: action.dataObra.dtcodobra,
            dtcorreoobra: action.dataObra.dtclaveobra,
            dtestadoobra: action.dataObra.dtestadoobra,
            //dtcreadopor: action.dataObra.dtcreadopor,
            //dtfechacreacion: action.dataObra.dtfechacreacion,
            dtmodificadopor: action.dataObra.dtmodificadopor,
            dtfechamodificacion: action.dataObra.dtfechamodificacion
          }
        } else return item;
      })
    case 'CLEAN_OBRA':
      return stateObras = dataVacio
    default:
      return stateObras;
  }
}
/**
 * Maestro de Estados Obras
 */
const dataEstadosObras = (stateEstadoObra = [], action) => {
  switch (action.type) {
    case 'ADD_ESTADOOBRA':
      return stateEstadoObra.concat([action.dataEstadoObra])
    case 'DELETE_ESTADOOBRA':
      return stateEstadoObra.filter((item) => item.id !== action.id)
    case 'UPDATE_ESTADOOBRA':
      return stateEstadoObra.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            value: action.dataEstadoObra.value,
            label: action.dataEstadoObra.label,
            dtestppae: action.dataEstadoObra.dtestppae,
            dtestppe: action.dataEstadoObra.dtestppe,
            //dtcreadopor: action.dataEstadoObra.dtcreadopor,
            //dtfechacreacion: action.dataEstadoObra.dtfechacreacion,
            dtmodificadopor: action.dataEstadoObra.dtmodificadopor,
            dtfechamodificacion: action.dataEstadoObra.dtfechamodificacion
          }
        } else return item;
      })
    case 'CLEAN_ESTADOOBRA':
      return stateEstadoObra = dataVacio
    default:
      return stateEstadoObra;
  }
}
/**
 * Maestro de Obras Estado
 */
// const dataObrasEstados = (stateObrasEstados = [], action) => {
//   switch (action.type) {
//     case 'ADD_OBRAESTADO':
//       return stateObrasEstados.concat([action.dataObraEstado])
//     case 'DELETE_OBRAESTADO':
//       return stateObrasEstados.filter((item) => item.id !== action.id)
//     case 'UPDATE_OBRAESTADO':
//       return stateObrasEstados.map((item) => {
//         if (item.id === action.id) {
//           return {
//             ...item,
//             dtnombreobra: action.dataObraEstado.dtnombreobra,
//             dtcodobra: action.dataObraEstado.dtcodobra,
//             dtcorreoobra: action.dataObraEstado.dtclaveobra,
//             dtestadoobra: action.dataObraEstado.dtestadoobra,
//             //dtcreadopor: action.dataObraEstado.dtcreadopor,
//             //dtfechacreacion: action.dataObraEstado.dtfechacreacion,
//             dtmodificadopor: action.dataObraEstado.dtmodificadopor,
//             dtfechamodificacion: action.dataObraEstado.dtfechamodificacion
//           }
//         } else return item;
//       })
//     case 'CLEAN_OBRAESTADO':
//       return stateObrasEstados = dataVacio
//     default:
//       return stateObrasEstados;
//   }
// }
/**
 * Maestro de Contratista
 */
const dataContratistas = (stateContratista = [], action) => {
  switch (action.type) {
    case 'ADD_CONTRATISTA':
      return stateContratista.concat([action.dataContratista])
    case 'DELETE_CONTRATISTA':
      return stateContratista.filter((item) => item.id !== action.id)
    case 'UPDATE_CONTRATISTA':
      return stateContratista.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            dtnombrecontratista: action.dataContratista.dtnombrecontratista,
            dtnitcontratista: action.dataContratista.dtnitcontratista,
            dtestadocontratista: action.dataContratista.dtestadocontratista,
            //dtcreadopor: action.dataContratista.dtcreadopor,
            //dtfechacreacion: action.dataContratista.dtfechacreacion,
            dtmodificadopor: action.dataContratista.dtmodificadopor,
            dtfechamodificacion: action.dataContratista.dtfechamodificacion
          }
        } else return item;
      })
    case 'CLEAN_CONTRATISTA':
      return stateContratista = dataVacio
    default:
      return stateContratista;
  }
}

/**
 * Maestro de Clasificaciones
 */
const dataClasificaciones = (stateClasificacion = [], action) => {
  switch (action.type) {
    case 'ADD_CLASIFICACION':
      return stateClasificacion.concat([action.dataClasificacion])
    case 'DELETE_CLASIFICACION':
      return stateClasificacion.filter((item) => item.id !== action.id)
    case 'UPDATE_CLASIFICACION':
      return stateClasificacion.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            dtclasificacion: action.dataClasificacion.dtclasificacion,
            dtestadoclasificacion: action.dataClasificacion.dtestadoclasificacion,
            //dtcreadopor: action.dataClasificacion.dtcreadopor,
            //dtfechacreacion: action.dataClasificacion.dtfechacreacion,
            dtmodificadopor: action.dataClasificacion.dtmodificadopor,
            dtfechamodificacion: action.dataClasificacion.dtfechamodificacion
          }
        } else return item;
      })
    case 'CLEAN_CLASIFICACION':
      return stateClasificacion = dataVacio
    default:
      return stateClasificacion;
  }
}
/**
 * Maestro de Capitulos
 */
const dataCapitulos = (stateCapitulos = [], action) => {
  switch (action.type) {
    case 'ADD_CAPITULO':
      return stateCapitulos.concat([action.dataCapitulo])
    case 'DELETE_CAPITULO':
      return stateCapitulos.filter((item) => item.id !== action.id)
    case 'UPDATE_CAPITULO':
      return stateCapitulos.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            dtcapitulo: action.dataCapitulo.dtcapitulo,
            dtcodcapitulo: action.dataCapitulo.dtcodcapitulo,
            dtfkidclasificacion: action.dataCapitulo.dtfkidclasificacion,
            dtnombclasificacion: action.dataCapitulo.dtnombclasificacion,
            dtestadoclasificacion: action.dataCapitulo.dtestadoclasificacion,
            //dtcreadopor: action.dataCapitulo.dtcreadopor,
            //dtfechacreacion: action.dataCapitulo.dtfechacreacion,
            dtmodificadopor: action.dataCapitulo.dtmodificadopor,
            dtfechamodificacion: action.dataCapitulo.dtfechamodificacion
          }
        } else return item;
      })
    case 'CLEAN_CAPITULO':
      return stateCapitulos = dataVacio
    default:
      return stateCapitulos;
  }
}
/**
 * Maestro de SubCapitulos
 */
const dataSubCapitulos = (stateSubCapitulos = [], action) => {
  switch (action.type) {
    case 'ADD_SUBCAPITULO':
      return stateSubCapitulos.concat([action.dataSubCapitulo])
    case 'DELETE_SUBCAPITULO':
      return stateSubCapitulos.filter((item) => item.id !== action.id)
    case 'UPDATE_SUBCAPITULO':
      return stateSubCapitulos.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            dtsubcapitulo: action.dataSubCapitulo.dtsubcapitulo,
            dtcodsubcapitulo: action.dataSubCapitulo.dtcodsubcapitulo,
            dtfkidcapitulo: action.dataSubCapitulo.dtfkidcapitulo,
            dtnombrecapitulo: action.dataSubCapitulo.dtnombrecapitulo,
            dtestadosubcapitulo: action.dataSubCapitulo.dtestadosubcapitulo,
            //dtcreadopor: action.dataSubCapitulo.dtcreadopor,
            //dtfechacreacion: action.dataSubCapitulo.dtfechacreacion,
            dtmodificadopor: action.dataSubCapitulo.dtmodificadopor,
            dtfechamodificacion: action.dataSubCapitulo.dtfechamodificacion
          }
        } else return item;
      })
    case 'CLEAN_SUBCAPITULO':
      return stateSubCapitulos = dataVacio
    default:
      return stateSubCapitulos;
  }
}
/**
 * Maestro de Centro de Costos
 */
const dataCentroCostos = (stateCentroCostos = [], action) => {
  switch (action.type) {
    case 'ADD_CENTROCOSTO':
      return stateCentroCostos.concat([action.dataCentroCosto])
    case 'DELETE_CENTROCOSTO':
      return stateCentroCostos.filter((item) => item.id !== action.id)
    case 'UPDATE_CENTROCOSTO':
      return stateCentroCostos.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            NombreCentroCosto: action.dataCentroCosto.NombreCentroCosto,
            FkSubCapitulo: action.dataCentroCosto.FkSubCapitulo,
            NombreSubCapitulo: action.dataCentroCosto.NombreSubCapitulo,
            CodCentroCosto: action.dataCentroCosto.CodCentroCosto,
            Estado: action.dataCentroCosto.Estado,
            Cargue: action.dataCentroCosto.Cargue,
            UsuarioModificador: action.dataCentroCosto.UsuarioModificador,
            FechaModificacion: action.dataCentroCosto.FechaModificacion
          }
        } else return item;
      })
    case 'CLEAN_CENTROCOSTO':
      return stateCentroCostos = dataVacio
    default:
      return stateCentroCostos;
  }
}
/**
 * Maestro de Isumo
 */
const dataIsumo = (stateInsumo = [], action) => {
  switch (action.type) {
    case 'ADD_INSUMO':
      return stateInsumo.concat([action.dataInsumo])
    case 'DELETE_INSUMO':
      return stateInsumo.filter((item) => item.id !== action.id)
    case 'UPDATE_INSUMO':
      return stateInsumo.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            NombreInsumo: action.dataInsumo.NombreInsumo,
            UnidadMedidaInsumo: action.dataInsumo.UnidadMedidaInsumo,
            FkEstado: action.dataInsumo.FkEstado,
            Estado: action.dataInsumo.Estado,
            Cargue: action.dataInsumo.Cargue,
            UsuarioModificador: action.dataInsumo.UsuarioModificador,
            FechaModificacion: action.dataInsumo.FechaModificacion,
            FkCentroCosto: action.dataInsumo.FkCentroCosto,
            NombreCentroCosto: action.dataInsumo.NombreCentroCosto,
          }
        } else return item;
      })
    case 'CLEAN_INSUMO':
      return stateInsumo = dataVacio
    default:
      return stateInsumo;
  }
}
/**
* Maestro de Encargados Contratistas
*/
const dataEncargadosContratistas = (stateEnContratista = [], action) => {
  switch (action.type) {
    case 'ADD_ENCARGADOCONTRATISTA':
      return stateEnContratista.concat([action.dataEContratista])
    case 'DELETE_ENCARGADOCONTRATISTA':
      return stateEnContratista.filter((item) => item.id !== action.id)
    case 'UPDATE_ENCARGADOCONTRATISTA':
      return stateEnContratista.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            Cedula: action.dataEContratista.Cedula,
            Nombre: action.dataEContratista.Nombre,
            FkContratista: action.dataEContratista.FkContratista,
            NombreContratista: action.dataEContratista.NombreContratista,
            Estado: action.dataEContratista.Estado,
            UsuarioModificador: action.dataEContratista.UsuarioModificador,
            FechaModificacion: action.dataEContratista.FechaModificacion
          }
        } else return item;
      })
    case 'CLEAN_ENCARGADOCONTRATISTA':
      return stateEnContratista = dataVacio
    default:
      return stateEnContratista;
  }
}

/**
 * Maestro de Insumos Asociados
 */
const dataInsumosAsociados = (stateInsumoAsociado = [], action) => {
  switch (action.type) {
    case 'ADD_INSUMOASOCIADO':
      return stateInsumoAsociado.concat([action.dataIAsociado])
    case 'DELETE_INSUMOASOCIADO':
      return stateInsumoAsociado.filter((item) => item.id !== action.id)
    case 'UPDATE_INSUMOASOCIADO':
      return stateInsumoAsociado.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            FkObra: action.dataIAsociado.FkObra,
            NombreObra: action.dataIAsociado.NombreObra,
            FkCentroCosto: action.dataIAsociado.FkCentroCosto,
            NombreCentroCosto: action.dataIAsociado.NombreCentroCosto,
            FkInsumo: action.dataIAsociado.FkInsumo,
            NombreInsumo: action.dataIAsociado.NombreInsumo,
            FkEstado: action.dataIAsociado.FkEstado,
            Estado: action.dataIAsociado.Estado,
            Cargue: action.dataIAsociado.Cargue,
            UsuarioModificador: action.dataIAsociado.UsuarioModificador,
            FechaModificacion: action.dataIAsociado.FechaModificacion
          }
        } else return item;
      })
    case 'CLEAN_INSUMOASOCIADO':
      return stateInsumoAsociado = dataVacio
    default:
      return stateInsumoAsociado;
  }
}
/**
 * FIN MAESTROS
 */


/**
 * Ingreso de Observaciones por numero de solicitud
 */
const dataObservaciones = (stateObservacion = [], action) => {
  switch (action.type) {
    case 'ADD_OBSERVACION':
      return stateObservacion.concat([action.dataListObserva])
    case 'CLEAN_OBSERVACION':
      return stateObservacion = dataVacio
    default:
      return stateObservacion;
  }
}
/**
 * Estado de solicitud
 */
const dataEstadoSolicitud = (stateEstadoSolicitud = [], action) => {
  switch (action.type) {
    case 'ADD_ESTADOSOLICITUD':
      return stateEstadoSolicitud.concat([action.dataListSoliEstado])
    case 'CLEAN_ESTADOSOLICITUD':
      return stateEstadoSolicitud = dataVacio
    default:
      return stateEstadoSolicitud;
  }
}
/**
 * Estado
 */
const dataEstado = (stateEstado = [], action) => {
  switch (action.type) {
    case 'ADD_ESTADO':
      return stateEstado.concat([action.dataEstado])
    case 'CLEAN_ESTADO':
      return stateEstado = dataVacio
    default:
      return stateEstado;
  }
}
/**
 * Ingreso de Firma
 */
const dataFirma = (stateFirma = [], action) => {
  switch (action.type) {
    case 'ADD_FIRMA':
      return stateFirma.concat([action.dataListFirma])
    case 'CLEAN_FIRMA':
      return stateFirma = dataVacio
    default:
      return stateFirma;
  }
}

/**
 * Modulo de Soliciud Material
 */
const dataSoliciudItems = (state = [], action) => {
  switch (action.type) {
    case 'ADD_ITEMS':
      return state.concat([action.dataSend])
    case 'DELETE_ITEMS':
      return state.filter((item) => item.id !== action.id)
    case 'UPDATE_ITEMS':
      return state.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            NumeroSolicitud: action.dataSend.NumeroSolicitud,
            DescripcionClasificacion: action.dataSend.DescripcionClasificacion,
            NombreCentroCosto: action.dataSend.NombreCentroCosto,
            NombreInsumo: action.dataSend.NombreInsumo,
            Unidad: action.dataSend.Unidad,
            Cantidad: action.dataSend.Cantidad
          }
        } else return item;
      })
    case 'CLEAN_ITEMS':
      return state = dataVacio
    default:
      return state;
  }
}

const dataSolicitudList = (stateList = [], action) => {
  switch (action.type) {
    case 'ADD_LIST':
      return stateList.concat([action.dataSendList])
    case 'DELETE_ITEMSLIST':
      return stateList.filter((item) => item.id !== action.id)
    case 'UPDATE_ITEMSLIST':
      return stateList.map((item) => {
        if (item.id === action.fkid) {
          item.DataListItems.map((iditem) => {
            if (iditem.id === action.id) {
              return {
                ...iditem,
                // formnro: action.iditem.formnro,
                DescripcionClasificacion: action.dataSendListItems.DescripcionClasificacion,
                NombreCentroCosto: action.dataSendListItems.NombreCentroCosto,
                NombreInsumo: action.dataSendListItems.NombreInsumo,
                Cantidad: action.dataSendListItems.Cantidad,
                Unidad: action.dataSendListItems.Unidad,
              }
            } else return iditem
          })
        }
        return item;
      })
    case 'UPDATE_ITEMSLISTCANTENTREGADA':
      return stateList.map((item) => {
        if (item.id === action.id) {
          item.DataListItems.map((iditem) => {
            if (iditem.id === action.id) {
              return {
                ...iditem,
                CantidadEntregada: action.dataSendListItems.CantidadEntregada,
              }
            } else return iditem
          })
        }
        return item;
      })
    case 'UPDATE_LISTOITEMSOBRA':
      return stateList.map((item) => {
        if (item.id === action.fkid) {
          item.DataListItems.map((iditem) => {
            if (iditem.fkidlist === action.id) {
              return {
                ...iditem,
                DescripcionClasificacion: action.dataSendListItems.DescripcionClasificacion,
                NombreCentroCosto: action.dataSendListItems.NombreCentroCosto,
              }
            } else return iditem
          })
        }
        return item;
      })
    case 'UPDATE_LISTOITEMCLASI':
      return stateList.map((item) => {
        if (item.id === action.fkid) {
          item.DataListItems.map((iditem) => {
            if (iditem.id === action.id) {
              return {
                ...iditem,
                NombreCentroCosto: action.dataSendListItems.NombreCentroCosto,
              }
            } else return iditem
          })
        }
        return item;
      })
    case 'UPDATE_ESTADOLIST':
      return stateList.map((item) => {
        if (item.id === action.id) {
          return {
            ...item,
            EstadoSolicitud: action.dataSendList.EstadoSolicitud,
          }
        } else return item;
      })
    case 'CLEAN_LIST':
      return stateList = dataVacio

    default:
      return stateList;
  }
}
/**
 * Modulo de Acceso a Obras
 */
const dataAccesoObra = (stateAccObra = [], action) => {
  switch (action.type) {
    case 'ADD_ACCEOBRA':
      return stateAccObra.concat([action.dataAccObra])
    case 'DELETE_ACCEOBRA':
      return stateAccObra.filter((item) => item.id !== action.id)
    case 'CLEAN_ACCEOBRA':
      return stateAccObra = dataVacio
    default:
      return stateAccObra;
  }
}
/**
 * Datos usuario logueado
 */
const dataLoginUser = (stateLogin = [], action) => {
  switch (action.type) {
    case 'ADD_LOGIN':
      return stateLogin.concat([action.dataLogin])
    case 'CLEAN_LOGIN':
      return stateLogin = dataVacio
    default:
      return stateLogin;
  }
}
/**
 * LISTADOS
 */
const dataListaObras = (stateListaObras = [], action) => {
  switch (action.type) {
    case 'ADD_LISTAOBRA':
      return stateListaObras.concat([action.dataListaObra])
    case 'CLEAN_LISTAOBRA':
      return stateListaObras = dataVacio
    default:
      return stateListaObras;
  }
}
const dataListaContratistas = (stateListaContratistas = [], action) => {
  switch (action.type) {
    case 'ADD_LISTACONTRATISTA':
      return stateListaContratistas.concat([action.dataListaContratista])
    case 'CLEAN_LISTACONTRATISTA':
      return stateListaContratistas = dataVacio
    default:
      return stateListaContratistas;
  }
}
const dataListaClasificaciones = (stateListaClasificaciones = [], action) => {
  switch (action.type) {
    case 'ADD_LISTACLASIFICACION':
      return stateListaClasificaciones.concat([action.dataListaClasificacion])
    case 'CLEAN_LISTACLASIFICACION':
      return stateListaClasificaciones = dataVacio
    default:
      return stateListaClasificaciones;
  }
}
const dataListaInsumos = (stateListaInsumos = [], action) => {
  switch (action.type) {
    case 'ADD_LISTAINSUMO':
      return stateListaInsumos.concat([action.dataListaInsumo])
    case 'CLEAN_LISTAINSUMO':
      return stateListaInsumos = dataVacio
    default:
      return stateListaInsumos;
  }
}
const dataListaCentroCostos = (stateListaCentroCostos = [], action) => {
  switch (action.type) {
    case 'ADD_LISTACENTROCOSTO':
      return stateListaCentroCostos.concat([action.dataListaCentroCosto])
    case 'CLEAN_LISTACENTROCOSTO':
      return stateListaCentroCostos = dataVacio
    default:
      return stateListaCentroCostos;
  }
}
const dataListaCapitulos = (stateListaCapitulos = [], action) => {
  switch (action.type) {
    case 'ADD_LISTACAPITULOS':
      return stateListaCapitulos.concat([action.dataListaCapitulo])
    case 'CLEAN_LISTACAPITULOS':
      return stateListaCapitulos = dataVacio
    default:
      return stateListaCapitulos;
  }
}
const dataListaSubcapitulos = (stateListaSubcapitulos = [], action) => {
  switch (action.type) {
    case 'ADD_LISTASUBCAPITULOS':
      return stateListaSubcapitulos.concat([action.dataListaSubcapitulo])
    case 'CLEAN_LISTASUBCAPITULOS':
      return stateListaSubcapitulos = dataVacio
    default:
      return stateListaSubcapitulos;
  }
}
const dataListaRelacionCIO = (stateListaRelacionCIO = [], action) => {
  switch (action.type) {
    case 'ADD_LISTARELACIONCIO':
      return stateListaRelacionCIO.concat([action.dataListaRCIO])
    case 'CLEAN_LISTARELACIONCIO':
      return stateListaRelacionCIO = dataVacio
    default:
      return stateListaRelacionCIO;
  }
}
const dataListaUsuarios = (stateListaUsuario = [], action) => {
  switch (action.type) {
    case 'ADD_LISTAUSUARIO':
      return stateListaUsuario.concat([action.dataListaUsuario])
    case 'CLEAN_LISTAUSUARIO':
      return stateListaUsuario = dataVacio
    default:
      return stateListaUsuario;
  }
}



const postReducer = combineReducers({
  AccObra: dataAccesoObra,
  Contratista: dataContratistas,
  Clasificaciones: dataClasificaciones,
  Capitulos: dataCapitulos,
  CentroCostos: dataCentroCostos,
  EncargadoContratista: dataEncargadosContratistas,
  Estado: dataEstado,
  EstadoObra: dataEstadosObras,
  EstadoSolicitud: dataEstadoSolicitud,
  Firma: dataFirma,
  Items: dataSoliciudItems,
  Insumo: dataIsumo,
  InsumosAsociados: dataInsumosAsociados,
  List: dataSolicitudList,
  Login: dataLoginUser,
  ListaObra: dataListaObras,
  ListaCapitulo: dataListaCapitulos,
  ListaContratista: dataListaContratistas,
  ListaClasificaciones: dataListaClasificaciones,
  ListaInsumos: dataListaInsumos,
  ListaCentroCostos: dataListaCentroCostos,
  ListaSubcapitulos: dataListaSubcapitulos,
  ListaRelacionCIO: dataListaRelacionCIO,
  ListaUsuarios: dataListaUsuarios,
  Observacion: dataObservaciones,
  Obra: dataObras,
  SubCapitulos: dataSubCapitulos,
  User: dataUsuarios,
})
export default postReducer;