import React from 'react';
import { DataUsuarioLogueo, DataModulosUsuario, RutaApi } from '../../const/Index';
import { connect } from 'react-redux';
import Moment from 'moment';
import axios from "axios";

class FormLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dt: new Date(),
            user: '',
            pass: '',
            msg: '',
            cont: 1,
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInput(e) {
        // este e.target me trae lo que tenga el campo,
        const { value, name } = e.target;
        //Este evento sera el encargado de poder cambiar el estado de los campos de this.state
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault();//Esto es para no recargarla pagina
        const usr = this.state.user;
        const ps = this.state.pass;
        this.getData(usr, ps);

    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async (usr, ps) => {
        this.setState({ msg: 'Validado, por favor espere...' });
        
        let resUser = await axios.get(RutaApi + 'Usuarios');
        const dataLogin = {
            usr: usr,
            pwd: ps,
        };

        let res = await axios.post(RutaApi + 'Login', JSON.stringify(dataLogin));
        if (res.data === true) {//Se debe validar un si es false o true
            this.setState({ msg: '' });
            this.props.dispatch({
                type: 'CLEAN_LOGIN'
            });
            let isLogind = false;
            
            var md5 = require('md5');
            let pass = md5(ps);
            // let dtLogin = DataUsuarioLogueo.filter(dt => usr === dt.user && ps === dt.pass);
            let dtLogin = resUser.data.filter(dt => usr === dt.Nombre && pass === dt.Contrasenia);
            if (dtLogin.length > 0) {
            //     let dtid = DataUsuarioLogueo.filter(dt => usr === dt.user && ps === dt.pass).map(crud => crud.IdUsuario);
                let dtid = resUser.data.filter(dt => usr === dt.Nombre && pass === dt.Contrasenia).map(crud => crud.IdUsuario);
                isLogind = true;
                let dtLogin2 = DataUsuarioLogueo.filter(dt => usr === dt.user && ps === dt.pass);
                dtLogin2.map(user => {
                    this.setState({ msg: '', user: '', pass: '', cont: this.state.cont + 1 });
                    // const cont = this.state.cont;
                    // Para agregar al store los permisos del usuario
                    let dtc = DataModulosUsuario.filter(dtP => user.rol === dtP.rol).map(crud => crud.c);
                    let dtr = DataModulosUsuario.filter(dtP => user.rol === dtP.rol).map(crud => crud.r);
                    let dtu = DataModulosUsuario.filter(dtP => user.rol === dtP.rol).map(crud => crud.u);
                    let dtd = DataModulosUsuario.filter(dtP => user.rol === dtP.rol).map(crud => crud.d);
                    const dataLogin = {
                        id: dtid[0],
                        dtuser: user.user,
                        dtuserid: dtid[0],
                        dtc: dtc[0],
                        dtr: dtr[0],
                        dtu: dtu[0],
                        dtd: dtd[0],
                        dtrol: user.rol,
                        dtfechalogin: Moment(new Date()).format("YYYY-MM-DD hh:mm"),//Para saber a la fecha en que se logueo
                    }
                    this.props.dispatch({
                        type: 'ADD_LOGIN',
                        dataLogin
                    });
                    return isLogind;
                });
                this.props.onSignIn(isLogind);
            } else {
                this.setState({ msg: 'El usuario y la contraseña no coinciden, por favor valide nuevamente' });
            }

        }else{
            this.setState({ msg: 'El usuario y la contraseña no coinciden, por favor valide nuevamente',  user:"",  pass: ""}); 
        }

        // setTimeout(() => { this.onChangeLoading(); }, 4000);
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="content-box">
                <div className="panel-heading">
                    <h3 className="panel-title">Ingrese sus datos</h3>
                </div>
                <div className="panel-body">
                    <fieldset>
                        <div className="form-group">
                            <div className="input-group">
                                <input type="text" name="user" className="form-control" placeholder="Usuario" value={this.state.user} onChange={this.handleInput} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="input-group">
                                <input type="password" name="pass" className="form-control" placeholder="Contraseña" value={this.state.pass} onChange={this.handleInput} required />
                            </div>
                        </div>
                        <label className="msg-login">{this.state.msg}</label>
                        <input type="submit" className="btn btn-lg btn-success btn-block" value="Iniciar sesión" />
                    </fieldset>
                </div>
            </form>
        );
    }

}
const mapStateToProps = (state) => {
    return {
        login: state.Login
    }
}

export default connect(mapStateToProps)(FormLogin);