import React from 'react';
import FormLogin from './FormLogin';
//import SelectOption from './SelectOption';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      seleTipo: true
    }
    this.handleSignIn = this.handleSignIn.bind(this);
  }
  handleSignIn(data) {
    //console.log("Aqui en Login " + data);
    this.props.onSingnIn(data);
    //e.preventDefault();//Esto es para no recargarla pagina
  }
  
  render() {
    return (
      <div className="bgLogin">
        <div className="container">
          <div className="row">
            <div className="col-md-2 col-sm-2 col-xs-12"></div>
            <div className="col-md-7 col-sm-7 col-xs-12">
              <div className="campLogin">
                {/* <SelectOption></SelectOption> */}
                <FormLogin onSignIn={this.handleSignIn}></FormLogin>
              </div>
            </div>
          </div>
        </div>
      </div>
    )

  }
}


export default Login;
