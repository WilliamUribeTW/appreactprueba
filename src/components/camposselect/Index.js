import React from 'react';
import { Form, Col } from 'react-bootstrap';

class CampoSelect extends React.Component {
    constructor(props) {
        super(props);
        this.handleImnput = this.handleImnput.bind(this);
    }
    handleImnput(e) {
        const send = { name: this.props.name, value: e.target.value }
        //console.log(send)
        this.props.onSelectData(send);
    }
    render() {
        return (
            <Form.Group as={Col} controlId={this.props.controlId}>
                <Form.Label></Form.Label>
                <Form.Control as="select" name={this.props.name} onChange={this.handleImnput}>
                    {
                        this.props.dataselect.map(dt => {
                            return (
                                <option key={dt.id}>{dt.value}</option>
                            )
                        })
                    }
                </Form.Control>
            </Form.Group>
        )
    }
}

export default CampoSelect;