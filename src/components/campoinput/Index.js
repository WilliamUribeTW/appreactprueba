import React from 'react';
import { Form, Col } from 'react-bootstrap';
import Moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";

class CampoInput extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
    }
    handleInput(e) {
        if (e.target.name === "Cantidad") {
            e.target.value = e.target.value.replace(/[,]/g, ".");
        }
        this.props.onResult(e.target);
    }
    render() {
        return (
            <Form.Group as={Col} controlId={this.props.controlId}>
                <Form.Label></Form.Label>
                {this.props.type === "date" && (<Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} min={Moment(new Date()).format("YYYY-MM-DD")} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled} />)}

                {this.props.type === "number" && (<Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled} min="0" step="any"/>)}

                {this.props.type === "text" && (<Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled} />)}

                {this.props.type === "file" && (<Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled} />)}

                {this.props.type === "password" && (<Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled} />)}

                {this.props.type === "email" && (<Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled} />)}

            </Form.Group>
        )
    }
}


export default CampoInput;