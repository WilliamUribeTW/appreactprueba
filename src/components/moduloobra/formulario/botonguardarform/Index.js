import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import './Index.css';
import Moment from 'moment';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import axios from 'axios';
import { RutaApi } from '../../../../const/Index';

class BotonGuardarForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sw: false,
            brrData: false,
            title: "",
            msj: "",
        };
        this.handleCreateData = this.handleCreateData.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    handleCreateData(e) {
        const { NombreObra, CodObra, Estado, FkEstado } = this.props.onDataState;

        if (NombreObra === "" || CodObra === "" || Estado === "") {
            this.setState({ sw: true, brrData: false })
            this.titleMsn = "Advertencia";
            this.textMsn = "Se debe diligenciar el formulario completo.";
        } else {
            //Agregar informacion a store de 
            this.setState({ sw: false, });
            let usr = this.props.user.map(dtuser => dtuser.dtuserid);
            let validcodobra = this.props.obra.filter(c => c.CodObra === CodObra).map(c => c.id);
            if (validcodobra.length > 0) {
                this.titleMsn = "Advertencia";
                this.textMsn = "El código ingresado ya se encuentra registrado.";
                this.setState({ sw: true, brrData: false, });
            } else {
                const dataObra = {
                    CodObra: CodObra,
                    NombreObra: NombreObra,
                    FkEstado: FkEstado,
                    FkUsuarioCreador: usr[0],
                    FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm"),

                }
                /**
                 * Guardar la informacón en la base de datos
                 */
                let result = 0;
                axios.post(RutaApi + 'Obra', JSON.stringify(dataObra))
                    .then(res => {
                        result = res.data;
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                        // this.titleMsn = "Error";
                        // this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                    });

                setTimeout(() => { this.handleShowMsj(result); }, 300);
            }
        }
    }
    handleShowMsj = async (result) => {
        // Validamos la variable para poder mostrar el mensaje
        if (parseInt(result) === parseInt(0)) {
            this.titleMsn = "Advertencia";
            this.textMsn = "Se presento un inconveniente al insertar la información.";
        } else {
            this.titleMsn = "Registro exitoso";
            this.textMsn = "Se registro la información";
            /**
             * Se limpia los datos para que no se dupliquen cada ves que se cargue
             */
            this.props.dispatch({
                type: 'CLEAN_OBRA'
            });
            this.props.dispatch({
                type: 'CLEAN_LISTAOBRA'
            });
            let res = await axios.get(RutaApi + 'Obra');
            //let { data } = res.data;
            for (let index = 0; index < res.data.length; index++) {
                const element = res.data[index];
                const dataObra = {
                    id: element.IdObra,
                    CodObra: element.CodObra,
                    CodObraOld: element.CodObra,
                    NombreObra: element.NombreObra,
                    FkEstado: element.FkEstado,
                    Estado: element.Estado,
                    FkUsuarioCreador: element.FkUsuarioCreador,
                    UsuarioCreador: element.UsuarioCreador,
                    FechaCreacion: element.FechaCreacion,
                    FkUsuarioModificador: element.FkUsuarioModificador,
                    UsuarioModificador: element.UsuarioModificador,
                    FechaModificacion: element.FechaModificacion,
                }
                this.props.dispatch({
                    type: 'ADD_OBRA',
                    dataObra
                });
                /**
                 * Cargar listado desplegable
                 */
                if (parseInt(element.FkEstado) !== parseInt(2)) {
                    const dataListaObra = {
                        value: element.IdObra,
                        label: element.NombreObra,
                        dtestppae: element.PendienteAprobarEntrega,
                        dtestppav: element.PendienteAprobarValidacion,
                    }
                    this.props.dispatch({
                        type: 'ADD_LISTAOBRA',
                        dataListaObra
                    });
                }

            }
        }
        this.setState({ sw: true, show: true, brrData: true, }, function () {
            this.props.onCreate(this.state.brrData);
        });
    }

    render() {
        return (
            <div>
                <br></br>
                <div className="divBoton">
                    <Button variant="primary" type="button" onClick={() => this.props.onCancel()} disabled={this.state.sw}>Volver</Button>
                    <Button variant="primary" type="submit" onClick={this.handleCreateData} disabled={this.state.sw}>Guardar</Button>
                </div>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        obra: state.Obra,
        user: state.Login
    }
}

export default connect(mapStateToProps)(BotonGuardarForm);