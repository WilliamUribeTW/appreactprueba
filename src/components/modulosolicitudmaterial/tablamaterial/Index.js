import React from 'react';
// import {BrowserRouter as Router, Link, Switch, Route} from 'react-router-dom';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { RutaApi } from '../../../const/Index';
//import Aprovacion from '../../../utils/aprobacion/Index';
import Observacion from '../../campoobservacion/Index';
import TablaDatosItemsID from './tablaitemsid/Index';
// import ModEntregaMaterial from '../../moduloentregamaterial/Index';
import Home from '../../inside/Home';
// import { DataMenuExterno } from '../../../const/Index';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

//Constantes para las internas
// const components = {
//     ModEntregaMaterial,
// };
class TablaSolicitud extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expan: false,
            swModel: true,
            muesApr: true,
            mst: true,
            arrList: [],
            inter: Home,
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        // this.createAprobacion = this.createAprobacion.bind(this);
        //this.handleModal = this.handleModal.bind(this);
        this.dtlis = this.props.list.length;
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        return (

            <div>
                {/* <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar item</button> */}
                {parseInt(crud[0].dtc) !== parseInt(0) && <button className="botonCrear" onClick={this.handleGoCreate}>Crear solicitud</button>}

            </div>
        );
    }
    /**
     * Otras funciones
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    handleChangeObser = (dt) => {
        // Validaremos en que va el contador del store
        let cont = 1;
        let contO = this.props.observ.map(dtid => dtid.id);
        //Capturamos el nombre del usuario que esta creando la solicitud
        let usr = this.props.lg.map(dtuser => dtuser.dtuser);
        let idusr = this.props.lg.map(dtuser => dtuser.dtuserid);
        if (contO.length > 0) { cont = contO.length + 1 }
        const dataListObserva = {
            id: cont,
            FkSolicitudMaterial: dt.idOb,
            FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
            FkUsuario: idusr[0],
            Usuario: usr[0],//Usuario logueado
            Observaciones: "Comentario: " + dt.valueOb,
        }
        //Guardar los datos observacion asociada a la solicitud
        axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
            .then(res => {
                if (res.data !== 0) {
                    this.props.dispatch({
                        type: 'ADD_OBSERVACION',
                        dataListObserva
                    })
                }
            })
            .catch(error => {
                console.log("Se presento un error; se detalla a continuación " + error.response);
            });


    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onBeforeSaveCell(row, cellName, cellValue) {
        return true;
    }
    /**
    * Guardar la información de la celda
    */
    onAfterSaveCell = (row, cellName, cellValue) => { }

    isExpandableRow(row) {
        return true;
    }

    expandComponent(row) {
        return (
            <TablaDatosItemsID onDataItems={row.DataListItems} key={row.id} />
        );
    }
    isEditable = (cell, row) => {
        return true;
    }
    /**
     * Carga de datos 
     */

    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = async () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        this.props.dispatch({
            type: 'CLEAN_LIST'
        });

        this.props.dispatch({
            type: 'CLEAN_OBSERVACION'
        });

        this.props.dispatch({
            type: 'CLEAN_ITEMS'
        });

        const accObra = this.props.lg;
        //Cargar la información del usuario logeado para poder sacar que accesos a obras tiene
        //Consultar que obras tiene asociadas
        let resObr = await axios.get(RutaApi + 'AccesoObra?_id=' + accObra[0].dtuserid);
        let dataSendListItem = {}, acobradata = resObr.data.map(a => a.FkObra);
        //Cargar la informacion de las solicitudes y las guarda en el store
        let res = await axios.get(RutaApi + 'SolicitudMaterial');
        for (let index = 0; index < res.data.length; index++) {
            this.arrList = [];
            const element = res.data[index];
            //Consultamos la obra para darnos cuenta si se encuentra inactiva 
            let estobra = this.props.obra.filter(ob => ob.id === element.FkObra).map(ob => ob.FkEstado);
            estobra = estobra[0];
            //Validamos que la obra no este inactiva
            if (parseInt(estobra) !== parseInt(2)) {
                //Recorremos las obras asignadas al usuario logeado
                for (let index = 0; index < acobradata.length; index++) {
                    const elementAccObra = acobradata[index];
                    if (element.FkObra === elementAccObra) {
                        for (let i = 0; i < element.DataListItems.length; i++) {
                            const e = element.DataListItems[i];
                            let cantentega = "";
                            if (element.EstadoSolicitud !== "Pendiente por Aprobar Entrega" && element.EstadoSolicitud !== "Pendiente por Entregar") { cantentega = e.CantidadEntregada }
                            dataSendListItem = {
                                id: e.IdSolicitudMaterialItem,
                                fkidlist: e.FkSolicitudMaterial,
                                DescripcionClasificacion: e.DescripcionClasificacion,
                                NombreCentroCosto: e.NombreCentroCosto,
                                CodCentroCosto: e.CodCentroCosto,
                                NombreInsumo: e.NombreInsumo,
                                CodInsumo: e.CodInsumo,
                                Cantidad: e.Cantidad,
                                // CantidadEntregada: e.CantidadEntregada,
                                CantidadEntregada: cantentega,
                                Unidad: e.Unidad,
                            }
                            // this.setState({ arrList: [...this.state.arrList, dataSendListItem] });
                            this.arrList.push(dataSendListItem);
                        }
                        const dataSendList = {
                            id: element.IdSolicitudMaterial,
                            NumeroSolicitud: element.NumeroSolicitud,
                            FechaCreacion: element.FechaCreacion,
                            NombreObra: element.NombreObra,
                            NombreEncargado: element.NombreEncargado,
                            NombreContratista: element.NombreContratista,
                            Usuario: element.Usuario,//Usuario logueado
                            FkObservacion: element.IdSolicitudMaterial,
                            // DataListItems: this.state.arrList,
                            DataListItems: this.arrList,
                            EstadoSolicitud: element.EstadoSolicitud,
                        }

                        this.props.dispatch({
                            type: 'ADD_LIST',
                            dataSendList
                        });
                    }

                }
            }
        }
        //Cargar la informacion de las observaciones y las guarda en el store
        let resObs = await axios.get(RutaApi + 'Observacion');
        for (let index = 0; index < resObs.data.length; index++) {
            const element = resObs.data[index];
            const dataListObserva = {
                id: element.IdObservacion,
                FkSolicitudMaterial: element.FkSolicitudMaterial,
                FechaCreacion: element.FechaCreacion,
                FkUsuario: element.FkUsuario,
                Usuario: element.Usuario,//Usuario logueado
                Observaciones: element.Observaciones,
            }
            this.props.dispatch({
                type: 'ADD_OBSERVACION',
                dataListObserva
            });
        }
        // axios.get(RutaApi + 'Observacion')
        //     .then(resObs => {

        //     })
        setTimeout(() => { this.onChangeLoading(); }, 300);
    }
    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        // const dataList = this.props.list.filter(p=> p.dtestado !== 'Finalizada Sin Entrega' && p.dtestado !== 'Finalizada' && p.dtestado !== 'Anulada' && p.dtestado !== 'Cancelada');
        const dataList = this.props.list;
        const dataObsvr = this.props.observ;
        const crud = this.props.lg;
        //Para poder generar el comentario
        const ref = React.createRef();
        const createObservationEditor = (onUpdate, props) => (<Observacion onUpdate={onUpdate}  {...props} ref={ref} onChangeObser={this.handleChangeObser} onObser={dataObsvr} onLogin={crud} />)

        //Mostrar los iconos dependiendo del estado
        const dataIcon = (cellValue, row, file) => {
            let ele = null;
            // DataEstado.map(dt => {
            this.props.listestsolicitud.map(dt => {
                if (row.EstadoSolicitud === dt.value) {
                    ele = `<i key=´${row.id}´ class='fa fa-${dt.icon}' aria-hidden='true' style='${dt.style}'></i>`;

                    // ele = `<Link to='${dt.url}'}><i key=´${row.id}´ class='fa fa-${dt.icon}' aria-hidden='true' style='${dt.style}' onClick='${this.onGoToInside()}'></i></Link>`;
                }
                return ele;
            });
            return ele;
        }
        const dataIconObserva = (cell, row, rowIndex) => {
            return `<i key=´${row.id}´ class='fa fa-pencil' aria-hidden='true' style='cursor:pointer'></i>`;
        }
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell,
            nonEditableRows: function () {
                // Se realiza el filtro para saber que fila se debe inhabilitar
                // let dtsw = dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada' || p.dtestado === 'Cancelada').map(p => p.id);
                // if (dtsw.length > 0) { return dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada' || p.dtestado === 'Cancelada').map(p => p.id); }
            }
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton,
            expandBy: 'column',//Esto es para saber que columnas son expandable true o false
        };

        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable
                        data={dataList}
                        bodyStyle={{ overflow: "overflow" }}
                        options={options}
                        pagination={true}
                        cellEdit={cellEditProp}
                        search={true}
                        expandableRow={this.isExpandableRow}
                        expandComponent={this.expandComponent}
                        expandColumnOptions={{ expandColumnVisible: true }}
                        deleteRow
                    >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="NumeroSolicitud" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal", width: "5%" }} thStyle={{ whiteSpace: "normal", width: "5%" }} expandable={false}>Nro</TableHeaderColumn>
                        <TableHeaderColumn dataField="FechaCreacion" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Fecha</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObra" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Obra</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreContratista" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Contratista</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreEncargado" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Encargado</TableHeaderColumn>
                        <TableHeaderColumn dataField="Usuario" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Solicitado por</TableHeaderColumn>
                        <TableHeaderColumn dataField="EstadoSolicitud" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Estado</TableHeaderColumn>
                        <TableHeaderColumn dataField="dticono" dataSort={true} dataFormat={dataIcon} /*customEditor={{ getElement: createAprobacion }}*/ editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' expandable={false} editable={false} width="90">Link</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkObservacion" dataFormat={dataIconObserva} customEditor={{ getElement: createObservationEditor }} expandable={false} editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' editable={this.isEditable} dataSort={true} headerText="Observación" width="90">Obs</TableHeaderColumn>
                    </BootstrapTable>
                }
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        list: state.List,
        observ: state.Observacion,
        lg: state.Login,
        listestsolicitud: state.EstadoSolicitud,
        obra: state.Obra,

    }
}


export default connect(mapStateToProps)(TablaSolicitud);