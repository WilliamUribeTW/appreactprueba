import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';

class TablaDatosItemsID extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataDel: [],
            cantEntre: false,
            dataUpd: {
                DescripcionClasificacion: "",
                NombreCentroCosto: "",
                NombreInsumo: "",
                Cantidad: "",
                Unidad: "",

            }
        };
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => { }

    render() {
        let dataListItems = this.props.onDataItems;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
        };
        return (
            <BootstrapTable data={dataListItems} options={options} pagination={true} cellEdit={cellEditProp}
                search={true} >
                <TableHeaderColumn dataField="id" isKey={true} hidden={true}>Id</TableHeaderColumn>
                <TableHeaderColumn dataField="fkidlist" hidden={true}>FkId</TableHeaderColumn>
                <TableHeaderColumn dataField="DescripcionClasificacion" editable={false}>Clasificación</TableHeaderColumn>
                {/* <TableHeaderColumn dataField="NombreCentroCosto" editable={false}>Centro de Costos</TableHeaderColumn> */}
                <TableHeaderColumn dataField="NombreCentroCosto" editable={false}>Actividad</TableHeaderColumn>
                {/* <TableHeaderColumn dataField="CodCentroCosto" editable={false} headerText="Código Actividad">Cod. Actividad</TableHeaderColumn> */}
                <TableHeaderColumn dataField="NombreInsumo" editable={false}>Insumo</TableHeaderColumn>
                <TableHeaderColumn dataField="CodInsumo" editable={false} headerText="Código Insumo">Cod. Insumo</TableHeaderColumn>
                <TableHeaderColumn dataField="Unidad" editable={false}>Unidad</TableHeaderColumn>
                <TableHeaderColumn dataField="Cantidad" editable={false} >Cantidad Solicitada</TableHeaderColumn>
                <TableHeaderColumn dataField="CantidadEntregada" editable={false} >Cantidad Entregada</TableHeaderColumn>
            </BootstrapTable>
        )
    }
}


export default connect()(TablaDatosItemsID);