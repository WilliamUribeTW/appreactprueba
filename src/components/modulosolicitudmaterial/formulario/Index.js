import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { RutaApi } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import CampoInput from '../../campoinput/Index';
import TablaDatosItems from './botonguardarform/Index';
import FormItem from '../../formitems/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    //Cargar numero solicitud
    let cont = getIdStoreSoli(this.props.list);
    //Cargamos todos los campos de la solicitud en general
    this.state = {
      NumeroSolicitud: cont,
      // formfecha: Moment(),
      // FechaCreacion: new Date(),
      FechaCreacion: "",
      NombreObra: "",
      NombreEncargado: "",
      NombreContratista: "",
      FkObra: "",
      sw: false,
      show: false,
      showItems: false,
      textMsn: "",
      typeMsn: "",
      brrData: false,
      // dtobra: this.props.listobra,
      dtobra: [],
      dtcontratista: this.props.listcontratista,
      dtencargado: [],
      // dtclasificacion: this.props.listclasificaciones,
      dtclasificacion: [],
      dtccosto: this.props.listccosto,
      dtinsumo: this.props.listinsumo,
      dtestadosolicitud: [],
      loading: true,
      swEncabe: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleCleanData = this.handleCleanData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childobra = React.createRef();
    this.childcontratista = React.createRef();
    this.childencargado = React.createRef();
  }


  handleInput = async (e) => {
    const { value, name, id } = e;

    //Colocar la unidad de medida en el campo
    if (name === 'NombreInsumo') {
      // let und = DataInsumo.filter(u => u.label === value).map(u => u.und);
      let und = this.state.dtinsumo.filter(u => u.label === value).map(u => u.und);
      if (und !== "") { this.setState({ formunidad: und }) }
    }

    if (name === 'NombreObra') {

      this.setState({ FkObra: id, dtencargado: [], dtclasificacion: [] });
      this.childcontratista.current.handleClick();
      this.childencargado.current.handleClick();

      //Realizamos la consulta para traer la informacion de la clasificacion asociada a la obra
      let resObraClasi = await axios.get(RutaApi + 'ObraClasificacion?_id=' + id);
      for (let index = 0; index < resObraClasi.data.length; index++) {
          const element = resObraClasi.data[index];
          let dtcl = this.props.listclasificaciones.filter(u => u.value === element.IdClasificacion);
          if (dtcl.length > 0) {
            for (let index = 0; index < dtcl.length; index++) {
              const element = dtcl[index];
              const dataListaClasificacion = {
                value: element.value,
                label: element.label,
              }
              this.setState({ dtclasificacion: [...this.state.dtclasificacion, dataListaClasificacion] });
            }
            
          }
      }
    }
    //Llenar array de encargados
    if (name === 'NombreContratista') {
      if (this.state.FkObra === "") {
        this.setState({ show: true });
        this.titleMsn = "Advertencia";
        this.textMsn = "Se debe seleccionar la obra para poder mostrar los encargados.";
        this.childcontratista.current.handleClick();
      } else {
        this.childencargado.current.handleClick();
        this.setState({ dtencargado: [] });
        // Cargar el listado de Encargado
        axios.get(RutaApi + 'Encargado?_id=' + id)
          .then(res => {
            for (let index = 0; index < res.data.length; index++) {
              const element = res.data[index];
              if (element.FkObra === this.state.FkObra) {
                const encargado = {
                  value: element.IdEncargado,
                  label: element.Nombre,
                  fkcontratista: element.FkContratista,
                }
                this.setState({ dtencargado: [...this.state.dtencargado, encargado] });

              }
            }
          });

      }
    }
    //Este evento sera el encargado de poder cambiar el estado de los campos de this.state
    this.setState({ [name]: value });
  }

  oncreateMSN = (msn) => {
    return <div style={{ color: "red", fontSize: 12 }}>{msn}</div>
  }

  handleChange = () => {
    this.setState({ show: false, showItems: false });
  }
  handleOnSumit(e) {
    const { DescripcionClasificacion, NombreCentroCosto, NombreInsumo, Cantidad, Unidad } = this.state;
    if (DescripcionClasificacion === null || NombreCentroCosto === "" || NombreInsumo === "" || parseFloat(Cantidad) <= parseFloat(0) || Unidad === "") {

    } else {


    }
    //e.preventDefault();
  }

  handleCleanData() {
    let contnext = this.state.NumeroSolicitud + 1;

    this.setState({
      NumeroSolicitud: contnext,
      FechaCreacion: "",
      NombreObra: "",
      NombreEncargado: "",
      NombreContratista: "",
    })
    /**
     * Se llama la función del componente 
     */
    this.childobra.current.handleClick();
    this.childcontratista.current.handleClick();
    this.childencargado.current.handleClick();
  }

  handleDisabledDataLis = (sw) => {
    this.setState({ swEncabe: sw });
  }
  /**
   * Carga de datos 
   */

  UNSAFE_componentWillMount() {
    this.onLoadData();
  }

  onLoadData = async () => {
    const accObra = this.props.lg;
    //Cargar la información del usuario logeado para poder sacar que accesos a obras tiene
    //Consultar que obras tiene asociadas
    let resObr = await axios.get(RutaApi + 'AccesoObra?_id=' + accObra[0].dtuserid);
    let acobradata = resObr.data.map(a => a.FkObra);
    for (let index = 0; index < this.props.listobra.length; index++) {
      const element = this.props.listobra[index];
      for (let index = 0; index < acobradata.length; index++) {
        const elementAccObra = acobradata[index];
        if (elementAccObra === element.value) {
          this.setState({ dtobra: [...this.state.dtobra, element] });
        }
      }
    }
    // Se realiza este setTimeout para dejar que cargue la informacion que trae del API
    setTimeout(() => { this.onChangeLoading(); }, 300);
  }

  onChangeLoading = () => {
    this.setState({ loading: false });
  }
  // setTimeout(this.setState({ loading: false }), 3000);
  render() {
    const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
    return (
      <div>
        {this.state.loading ?
          <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
          :
          <div>
            <Form onSubmit={this.handleOnSumit} ref="form">
              <Form.Row>
                <CampoInput onResult={this.handleInput} value={this.state.NumeroSolicitud} onRequired={true} onDisabled={true} type="number" placeholder="Nro" name="NumeroSolicitud" controlId="controlId"></CampoInput>
                <CampoInput onResult={this.handleInput} value={this.state.FechaCreacion} onRequired={true} onDisabled={false} type="date" placeholder="Fecha Solicitud" name="FechaCreacion" controlId="fechaCreacion"></CampoInput>

              </Form.Row>
              <Form.Row>
                <DataListInput dataselect={this.state.dtobra} onSelectAuto={this.handleInput} onValue={this.state.NombreObra} ref={this.childobra} onDisabled={this.state.swEncabe} onRequired={true} placeholder="Obra" name="NombreObra" list="nombreObra">
                </DataListInput>
                <DataListInput dataselect={this.state.dtcontratista} onSelectAuto={this.handleInput} onValue={this.state.NombreContratista} ref={this.childcontratista} onDisabled={this.state.swEncabe} onRequired={true} placeholder="Contratista" name="NombreContratista">
                </DataListInput>
                <DataListInput dataselect={this.state.dtencargado} onSelectAuto={this.handleInput} onValue={this.state.NombreEncargado} ref={this.childencargado} onDisabled={this.state.swEncabe} onRequired={true} placeholder="Encargado" name="NombreEncargado">
                </DataListInput>

              </Form.Row>
              <hr></hr>
            </Form>
            <FormItem onDataState={this.state} onDisabledDataLis={this.handleDisabledDataLis}></FormItem>
            <TablaDatosItems onClear={this.handleCleanData} onDataState={this.state} onCancel={() => this.props.onCancel()} onDataObra={this.state.dtobra} onDisabledDataLis={this.handleDisabledDataLis}></TablaDatosItems>
            {this.state.show ? <ModalGeneral onShow={this.state.show} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
          </div>
        }
      </div>
    )
  }
}

function getIdStoreSoli(data) {
  let cont = 1, lastdata = 0;
  // let contO = data.map(dtid => dtid.id);
  for (let index = 0; index < data.length; index++) {
    const element = data[index].id;
    lastdata = element;
  }
  if (lastdata > 0) { cont = lastdata + 1 }
  return cont;
}

const mapStateToProps = (state) => {
  return {
    list: state.List,
    items: state.Items,
    observ: state.Observacion,
    listobra: state.ListaObra,
    listcontratista: state.ListaContratista,
    listclasificaciones: state.ListaClasificaciones,
    listinsumo: state.ListaInsumos,
    listccosto: state.ListaCentroCostos,
    listestsolicitud: state.EstadoSolicitud,
    lg: state.Login,
  }
}
export default connect(mapStateToProps)(FormularioGeneral);