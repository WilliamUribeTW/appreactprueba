import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import TablaDatosItems from '../../tableitems/Index';
import './Index.css';
import Moment from 'moment';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import { RutaApi } from '../../../../const/Index';
import axios from "axios";

class TablaDatos extends React.Component {
    constructor(props) {
        super(props);
        this.dtitems = [];
        this.titleMsn = "";
        this.textMsn = "";
        this.state = {
            sw: false,
            show: false,
            brrData: false,
            showItems: false
        };
        this.handleCreateData = this.handleCreateData.bind(this);
        this.handleShow = this.handleShow.bind(this);
    }

    handleShow(e) {
        this.setState({ sw: e, show: e, });
    }

    handleChange = () => {
        this.setState({ show: false, showItems: false });
    }
    handleCreateData(e) {
        let data = this.props.items;
        const { NumeroSolicitud, FechaCreacion, NombreObra, NombreContratista, NombreEncargado, dtcontratista, dtencargado, dtobra, dtclasificacion, dtccosto, dtinsumo, dtestadosolicitud } = this.props.onDataState;
        if (NumeroSolicitud === "" || FechaCreacion === "" || NombreObra === "" || NombreContratista === "" || NombreEncargado === "" || data.length === 0) {
            this.setState({ show: true });
            this.titleMsn = "Advertencia";
            this.textMsn = "Se debe diligenciar el formulario completo.";
        } else {
            /**
             * Guardar la solicitud
             */
            //Capturamos el nombre del usuario que esta creando la solicitud
            let usr = this.props.lg.map(dtuser => dtuser.dtuserid);
            let usrname = this.props.lg.map(dtuser => dtuser.dtuser);
            //Validaremos en que va el contador del store
            let est = "";

            //Busacamos la obra seleccionada, para saber su parametrización y poder llevar el estado en que se encuentra
            let lstObraEst = dtobra.filter(u => u.label === this.props.onDataState.NombreObra).map(u => u.dtestppae);
            if (lstObraEst[0] === "Si") { est = 1 } else { est = 2 }

            let nomEstSoli = dtestadosolicitud.filter(u => u.id === est).map(u => u.value);
            let dtfkobra = dtobra.filter(u => u.label === this.props.onDataState.NombreObra).map(u => u.value);
            let dtfkcontratista = dtcontratista.filter(u => u.label === this.props.onDataState.NombreContratista).map(u => u.value);
            let dtfkencargado = dtencargado.filter(u => u.label === this.props.onDataState.NombreEncargado).map(u => u.value);

            //Sacamos la informacion de los items para poderlos enviar a la bd
            for (let index = 0; index < this.props.items.length; index++) {
                const element = this.props.items[index];
                let idclasificacion = dtclasificacion.filter(u => u.label === element.DescripcionClasificacion).map(u => u.value);
                let idccosto = dtccosto.filter(u => u.label === element.NombreCentroCosto).map(u => u.value);
                let idinsumo = dtinsumo.filter(u => u.label === element.NombreInsumo).map(u => u.value);
                const dataSendListItems = {
                    FkSolicitudMaterial: NumeroSolicitud,
                    FkClasificacion: idclasificacion[0],
                    FkCentroCosto: idccosto[0],
                    FkInsumo: idinsumo[0],
                    Cantidad: element.Cantidad,
                    Unidad: element.Unidad,
                    DescripcionClasificacion: element.DescripcionClasificacion,
                    NombreCentroCosto: element.NombreCentroCosto,
                    NombreInsumo: element.NombreInsumo
                    // fkidlist: NumeroSolicitud
                }
                this.dtitems.push(dataSendListItems);
            }
            const dataSendList = {
                NumeroSolicitud: NumeroSolicitud,
                FechaCreacion: FechaCreacion,
                FkObra: dtfkobra[0],
                NombreObra: this.props.onDataState.NombreObra,
                FkContratista: dtfkcontratista[0],
                NombreContratista: this.props.onDataState.NombreContratista,
                FkEncargado: dtfkencargado[0],
                NombreEncargado: this.props.onDataState.NombreEncargado,
                FkUsuario: usr[0],//Usuario logueado
                Usuario: usrname[0],//Usuario logueado
                FkObservacion: NumeroSolicitud,
                DataListItems: this.dtitems,
                FkEstadoSolicitud: est,
                EstadoSolicitud: nomEstSoli[0]
            }

            // Guardar los datos de la solicitud
            axios.post(RutaApi + 'SolicitudMaterial', JSON.stringify(dataSendList))
                .then(res => {
                    if (res.data !== 0) {
                        this.titleMsn = "Registro exitoso";
                        this.textMsn = "Se registro la información de manera correcta.";
                        this.props.dispatch({
                            type: 'ADD_LIST',
                            dataSendList
                        });
                    } else {
                        this.titleMsn = "Advertencia";
                        this.textMsn = "Se presento un inconveniente al insertar la información.";
                    }
                })
                .catch(error => {
                    // console.log("error.response");
                    this.titleMsn = "Error";
                    this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                });

            //Guardar los datos observacion asociada a la solicitud
            const dataListObserva = {
                FkSolicitudMaterial: NumeroSolicitud,
                FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
                FkUsuario: usr[0],//Usuario logueado
                Observaciones: "Cambio de estado: Solicitada",
            }
            axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
                .then(res => {
                    //console.log(res.data);
                    if (res.data !== 0) {
                        this.titleMsn = "Registro exitoso";
                        this.textMsn = "Se registro la información de manera correcta.";
                        this.props.dispatch({
                            type: 'ADD_OBSERVACION',
                            dataListObserva
                        });
                    } else {
                        this.titleMsn = "Advertencia";
                        this.textMsn = "Se presento un inconveniente al insertar la información.";
                    }
                })
                .catch(error => {
                    //console.log(error.response);
                    this.titleMsn = "Error";
                    this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                });

            this.setState({ show: true, });//sw: false, 
            //Limpiar los datos del los items del formulario
            this.props.dispatch({
                type: 'CLEAN_ITEMS'
            });
            //Esta funcion me sirve para llamar el limpiado de los campos del formulario
            setTimeout(() => { this.props.onClear(); }, 100);
        }
    }
    handleDisabledDataLis = () => {
        this.props.onDisabledDataLis(false);
    }
    render() {
        return (
            <div>
                <br></br>
                <TablaDatosItems onDataItems={this.props.items} onShowModal={this.onShowModal} onDisabledDataLis={this.handleDisabledDataLis}></TablaDatosItems>
                <br></br>
                <div className="divBoton">
                    <Button variant="primary" type="button" onClick={() => this.props.onCancel()} >Volver</Button>
                    <Button variant="primary" type="submit" onClick={this.handleCreateData} >Guardar</Button>
                </div>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle="Advertencia" onBody="Se debe diligenciar el formulario completo." onIf={false} onClose={false} ></ModalGeneral> : null}
                {this.state.show ? <ModalGeneral onShow={this.state.show} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        items: state.Items,
        lg: state.Login,
        list: state.List,
        observ: state.Observacion,
    }
}

export default connect(mapStateToProps)(TablaDatos);