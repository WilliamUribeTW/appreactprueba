import React from 'react';
import Formulario from './formulario/Index';
import TablaSolicitud from './tablamaterial/Index';

class ModMaterial extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      swD: true,
      loading: true,
    }
    this.handleCreateSoli = this.handleCreateSoli.bind(this);
    this.handleCancelSoli = this.handleCancelSoli.bind(this);
  }

  handleCreateSoli() {
    this.setState({ swD: false });
  }
  handleCancelSoli() {
    this.setState({ swD: true });
  }

  onChangeLoading = () => {
    this.setState({ loading: false });
  }
  render() {
    return (
      <div className="container">
        {this.state.swD ? <TablaSolicitud onCreate={this.handleCreateSoli}></TablaSolicitud> : <Formulario onCancel={this.handleCancelSoli}></Formulario>}

      </div>
    )
  }
}

export default ModMaterial;