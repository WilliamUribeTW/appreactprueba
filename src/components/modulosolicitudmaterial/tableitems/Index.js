import React from 'react';
import { connect } from 'react-redux';
import './Index.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
// import { DataInsumo, DataClasSoli, DataCostos } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';

class TablaDatosItems extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataDel: [],
            sw: false,
            dataUpd: {
                DescripcionClasificacion: "",
                NombreCentroCosto: "",
                NombreInsumo: "",
                Cantidad: "",
                Unidad: ""
            }
        };
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }

    onShowModal = () => {
        this.setState({ sw: true });
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_ITEMS',
                id: items[i]
            })
        }
    }
    /**
     * Creacion del botón eliminar
     */
    createCustomDeleteButton = (onBtnClick) => {
        return (
            <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar material(es)</button>
        );
    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        if (cellName === 'NombreInsumo') {
            let und = this.props.onDataState.dtinsumo.filter(u => u.label === cellValue).map(u => u.und);
            // let und = DataInsumo.filter(u => u.label === cellValue).map(u => u.und);
            if (und !== "") { row.Unidad = und }
        }

        // Validar que no coloquen 0
        if (cellName === 'Cantidad') {
            let val = cellValue.replace(/,/g, '.');
            // let val2 = val.replace(/\./g, '');
            // let x = Number(val2);
            row.Cantidad = val;
            if (parseFloat(row.Cantidad) <= parseFloat(0)) {
                this.onShowModal();
                this.setState({ sw: true });
                row.Cantidad = "1";
            } else {
                this.setState({ sw: false });
            }
        }

        if (!this.state.sw) {
            const dataSend = {
                DescripcionClasificacion: row.DescripcionClasificacion,
                NombreCentroCosto: row.NombreCentroCosto,
                NombreInsumo: row.NombreInsumo,
                Unidad: row.Unidad,
                Cantidad: row.Cantidad
            }
            //Editar los datos al Store de react-redux
            this.props.dispatch({
                type: 'UPDATE_ITEMS',
                id: row.id,
                dataSend
            });

        }
    }
    onBeforeSaveCell(row, cellName, cellValue) {
        // You can do any validation on here for editing value,
        // return false for reject the editing
        console.log("onBeforeSaveCell");
        return true;
      }
      
    onLoadData = () => {
        if (this.props.itemsList.length === 0) { this.props.onDisabledDataLis() }//Activar los campos de Obra, contrtista y encargado
    }
    
    render() {
        setTimeout(() => { this.onLoadData(); }, 1000);
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            onBeforeSaveCell: this.onBeforeSaveCell,
            afterSaveCell: this.onAfterSaveCell,
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        const selectRow = {
            mode: 'checkbox',
            onSelect: this.onSelectDelete

        };//
        return (
            <div>
                <BootstrapTable data={this.props.onDataItems} options={options} pagination={true} cellEdit={cellEditProp}
                    search={true} selectRow={selectRow} deleteRow>
                    <TableHeaderColumn dataField="id" isKey={true} hidden={true}># Item</TableHeaderColumn>
                    <TableHeaderColumn dataField="DescripcionClasificacion" editable={{ type: 'select', options: { values: this.props.listclasificaciones.map(dt => { return dt.label }) } }} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Clasificación</TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="NombreCentroCosto" editable={{ type: 'select', options: { values: DataCostos.map(dt => { return dt.label }) } }}>Centro de Costos</TableHeaderColumn> */}
                    <TableHeaderColumn dataField="NombreCentroCosto" editable={{ type: 'select', options: { values: this.props.listccosto.map(dt => { return dt.label }) } }} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Actividad</TableHeaderColumn>
                    <TableHeaderColumn dataField="NombreInsumo" editable={{ type: 'select', options: { values: this.props.listinsumo.map(dt => { return dt.label }) } }} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="Unidad" editable={false}>Unidad</TableHeaderColumn>
                    <TableHeaderColumn dataField="Cantidad" editable={{ type: 'number' }} >Cantidad</TableHeaderColumn>
                </BootstrapTable>
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle="Advertencia" onBody="La cantidad ingresada no puede ser menor o igual a 0(cero)." onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        itemsList: state.Items,
        listclasificaciones: state.ListaClasificaciones,
        listinsumo: state.ListaInsumos,
        listccosto: state.ListaCentroCostos,
    }
}
export default connect(mapStateToProps)(TablaDatosItems);