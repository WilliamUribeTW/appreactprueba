import React from 'react';
import { Form, Col } from 'react-bootstrap';
import DataPicker from 'react-datepicker';
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";


class DataPickers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date()
          };
        this.handleInput = this.handleInput.bind(this);
    }
    handleInput(e) {
        this.setState({
            startDate: e
          });
        //this.props.onResult(e.target);
    }
    render() {
        return (
            <Form.Group as={Col} controlId={this.props.controlId}>
                <Form.Label></Form.Label>
                {/* <Form.Control type={this.props.type} placeholder={this.props.placeholder} name={this.props.name} onChange={this.handleInput} value={this.props.value}
                    required={this.props.onRequired ? "required" : ""} disabled={this.props.onDisabled}/> */}
                <DataPicker selected={this.state.startDate} onChange={this.handleInput}   minDate={moment(new Date('02-01-1970'))}/>
            </Form.Group>
        )
    }
}

export default DataPickers;
