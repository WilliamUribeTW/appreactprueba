import React from 'react';
//import Formulario from './formulario/Index';
import TablaEnviarInventario from './tablaenviarinventario/Index';

class ModAprobarValidacion extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      swD:true
    }
    this.handleCreateSoli = this.handleCreateSoli.bind(this);
    this.handleCancelSoli = this.handleCancelSoli.bind(this);
  }

  handleCreateSoli(){
    this.setState({swD:false});
  }
  handleCancelSoli(){
    this.setState({swD:true});
  }
  render() {
    return (
      <div className="container">
        {/* {this.state.swD ? <TablaEntregaMaterial onCreate={this.handleCreateSoli}></TablaEntregaMaterial>:<Formulario onCancel={this.handleCancelSoli}></Formulario>} */}
        <TablaEnviarInventario onCreate={this.handleCreateSoli}></TablaEnviarInventario>
      </div>
    )
  }
}

export default ModAprobarValidacion;