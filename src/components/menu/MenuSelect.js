import React from 'react';
import { Button, Navbar, Nav, Form, NavDropdown } from 'react-bootstrap';//NavDropdown, FormControl
import { DataModulosUsuario } from '../../const/Index';//DataMenuExterno,
import './Index.css';
import { connect } from 'react-redux';


class MenuSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      valBusca: "",
    };
    this.handleGotMenu = this.handleGotMenu.bind(this);
    this.handleSignOutMenu = this.handleSignOutMenu.bind(this);
    this.handleInputSerach = this.handleInputSerach.bind(this);
  }

  handleSignOutMenu(e) {
    e.preventDefault();//Esto es para no recargarla pagina
    this.props.onSignOut(false);
  }

  handleGotMenu(e) {
    const mn = e.target.parentElement.getAttribute('name');
    const sbmn = e.target.getAttribute('name');
    if (mn !== null) {
      this.props.onGotInter(mn);
    } else {
      this.props.onGotInter(sbmn);
    }
    e.preventDefault();//Esto es para no recargarla pagina
  }

  handleInputSerach(e) {
    this.setState({ valBusca: e.target.value });
  }

  handleSearch(e) {
    //alert("Buscar " + this.state.valBusca);
    this.props.onSearch(this.state.valBusca);
    this.setState({ valBusca: "" });
    e.preventDefault();//Esto es para no recargarla pagina
  }

  render() {
    let dtmod = [];
    let lg = this.props.login.map(dt => { return dt.dtrol });
    DataModulosUsuario.forEach(element => {
      if (element.rol === lg[0]) {
        dtmod = element.mod;
      }
    })
    return (
      <Navbar bg="light" expand="lg">
        <Navbar.Brand>
          <img src={process.env.PUBLIC_URL + '/image/conaltura.png'} style={{ width: 100, marginTop: -7 }} alt="" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            {
              dtmod.map(dataSM => {
                if (dataSM.subM === undefined) {
                  return (<Nav.Link href={dataSM.url} key={dataSM.id} name={dataSM.id} onClick={this.handleGotMenu}>
                    <i className={dataSM.icon}></i>
                    <span>{dataSM.value}</span>
                  </Nav.Link>)
                }else{
                  return (<NavDropdown title={dataSM.value} id="basic-nav-dropdown" key={dataSM.id}>
                    {
                      dataSM.subM.map(dataSM => {
                        return <NavDropdown.Item href={dataSM.url} name={dataSM.id} onClick={this.handleGotMenu} key={dataSM.id}>{dataSM.value}</NavDropdown.Item>
                      })
                    }
                  </NavDropdown>)
                }
              })
            }
          </Nav>
          {/* Se oculta el buscador de la pagina */}
          {/* <Form inline>
            <FormControl type="text" placeholder="Buscar" className="mr-sm-2" onChange={this.handleInputSerach} value={this.state.valBusca} />
            <Button variant="outline-success" className="fa fa-search" onClick={this.handleSearch.bind(this)}></Button>
          </Form> */}
          <Form inline>
            <hr className="vl"></hr>
            <Button variant="outline-success" className="fa fa-user-circle" onClick={this.handleSignOutMenu}></Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.Login
  }
}

export default connect(mapStateToProps)(MenuSelect);