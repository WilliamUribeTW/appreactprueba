import React from 'react';
import MenuSelect from './MenuSelect';
import ListResult from '../listResults/ListResults';
import ModMaterial from '../modulosolicitudmaterial/Index';
import ModUsuario from '../modulousuario/Index';
import ModAccesoObra from '../moduloaccesoobra/Index';
import ModObraEstado from '../moduloobraestado/Index';
import ModEntregaMaterial from '../moduloentregamaterial/Index';
import ModPendienteValidar from '../modulopendientevalidar/Index';
import ModAprobarEntrega from  '../moduloaprobarentrega/Index';
import ModAprobarValidacion from  '../moduloaprobarvalidacion/Index';
import ModEnviarInventario from  '../moduloenviarinventario/Index';
import ModObra from  '../moduloobra/Index';
import ModContratista from  '../modulocontratista/Index';
import ModClasificacion from  '../moduloclasificacion/Index';
import ModCapitulo from  '../modulocapitulo/Index';
import ModSubCapitulo from  '../modulosubcapitulo/Index';
import ModCentroCosto from  '../modulocentrocosto/Index';
import ModInsumo from  '../moduloinsumo/Index';
import ModEstadoSolicitud from  '../moduloestadosolicitud/Index';
import ModEContratista from  '../moduloencargadocontratista/Index';
import ModIAsociado from  '../moduloinsumoasociado/Index';
import { DataHeader, DataMenuExterno } from '../../const/Index';
// import { Home } from '../inside/Index';
import Home from '../inside/Home';

//Constantes para las internas
const components = {
    ModMaterial,
    ModUsuario,
    ModAccesoObra,
    ModObraEstado,
    ModEntregaMaterial,
    ModPendienteValidar,
    ModAprobarEntrega,
    ModAprobarValidacion,
    ModEnviarInventario,
    ModObra,
    ModContratista,
    ModClasificacion,
    ModCapitulo,
    ModSubCapitulo,
    ModCentroCosto,
    ModInsumo,
    ModEstadoSolicitud,
    ModEContratista,
    ModIAsociado,
};
class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inter: Home
        };
        this.handleSignOutMenu = this.handleSignOutMenu.bind(this);
        this.handleGotInter = this.handleGotInter.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }
    handleSignOutMenu(e) {
        this.props.onSignOut(false);
    }
    handleSearch(sch) {
        const result = DataHeader.filter(
            function (item) {
                //Se colocan los datos en mayusculas, para las mayores conincidencias
                const arrDato = item.value.toUpperCase();
                const varBusca = sch.toUpperCase();
                return arrDato.indexOf(varBusca) > -1
            }
        );
        this.setState({ inter: <ListResult result={result} onGotInter={this.handleGotInter}></ListResult> });

    }
    handleGotInter(sw) {
        let slec = null;
        DataMenuExterno.map(name => {
            if (name.id === sw) {
                return slec = components[name.id]// aqui se crea el componente que se va a mostrar
            }
            return slec;
        });
        this.setState({ inter: slec });
    }
    render() {
        return (
            <div>
                <MenuSelect onSignOut={this.handleSignOutMenu} onGotInter={this.handleGotInter} onSearch={this.handleSearch} onRolUser={this.props.onRolUser}></MenuSelect>
                {React.createElement(this.state.inter, {onChangeIntern:"this.handleGotInter"}, null)}
            </div>

        )
    }
}

export default Menu;