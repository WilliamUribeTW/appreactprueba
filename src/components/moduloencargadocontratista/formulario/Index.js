import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { DataEstadoUsuario, DataContratista } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';
import CampoInput from '../../campoinput/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Cedula: "",
      Nombre: "",
      FkContratista: "",
      NombreContratista: "",
      FkEstado: "",
      Estado: "",
      FkObra: "",
      NombreObra: "",
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childestado = React.createRef();
    this.childcontratista = React.createRef();
  }

  handleInput(e) {
    const { value, name, id } = e;
    if (name === "NombreContratista") { this.setState({ FkContratista: id }); }
    if (name === "Estado") { this.setState({ FkEstado: id }); }
    if (name === "NombreObra") { this.setState({ FkObra: id }); }
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {
      this.setState({
        Cedula: "",
        Nombre: "",
        FkContratista: "",
        NombreContratista: "",
        Estado: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: "",
      });

      /**
       * Se llama la función del componente 
       */
      this.childestado.current.handleClick();
      this.childcontratista.current.handleClick();
    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoInput onResult={this.handleInput} value={this.state.Cedula} onRequired={true} onDisabled={false} type="number" placeholder="Cédula" name="Cedula" controlId="Cedula"></CampoInput>

          <CampoInput onResult={this.handleInput} value={this.state.Nombre} onRequired={true} onDisabled={false} type="text" placeholder="Nombre empleado" name="Nombre" controlId="Nombre"></CampoInput>

          <DataListInput dataselect={this.props.lstobra} onSelectAuto={this.handleInput} valueText={this.state.NombreContratista} ref={this.childcontratista} placeholder="Seleccione la obra" name="NombreObra" list="NombreObra" onRequired={true}>
          </DataListInput>

          <DataListInput dataselect={this.props.lstcontratista} onSelectAuto={this.handleInput} valueText={this.state.NombreContratista} ref={this.childcontratista} placeholder="Seleccione el contratista" name="NombreContratista" list="NombreContratista" onRequired={true}>
          </DataListInput>

          <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.Estado} ref={this.childestado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
          </DataListInput>
          
          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
      estado: state.Estado,
      lstcontratista: state.ListaContratista,
      lstobra: state.ListaObra,
  }
}
export default connect(mapStateToProps)(FormularioGeneral);