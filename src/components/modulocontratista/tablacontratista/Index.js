import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { /*DataEstadoUsuario,*/ RutaApi } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaContratista extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sw: false,
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        return (
            <div>
                {/* <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar item</button> */}
                {parseInt(crud[0].dtc) !== parseInt(0) && <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear Contratista</button>}

            </div>
        );
    }
    /**
     * Ir a crear acceso a obra
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_CONTRATISTA',
                id: items[i]
            })
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), idest = 0;
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'Contratista');
        let dt = res.data.filter(f => f.IdContratista === row.id);
        dt = dt[0];

        if (cellName === "Estado") { idest = this.props.estado.filter(e => e.label === cellValue).map(e => e.value); idest = idest[0]; } else { idest = row.FkEstado }

        if (idest === row.FkEstado && dt.NombreContratista === row.NombreContratista && dt.Nit === row.Nit) {
            // console.log("no actualizar");
        } else {
            if (cellName === "Nit") {
                /**
                 * Validar que el Nit no este repetido
                 */
                let nitcontra = res.data.filter(f => f.Nit === row.Nit);
                console.log(nitcontra);
                if (nitcontra.length > 0) {
                    row.Nit = row.NitOld;
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El Nit ingresado ya se encuentra registrado";
                    this.setState({ sw: true, });
                } else {
                    const dataContratista = {
                        IdContratista: row.id,
                        NombreContratista: row.NombreContratista,
                        Nit: row.Nit,
                        FkEstado: idest,
                        FkUsuarioModificador: usr[0],
                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                    }
                    /**
                     * Modificar la informacón en la base de datos
                     */
                    axios.post(RutaApi + 'Contratista', JSON.stringify(dataContratista))
                        .then(res => {
                            // console.log(res.data);
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                        });
                    this.UNSAFE_componentWillMount();
                }
            } else {
                const dataContratista = {
                    IdContratista: row.id,
                    NombreContratista: row.NombreContratista,
                    Nit: row.Nit,
                    FkEstado: idest,
                    FkUsuarioModificador: usr[0],
                    FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                }
                /**
                 * Modificar la informacón en la base de datos
                 */
                axios.post(RutaApi + 'Contratista', JSON.stringify(dataContratista))
                    .then(res => {
                        // console.log(res.data);
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                    });
                this.UNSAFE_componentWillMount();

            }

        }

    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }


    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_CONTRATISTA'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTACONTRATISTA'
        });
        setTimeout(() => { this.getData(); }, 100);
    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'Contratista');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataContratista = {
                id: element.IdContratista,
                NombreContratista: element.NombreContratista,
                Nit: element.Nit,
                NitOld: element.Nit,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }
            this.props.dispatch({
                type: 'ADD_CONTRATISTA',
                dataContratista
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaContratista = {
                    value: element.IdContratista,
                    label: element.NombreContratista,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACONTRATISTA',
                    dataListaContratista
                });
            }
        }
        setTimeout(() => { this.onChangeLoading(); }, 300);
    };

    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataC = this.props.contratista;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton,
        };
        // const selectRow = {
        //     mode: 'checkbox',
        //     onSelect: this.onSelectDelete

        // };//deleteRow selectRow={selectRow}
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataC} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkEstado" hidden={true}># CodEstado</TableHeaderColumn>
                        <TableHeaderColumn dataField="NitOld" hidden={true}># NitOld</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreContratista" dataSort={true} editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Nombre Contratista</TableHeaderColumn>
                        <TableHeaderColumn dataField="Nit" dataSort={true} editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>NIT Contratista</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={true} editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Estado Contratista</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        contratista: state.Contratista,
        lg: state.Login,
        estado: state.Estado,
    }
}


export default connect(mapStateToProps)(TablaContratista);