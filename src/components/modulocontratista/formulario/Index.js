import React from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';
// import { DataEstadoUsuario } from '../../../const/Index';
import DataListInput from '../../autocompletado/Index';
import BotonGuardarForm from './botonguardarform/Index';
import CampoInput from '../../campoinput/Index';

class FormularioGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      NombreContratista: "",
      Nit: "",
      FkEstado: "",
      Estado: "",
      UsuarioCreador: "",
      FechaCreacion: "",
      UsuarioModificador: "",
      FechaModificacion: "",
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleOnSumit = this.handleOnSumit.bind(this);
    this.handleClearData = this.handleClearData.bind(this);

    /**
     * Se crean estas funciones para poder llamar una funcion
     * que se encuentra en el componente
     */
    this.childestado = React.createRef();
  }

  handleInput(e) {
    const { value, name, id} = e;
    if(name === "Estado"){ this.setState({FkEstado: id});}
    this.setState({
      [name]: value
    });
  }

  handleClearData(data) {
    if (data) {
      this.setState({
        NombreContratista: "",
        Nit: "",
        Estado: "",
        UsuarioCreador: "",
        FechaCreacion: "",
        UsuarioModificador: "",
        FechaModificacion: "",
      });

      /**
       * Se llama la función del componente 
       */
      this.childestado.current.handleClick();
    }
  }

  handleOnSumit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleOnSumit} ref="form">
          <CampoInput onResult={this.handleInput} value={this.state.NombreContratista} onRequired={true} onDisabled={false} type="text" placeholder="Nombre Contratista" name="NombreContratista" controlId="NombreContratista"></CampoInput>
          <CampoInput onResult={this.handleInput} value={this.state.Nit} onRequired={true} onDisabled={false} type="text" placeholder="NIT Contratista" name="Nit" controlId="Nit"></CampoInput>
          <DataListInput dataselect={this.props.estado} onSelectAuto={this.handleInput} valueText={this.state.Estado} ref={this.childestado} placeholder="Seleccione el estado" name="Estado" list="Estado" onRequired={true}>
          </DataListInput>
          <BotonGuardarForm onDataState={this.state} onCancel={() => this.props.onCancel()} onCreate={this.handleClearData}></BotonGuardarForm>
        </Form>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
      estado: state.Estado,
  }
}
export default connect(mapStateToProps)(FormularioGeneral);