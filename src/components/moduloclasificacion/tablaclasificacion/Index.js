import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { /*DataEstadoUsuario,*/ RutaApi } from '../../../const/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import Moment from 'moment';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaClasificacion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sw: false,
            dataDel: [],
            loading: true,
            matches: window.matchMedia("(max-width: 1300px)").matches,//Se crea la variable en el estado para mostrar columnas
            wdthCCIN: "15%",
            wdthEICA: "7%",
        }
        this.handleGoCreate = this.handleGoCreate.bind(this);
        this.handleDataDelete = this.handleDataDelete.bind(this);
    }
    handleChange = () => {
        this.setState({ sw: false });
    }
    /**
     * Creacion del botón crear
     */
    createCustomDeleteButton = (onBtnClick) => {
        const crud = this.props.lg;
        return (
            <div>
                {/* <button className="botonEliminar" onClick={this.handleDataDelete}>Eliminar item</button> */}
                {parseInt(crud[0].dtc) !== parseInt(0) && <button className="botonCrear" style={{ marginLeft: '2px' }} onClick={this.handleGoCreate}>Crear Clasificación</button>}
            </div>
        );
    }
    /**
     * Ir a crear acceso a obra
     */
    handleGoCreate() {
        this.props.onCreate();
    }
    /**
     * Eliminar los items seleccionados
     */
    handleDataDelete(e) {
        const items = this.state.dataDel;
        for (var i = 0; i <= items.length; i++) {
            //Eliminar los datos al Store de react-redux
            this.props.dispatch({
                type: 'DELETE_CLASIFICACION',
                id: items[i]
            })
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = async (row, cellName, cellValue) => {
        let usr = this.props.lg.map(dtuser => dtuser.dtuserid), idest = 0;
        /**
         * Realizamos la consulta a la BD nuevamente para validar 
         * si uno de los campos de la tabla que se le muestra al usuario 
         * fue actualizado
         */
        let res = await axios.get(RutaApi + 'Clasificacion');
        let dt = res.data.filter(f => f.IdClasificacion === row.id);
        dt = dt[0];

        if (cellName === "Estado") { idest = this.props.estado.filter(e => e.label === cellValue).map(e => e.value); idest = idest[0]; } else { idest = row.FkEstado }

        if (idest === row.FkEstado && dt.CodClasificacion === row.CodClasificacion && dt.DescripcionClasificacion === row.DescripcionClasificacion) {
            // console.log("no actualizar");
        } else {

            if (cellName === "CodClasificacion") {
                /**
                 * Validar que el codigo no este repetido
                 */
                let codclasi = res.data.filter(f => f.CodClasificacion === row.CodClasificacion);
                if (codclasi.length > 0) {
                    row.CodClasificacion = row.CodClasificacionOld;
                    this.titleMsn = "Advertencia";
                    this.textMsn = "El código ingresado ya se encuentra registrado";
                    this.setState({ sw: true, });

                } else {
                    const dataClasificacion = {
                        IdClasificacion: row.id,
                        CodClasificacion: row.CodClasificacion,
                        DescripcionClasificacion: row.DescripcionClasificacion,
                        FkEstado: idest,
                        FkUsuarioModificador: usr[0],
                        FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                    }
                    /**
                     * Modificar la informacón en la base de datos
                     */
                    axios.post(RutaApi + 'Clasificacion', JSON.stringify(dataClasificacion))
                        .then(res => {
                            // console.log(res.data);
                        })
                        .catch(error => {
                            console.log("Se presento un error; se detalla a continuación " + error.response);
                        });
                    this.onLoadData();
                }
            } else {
                const dataClasificacion = {
                    IdClasificacion: row.id,
                    CodClasificacion: row.CodClasificacion,
                    DescripcionClasificacion: row.DescripcionClasificacion,
                    FkEstado: idest,
                    FkUsuarioModificador: usr[0],
                    FechaModificacion: Moment(new Date()).format("YYYY-MM-DD hh:mm")
                }
                /**
                 * Modificar la informacón en la base de datos
                 */
                axios.post(RutaApi + 'Clasificacion', JSON.stringify(dataClasificacion))
                    .then(res => {
                        // console.log(res.data);
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                    });
                this.onLoadData();
            }

        }
    }
    /**
     * Seleccionar o deseccionar la casilla, 
     * ya sea para agregar o eliminar el item
     * para agregarlo a un array y poder hacer el filto 
     */
    onSelectDelete = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            this.setState({ dataDel: [...this.state.dataDel, row.id] });
        } else {
            const items = this.state.dataDel.filter(item => item !== row.id);
            this.setState({ dataDel: items });
        }
    }

    /**
     * Carga de datos 
     * Se coloca el método componentDidMount() para validar el tamaño de la pantalla
     * Se coloca el método componentWillMount(), ya que es un complemento del metodo anterior
     */
    componentDidMount() {
        this._isMounted = true;
        const handler = e => this.setState({ matches: e.matches });
        window.matchMedia("(max-width: 1300px)").addListener(handler);
        if (window.innerWidth < 1280) {
            this.setState({ wdthCCIN: "25%", wdthEICA: "11%" });
        } else {
            this.setState({ wdthCCIN: "15%", wdthEICA: "7%" });
        }
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = () => {
        if (!this.state.loading) {
            this.setState({ loading: true });
        }
        /**
         * Se limpia los datos para que no se dupliquen cada ves que se cargue
         */
        this.props.dispatch({
            type: 'CLEAN_CLASIFICACION'
        });
        this.props.dispatch({
            type: 'CLEAN_LISTACLASIFICACION'
        });

        setTimeout(() => { this.getData(); }, 100);

    }

    /**
     * Cargamos la información del maestro 
     */
    getData = async () => {
        let res = await axios.get(RutaApi + 'Clasificacion');
        for (let index = 0; index < res.data.length; index++) {
            const element = res.data[index];
            const dataClasificacion = {
                id: element.IdClasificacion,
                CodClasificacion: element.CodClasificacion,
                CodClasificacionOld: element.CodClasificacion,
                DescripcionClasificacion: element.DescripcionClasificacion,
                FkEstado: element.FkEstado,
                Estado: element.Estado,
                FkUsuarioCreador: element.FkUsuarioCreador,
                UsuarioCreador: element.UsuarioCreador,
                FechaCreacion: element.FechaCreacion,
                FkUsuarioModificador: element.FkUsuarioModificador,
                UsuarioModificador: element.UsuarioModificador,
                FechaModificacion: element.FechaModificacion,
            }

            this.props.dispatch({
                type: 'ADD_CLASIFICACION',
                dataClasificacion
            });
            /**
             * Cargar listado desplegable
             */
            if (parseInt(element.FkEstado) !== parseInt(2)) {
                const dataListaClasificacion = {
                    value: element.IdClasificacion,
                    label: element.DescripcionClasificacion,
                }
                this.props.dispatch({
                    type: 'ADD_LISTACLASIFICACION',
                    dataListaClasificacion
                });
            }
        }
        setTimeout(() => { this.onChangeLoading(); }, 300);
    };

    onChangeLoading = () => {
        this.setState({ loading: false });
    }

    render() {
        const dataC = this.props.clasificacion;
        const crud = this.props.lg;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            btnGroup: this.createCustomDeleteButton
        };
        // const selectRow = {
        //     mode: 'checkbox',
        //     onSelect: this.onSelectDelete

        // };//deleteRow selectRow={selectRow}
        const override = css`
        display: block;
        margin: 0 auto;
        border-color: red;`;
        return (
            <div>
                {this.state.loading ?
                    <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                    :
                    <BootstrapTable data={dataC} bodyStyle={{ overflow: "overflow" }} options={options} pagination={true} cellEdit={cellEditProp} search={true} >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodClasificacionOld" hidden={true}># CodClasificacionOld</TableHeaderColumn>
                        <TableHeaderColumn dataField="DescripcionClasificacion" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Clasificación</TableHeaderColumn>
                        <TableHeaderColumn dataField="CodClasificacion" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'text' } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Código Clasificación">Cód. Clasificación</TableHeaderColumn>
                        <TableHeaderColumn dataField="Estado" dataSort={ true } editable={parseInt(crud[0].dtu) !== parseInt(0) ? { type: 'select', options: { values: this.props.estado.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Estado Clasificación</TableHeaderColumn>
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioCreador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Creado por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaCreacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Fecha Creación</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="UsuarioModificador" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Modificado por">Mod. Por</TableHeaderColumn>)}
                        {!this.state.matches && (<TableHeaderColumn dataField="FechaModificacion" dataSort={ true } editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} headerText="Fecha Modificación">Fecha Mod.</TableHeaderColumn>)}
                    </BootstrapTable>
                }
                {this.state.sw ? <ModalGeneral onShow={this.state.sw} onChange={this.handleChange} onTitle={this.titleMsn} onBody={this.textMsn} onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        clasificacion: state.Clasificaciones,
        lg: state.Login,
        estado: state.Estado,
    }
}


export default connect(mapStateToProps)(TablaClasificacion);