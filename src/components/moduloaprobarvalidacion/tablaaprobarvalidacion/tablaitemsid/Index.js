import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import ModalGeneral from '../../../../utils/modalgeneral/Index';
import { RutaApi } from '../../../../const/Index';
import Moment from 'moment';
import axios from "axios";

class TablaDatosItemsID extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            swmodal: false,
            swfirma: false,
            estado: "",
            dataUpd: {
                DescripcionClasificacion: "",
                NombreCentroCosto: "",
                NombreInsumo: "",
                Unidad: "",
                Cantidad: "",
            },
            dataItems: this.props.onDataItems
        };
    }
    handleChange = () => {
        this.setState({ swmodal: false });
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onAfterSaveCell = (row, cellName, cellValue) => {
        if (cellName === "DescripcionClasificacion") {

            if (cellValue !== row.DescripcionClasificacionValidar) {
                this.setState({ swmodal: true });
                /**
                * Capturamos el nombre del usuario que esta creando la solicitud
                */
                let usr = this.props.lg.map(dtuser => dtuser.dtuserid);
                /**
                 * Capurar el id siguiente para las observaciones
                 */
                let contObs = 0, comObser = "";
                let contObser = this.props.observ.map(dtid => dtid.id);
                if (contObser.length > 0) { contObs = contObser.length + 1 } else { contObs = 1 }
                comObser = "Registro de Solicitud: Solicitud modificada en pendiente por validar, campo de clasificación";
                const dataListObserva = {
                    id: contObs,
                    FkSolicitudMaterial: row.fkidlist,
                    FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
                    FkUsuario: usr[0],//Usuario logueado
                    Observaciones: comObser,
                }
                axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
                    .then(res => {
                        //console.log(res.data);
                        if (res.data !== 0) {
                            this.props.dispatch({
                                type: 'ADD_OBSERVACION',
                                dataListObserva
                            });
                        }
                    })
                    .catch(error => {
                        //console.log(error.response);
                        this.titleMsn = "Error";
                        this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                    });
                /**
                 * Recorremos el array de los items de la solicitud
                 */
                /**
                 * Recorremos el array de los items de la solicitud
                 */
                let dataList = this.props.listclasificaciones.filter(p => p.label === cellValue);
                const dataListSoliMateItemAxios = {
                    IdSolicitudMaterialItem: row.id,
                    FkClasificacion: dataList[0].value,
                    FkCentroCosto: 0,
                }
                axios.post(RutaApi + 'SolicitudMaterialItem', JSON.stringify(dataListSoliMateItemAxios))
                    .then(res => {
                        //console.log(res.data);
                        if (res.data !== 0) {
                            row.NombreCentroCosto = undefined;
                            row.CodCentroCosto = undefined;
                            const dataSendListItems = row;
                            /**
                             * Actualizar los campos de Clasificación y C. Costos
                             * Editar los datos al Store de react-redux
                             */
                            this.props.dispatch({
                                type: 'UPDATE_LISTOITEMCLASI',
                                id: row.id,
                                fkid: row.fkidlist,
                                dataSendListItems
                            });
                        }
                    })
                    .catch(error => {
                        console.log("Se presento un error; se detalla a continuación " + error.response);
                    });
            }

        }
        if (cellName === "NombreCentroCosto") {
            /**
             * Recorremos el array de centro de costos
             */
            let dataList = this.props.listccosto.filter(p => p.label === cellValue);
            row.CodCentroCosto = dataList[0].codCC;
            const dataListSoliMateItemAxios = {
                IdSolicitudMaterialItem: row.id,
                FkClasificacion: 0,
                FkCentroCosto: dataList[0].value,
            }
            axios.post(RutaApi + 'SolicitudMaterialItem', JSON.stringify(dataListSoliMateItemAxios))
                .then(res => {
                    //console.log(res.data);
                    if (res.data !== 0) {

                    }
                })
                .catch(error => {
                    console.log("Se presento un error; se detalla a continuación " + error.response);
                });
        }

    }

    render() {
        let dataLg = this.props.lg
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };
        const options = {
            noDataText: 'No hay datos en el momento',
        };
        return (
            <div>
                <BootstrapTable data={this.state.dataItems} options={options} pagination={true} cellEdit={cellEditProp}
                    search={true} >
                    <TableHeaderColumn dataField="id" isKey={true} hidden={true}>Id</TableHeaderColumn>
                    <TableHeaderColumn dataField="fkidlist" hidden={true}>FkId</TableHeaderColumn>
                    <TableHeaderColumn dataField="DescripcionClasificacionValidar" hidden={true}>DescripcionClasificacionValidar</TableHeaderColumn>
                    <TableHeaderColumn dataField="DescripcionClasificacion" editable={parseInt(dataLg[0].dtu) === 1 ? { type: 'select', options: { values: this.props.listclasificaciones.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Clasificación</TableHeaderColumn>
                    <TableHeaderColumn dataField="NombreCentroCosto" editable={parseInt(dataLg[0].dtu) === 1 ? { type: 'select', options: { values: this.props.listccosto.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Actividad</TableHeaderColumn>
                    <TableHeaderColumn dataField="CodCentroCosto" editable={false} headerText="Código Actividad">Cod. Actividad</TableHeaderColumn>
                    <TableHeaderColumn dataField="NombreInsumo" editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }}>Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="CodInsumo" editable={false} headerText="Código Insumo">Codi. Insumo</TableHeaderColumn>
                    <TableHeaderColumn dataField="Unidad" editable={false}>Unidad</TableHeaderColumn>
                    <TableHeaderColumn dataField="Cantidad" editable={false} >Cantidad solicitada</TableHeaderColumn>
                    <TableHeaderColumn dataField="CantidadEntregada" editable={false} >Cantidad entregada</TableHeaderColumn>
                </BootstrapTable>
                {this.state.swmodal ? <ModalGeneral onShow={this.state.swmodal} onChange={this.handleChange} onTitle="Advertencia" onBody="Recuerde que al cambiar la Clasificación, se debe seleccionar el campo de Actividades." onIf={false} onClose={false} ></ModalGeneral> : null}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        lg: state.Login,
        list: state.List,
        observ: state.Observacion,
        listclasificaciones: state.ListaClasificaciones,
        listccosto: state.ListaCentroCostos,
    }
}

export default connect(mapStateToProps)(TablaDatosItemsID);