import React from 'react';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Index.css';
import { RutaApi } from '../../../const/Index';
import TablaDatosItemsID from './tablaitemsid/Index';
import Observacion from '../../campoobservacion/Index';
import Moment from 'moment';
import Aprovacion from '../../../utils/aprobacion/Index';
import ModalGeneral from '../../../utils/modalgeneral/Index';
import axios from "axios";
import { BounceLoader } from 'react-spinners';
import { css } from '@emotion/core';

class TablaAprobarValidacion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expan: false,
            swmodal: false,
            swmodalApro: false,
            loading: true,
        }
        this.dtlis = this.props.list.length;
    }

    handleChange = () => {
        this.setState({ swmodal: false, swmodalApro: false });
    }
    handleChangeObser = (dt) => {
        // Validaremos en que va el contador del store
        let cont = 1;
        let contO = this.props.observ.map(dtid => dtid.id);
        //Capturamos el nombre del usuario que esta creando la solicitud
        let usr = this.props.lg.map(dtuser => dtuser.dtuser);
        let idusr = this.props.lg.map(dtuser => dtuser.dtuserid);
        if (contO.length > 0) { cont = contO.length + 1 }
        const dataListObserva = {
            id: cont,
            FkSolicitudMaterial: dt.idOb,
            FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
            FkUsuario: idusr[0],
            Usuario: usr[0],//Usuario logueado
            Observaciones: "Comentario: " + dt.valueOb,
        }
        //Guardar los datos observacion asociada a la solicitud
        axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
            .then(res => {
                if (res.data !== 0) {
                    this.props.dispatch({
                        type: 'ADD_OBSERVACION',
                        dataListObserva
                    })
                }
            })
            .catch(error => {
                console.log(error.response);
            });
    }
    handleChangeEst = (dt) => {
        console.log(dt);
        let contObs = 0, comObser = "", est = "", sw = false, dtList = this.props.list.filter(p => p.EstadoSolicitud === 'Pendiente por aprobar validación' && p.id === dt.idSoli);
        /**
         * Validamos que los materiales agregados no esten undefined en los campos de 
         * Clasificación y Centro de Costos
         */
        dtList.map((item) => {
            item.DataListItems.map((listItems) => {
                if (listItems.DescripcionClasificacion === undefined || listItems.NombreCentroCosto === undefined) {
                    return sw = true
                }
                return sw
            });
            return sw
        });

        if (sw === true) {
            this.setState({ swmodalApro: true });
        } else {
            /**
             * Capturar el estado 
             * Asignamos la observación
             */
            if (dt.valueEs === 10) {
                est = "Cancelada";
                comObser = "Cambio de estado: " + est;
            } else {
                est = "Pendiente por Enviar al Inventario";
                comObser = "Cambio de estado: " + est;
            }
            /**
             * Capturamos el nombre del usuario que esta creando la solicitud
            */
            let usr = this.props.lg.map(dtuser => dtuser.dtuserid);
            /**
             * Capurar el id siguiente para las observaciones
             */
            let contObser = this.props.observ.map(dtid => dtid.id);
            if (contObser.length > 0) { contObs = contObser.length + 1 } else { contObs = 1 }
            const dataListObserva = {
                id: contObs,
                FkSolicitudMaterial: dt.idSoli,
                FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
                FkUsuario: usr[0],//Usuario logueado
                Observaciones: comObser,
            }
            axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
                .then(res => {
                    //console.log(res.data);
                    if (res.data !== 0) {
                        this.props.dispatch({
                            type: 'ADD_OBSERVACION',
                            dataListObserva
                        });
                    }
                })
                .catch(error => {
                    //console.log(error.response);
                    this.titleMsn = "Error";
                    this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                });
            /**
             * Cambiar el estado de la solicitud
             */
            const dataSendListAxios = {
                IdSolicitudMaterial: dt.idSoli,
                FkObra: 0,
                FkEstadoSolicitud: dt.valueEs,
                DataListItems: []
            }
            axios.post(RutaApi + 'SolicitudMaterial', JSON.stringify(dataSendListAxios))
                .then(res => {
                    if (res.data !== 0) {
                        const dataSendList = {
                            EstadoSolicitud: est
                        }
                        //Editar los datos al Store de react-redux
                        this.props.dispatch({
                            type: 'UPDATE_ESTADOLIST',
                            id: dt.idSoli,
                            dataSendList
                        });
                    }
                })
                .catch(error => {
                    //console.log(error.response);
                    this.titleMsn = "Error";
                    this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                });
        }
    }
    /**
    * Seleccionar el campo que se desea editar
    */
    onBeforeSaveCell(row, cellName, cellValue) {
        // You can do any validation on here for editing value,
        // return false for reject the editing
        return true;
    }
    /**
    * Guardar la información de la celda
    */
    onAfterSaveCell = (row, cellName, cellValue) => {

        if (cellName === "NombreObra") {
            //Validamos que se cambio de obra
            if (cellValue !== row.NombreObraValidar) {
                //Se debe actualizar los datos de los materiales, cuando se seleccione 
                this.setState({ swmodal: true });
                /**
                 * Capturamos el nombre del usuario que esta creando la solicitud
                */
                let usr = this.props.lg.map(dtuser => dtuser.dtuserid);
                /**
                 * Capurar el id siguiente para las observaciones
                 */
                let contObs = 0, comObser = "";
                let contObser = this.props.observ.map(dtid => dtid.id);
                if (contObser.length > 0) { contObs = contObser.length + 1 } else { contObs = 1 }

                comObser = "Registro de Solicitud: Solicitud modificada en pendiente por validar, campo de obra";
                const dataListObserva = {
                    id: contObs,
                    FkSolicitudMaterial: row.id,
                    FechaCreacion: Moment(new Date()).format("YYYY-MM-DD hh:mm a"),
                    FkUsuario: usr[0],//Usuario logueado
                    Observaciones: comObser,
                }

                axios.post(RutaApi + 'Observacion', JSON.stringify(dataListObserva))
                    .then(res => {
                        //console.log(res.data);
                        if (res.data !== 0) {
                            this.props.dispatch({
                                type: 'ADD_OBSERVACION',
                                dataListObserva
                            });
                        }
                    })
                    .catch(error => {
                        //console.log(error.response);
                        this.titleMsn = "Error";
                        this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                    })
                /**
                 * Recorremos el array de los items de la solicitud
                 * para dejar los campos de Calsificacón y Centro de Costos
                 * en undefined, para poder enviarlo al store del redux 
                 */
                for (let index = 0; index < row.DataListItems.length; index++) {
                    row.DataListItems[index].DescripcionClasificacion = undefined;
                    row.DataListItems[index].CodCentroCosto = undefined;
                    row.DataListItems[index].NombreCentroCosto = undefined;
                }
                const dataSendListItems = row.DataListItems;
                /**
                 * Actualizar los campos de Clasificación y C. Costos
                 */
                for (let index = 0; index < row.DataListItems.length; index++) {
                    //Editar los datos al Store de react-redux
                    this.props.dispatch({
                        type: 'UPDATE_LISTOITEMSOBRA',
                        id: row.DataListItems[index].id,
                        fkid: row.id,
                        dataSendListItems
                    });
                }

                /**
                 * Cambiar el obra de la solicitud en BD
                 */
                let dataListObra = this.props.listobra.filter(p => p.label === cellValue);
                const dataSendListAxios = {
                    IdSolicitudMaterial: row.id,
                    FkObra: dataListObra[0].value,
                    FkEstadoSolicitud: 0,
                    DataListItems: []
                }
                axios.post(RutaApi + 'SolicitudMaterial', JSON.stringify(dataSendListAxios))
                    .then(res => {
                        if (res.data !== 0) {

                        }
                    })
                    .catch(error => {
                        //console.log(error.response);
                        this.titleMsn = "Error";
                        this.textMsn = "Se presento un error; se detalla a continuación " + error.response;
                    });
            }

        }
    }

    onLengthData = () => {

    }

    isExpandableRow(row) {
        //if (row.id < DataListPrueba.length+1) return true;
        // if (row.id < this.dtlis+1) return true;
        // else return false;
        return true;
    }

    expandComponent(row) {
        return (
            <TablaDatosItemsID onDataItems={row.DataListItems} onEncargado={row.NombreEncargado} onNroSolici={row.NumeroSolicitud} onIdSolici={row.id} />
        );
    }
    UNSAFE_componentWillMount() {
        this.onLoadData();
    }
    onLoadData = async () => {
        // Se realiza este setTimeout para dejar que cargue la informacion que trae del API
        this.props.dispatch({
            type: 'CLEAN_LIST'
        });
        this.props.dispatch({
            type: 'CLEAN_OBSERVACION'
        });
        const accObra = this.props.lg;
        //Cargar la información del usuario logeado para poder sacar que accesos a obras tiene
        //Consultar que obras tiene asociadas
        let resObr = await axios.get(RutaApi + 'AccesoObra?_id=' + accObra[0].dtuserid);
        let dataSendListItem = {}, acobradata = resObr.data.map(a => a.FkObra);
        //Cargar la informacion de las solicitudes y las guarda en el store
        let res = await axios.get(RutaApi + 'SolicitudMaterial');
        for (let index = 0; index < res.data.length; index++) {
            this.arrList = [];
            // this.setState(previousState => ({
            //     arrList: []
            // }));
            const element = res.data[index];
            //Consultamos la obra para darnos cuenta si se encuentra inactiva 
            let estobra = this.props.obra.filter(ob => ob.id === element.FkObra).map(ob => ob.FkEstado);
            estobra = estobra[0];
            //Validamos que la obra no este inactiva
            if (parseInt(estobra) !== parseInt(2)) {
                //Recorremos las obras asignadas al usuario logeado
                for (let index = 0; index < acobradata.length; index++) {
                    const elementAccObra = acobradata[index];
                    if (element.FkObra === elementAccObra) {
                        for (let i = 0; i < element.DataListItems.length; i++) {
                            const e = element.DataListItems[i];

                            dataSendListItem = {
                                id: e.IdSolicitudMaterialItem,
                                fkidlist: e.FkSolicitudMaterial,
                                DescripcionClasificacion: e.DescripcionClasificacion,
                                DescripcionClasificacionValidar: e.DescripcionClasificacion,
                                NombreCentroCosto: e.NombreCentroCosto,
                                CodCentroCosto: e.CodCentroCosto,
                                NombreInsumo: e.NombreInsumo,
                                CodInsumo: e.CodInsumo,
                                Cantidad: e.Cantidad,
                                CantidadEntregada: e.CantidadEntregada,
                                Unidad: e.Unidad,
                            }
                            // this.setState({ arrList: [...this.state.arrList, dataSendListItem] });
                            this.arrList.push(dataSendListItem);
                        }
                        const dataSendList = {
                            id: element.IdSolicitudMaterial,
                            NumeroSolicitud: element.NumeroSolicitud,
                            FechaCreacion: element.FechaCreacion,
                            NombreObra: element.NombreObra,
                            NombreObraValidar: element.NombreObra,
                            NombreEncargado: element.NombreEncargado,
                            NombreContratista: element.NombreContratista,
                            Usuario: element.Usuario,//Usuario logueado
                            FkObservacion: element.IdSolicitudMaterial,
                            // DataListItems: this.state.arrList,
                            DataListItems: this.arrList,
                            EstadoSolicitud: element.EstadoSolicitud,
                        }
                        this.props.dispatch({
                            type: 'ADD_LIST',
                            dataSendList
                        });

                    }
                }

            }
        }
        // axios.get(RutaApi + 'SolicitudMaterial')
        //     .then(res => {

        //     })

        //Cargar la informacion de las observaciones y las guarda en el store
        let resObs = await axios.get(RutaApi + 'Observacion');
        for (let index = 0; index < resObs.data.length; index++) {
            const element = resObs.data[index];
            const dataListObserva = {
                id: element.IdObservacion,
                FkSolicitudMaterial: element.FkSolicitudMaterial,
                FechaCreacion: element.FechaCreacion,
                FkUsuario: element.FkUsuario,
                Usuario: element.Usuario,//Usuario logueado
                Observaciones: element.Observaciones,
            }
            this.props.dispatch({
                type: 'ADD_OBSERVACION',
                dataListObserva
            });
        }
        // axios.get(RutaApi + 'Observacion')
        //     .then(resObs => {

        //     })
        setTimeout(() => { this.onChangeLoading(); }, 300);
    }
    onChangeLoading = () => {
        this.setState({ loading: false });
    }
    render() {
        let dataList = this.props.list.filter(p => p.EstadoSolicitud === 'Pendiente por Aprobar Validación'), dataObsvr = this.props.observ, dataLg = this.props.lg;
        // const dataObsvr = DataObservacionPrueba;
        //Para poder generar el comentario
        const ref = React.createRef();
        const createObservationEditor = (onUpdate, props) => (<Observacion onUpdate={onUpdate}  {...props} ref={ref} onChangeObser={this.handleChangeObser} onObser={dataObsvr} />)
        //Cambio de estado
        const refEst = React.createRef();
        const createAprobacion = (onUpdate, props) => (<Aprovacion onUpdate={onUpdate}  {...props} ref={refEst} onChangeEst={this.handleChangeEst} onDataEst={this.props.listestsolicitud} />)

        //Mostrar los iconos dependiendo del estado
        const dataIcon = (cell, row, file) => {
            let ele = null;
            this.props.listestsolicitud.map(dt => {
                if (row.EstadoSolicitud === dt.value) {
                    ele = `<i class='fa fa-${dt.icon}' aria-hidden='true' style='${dt.style}'></i>`;
                }
                return ele;
            });
            return ele;
        }
        const dataIconObserva = (cell, row, rowIndex) => {
            return `<i class='fa fa-pencil' aria-hidden='true' style='cursor:pointer'></i>`;
        }
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell,
            nonEditableRows: function () {
                // Se realiza el filtro para para saber que fila se debe inhabilitar
                // let dtsw = dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada').map(p => p.id);
                // if (dtsw.length > 0) { return dataList.filter(p => p.dtestado === 'Finalizada Sin Entrega' || p.dtestado === 'Finalizada' || p.dtestado === 'Anulada').map(p => p.id); }

            }
        };
        const options = {
            noDataText: 'No hay datos en el momento',
            //btnGroup: this.createCustomDeleteButton,
            expandBy: 'column',//Esto es para saber que columnas son expandable true o false
        };
        const override = css`
            display: block;
            margin: 0 auto;
            border-color: red;`;
        return (
            <div>{this.state.loading ?
                <BounceLoader css={override} sizeUnit={"px"} size={150} color={'#123abc'} loading={this.state.loading}></BounceLoader>
                :
                <div>
                    <BootstrapTable
                        data={dataList}
                        bodyStyle={{ overflow: "overflow" }}
                        options={options}
                        pagination={true}
                        cellEdit={cellEditProp}
                        search={true}
                        expandableRow={this.isExpandableRow}
                        expandComponent={this.expandComponent}
                        expandColumnOptions={{ expandColumnVisible: true }}
                    >
                        <TableHeaderColumn dataField="id" isKey={true} hidden={true}># item</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObraValidar" hidden={true}>#NombreObraValidar</TableHeaderColumn>
                        <TableHeaderColumn dataField="NumeroSolicitud" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Nro</TableHeaderColumn>
                        <TableHeaderColumn dataField="FechaCreacion" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Fecha</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreObra" dataSort={true} editable={parseInt(dataLg[0].dtu) === 1 ? { type: 'select', options: { values: this.props.listobra.map(dt => { return dt.label }) } } : false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Obra</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreContratista" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Contratista</TableHeaderColumn>
                        <TableHeaderColumn dataField="NombreEncargado" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Encargado</TableHeaderColumn>
                        <TableHeaderColumn dataField="Usuario" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Solicitado por</TableHeaderColumn>
                        <TableHeaderColumn dataField="EstadoSolicitud" dataSort={true} editable={false} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} expandable={false}>Estado</TableHeaderColumn>
                        <TableHeaderColumn dataField="dticono" dataFormat={dataIcon} tdStyle={{ whiteSpace: "normal" }} thStyle={{ whiteSpace: "normal" }} customEditor={{ getElement: createAprobacion }} expandable={false}>Icono</TableHeaderColumn>
                        <TableHeaderColumn dataField="FkObservacion" dataFormat={dataIconObserva} customEditor={{ getElement: createObservationEditor }} expandable={false} editColumnClassName='editing-jobsname-class' invalidEditColumnClassName='invalid-jobsname-class' editable={this.isEditable} dataSort={true}>Observación</TableHeaderColumn>
                    </BootstrapTable>
                    {this.state.swmodal ? <ModalGeneral onShow={this.state.swmodal} onChange={this.handleChange} onTitle="Advertencia" onBody="Recuerde que al cambiar la Obra, se deben seleccionar los campos de Clasificación y Actividades, en el detalle de los materiales." onIf={false} onClose={false} ></ModalGeneral> : null}
                    {this.state.swmodalApro ? <ModalGeneral onShow={this.state.swmodalApro} onChange={this.handleChange} onTitle="Advertencia" onBody="Recuerde que para poder aceptar la validacion, debes seleccionar los campos de Clasificación y Actividades." onIf={false} onClose={false} ></ModalGeneral> : null}
                </div>
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.List,
        lg: state.Login,
        observ: state.Observacion,
        listobra: state.ListaObra,
        listestsolicitud: state.EstadoSolicitud,
        obra: state.Obra,
    }
}


export default connect(mapStateToProps)(TablaAprobarValidacion);